FROM openjdk:17
#ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:9200
ARG JAR_FILE=target/service-members-*.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=docker" ,"-jar", "/app.jar"]
EXPOSE 8445