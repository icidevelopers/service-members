package org.ici.members;

public class MemberUtil {
    public static final int TYPE_UNKNOWN                = 0;
    public static final int TYPE_EMPLOYEE               = 1;
    public static final int TYPE_MEMBER                 = 2;
    public static final int TYPE_INDEPENDENT_DIRECTOR   = 3;
    public static final int TYPE_ASSOCIATE_MEMBER       = 4;
    public static final int TYPE_ENTERPRISE_SUBSCRIBER  = 5;
    public static final int TYPE_STATISTICAL_SUBSCRIBER = 6;
    public static final int TYPE_DATA_REPUBLISHER       = 7;
    public static final int TYPE_EVENT_ATTENDEE         = 8;

    public static String[] TYPE_STRINGS = {"nonMember", "employee", "fullMember", "independentDirector", "associateMember",
                            "enterpriseSubscriber", "statisticalSubscriber", "dataRepublisher", "eventAttendee"};

    public static final int MAILING_CONFIDENTIAL        = 186;
    public static final int MAILING_DAILY               = 594;
    public static final int MAILING_QUERYTOOL           = 677;
    public static final int MAILING_IQUEST              = 781;

/*
    *** Statistical Report Mailings ***
            AggregateMonthlyMFHistoricalFile = 890
            ClosedEndFundData = 501
            CombinedEstimatedLTMFFlowsAndETCNetIssuance = 952
            ETFData = 188
            InstitutionalMFShareholderReport = 1563
            MFAssetsinRetirementAccounts = 1583
            MFComplexAssetReport = 407
            MFDistributions = 893
            TrendsinMFInvestingReport = 523
            UITData = 28
            WeeklyEstimatedETFNetIssuance = 951
            WeeklyEstimatedLTMFFlows = 510
            WeeklyMMAssets = 509
 */

}
