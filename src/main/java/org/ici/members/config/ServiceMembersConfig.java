package org.ici.members.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceMembersConfig {
    @Bean
    public DozerBeanMapper beanMapper(){return new DozerBeanMapper();}
}
