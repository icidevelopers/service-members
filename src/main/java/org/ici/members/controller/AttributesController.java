package org.ici.members.controller;

import com.thoughtworks.xstream.XStream;
import org.ici.members.db.mappers.ApplicationMapper;
import org.ici.members.db.mappers.AttributesMapper;
import org.ici.services.domain.dto.component.email.Person;
import org.ici.services.domain.dto.component.email.PnumCommittee;
import org.ici.services.domain.dto.component.email.PnumMailing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/member", produces = {MediaType.APPLICATION_JSON_VALUE})
public class AttributesController {

    private Logger logger = LoggerFactory.getLogger(AttributesController.class);
    private List<PnumCommittee> pnumCommitteesList;
    private List<PnumMailing> pnumMailingList;

    @Autowired
    private ApplicationMapper applicationMapper;

    @Autowired
    private AttributesMapper attributesMapper;

    @RequestMapping(value = "/attributes/v1.0/mailing/{mailingName}", method = RequestMethod.GET)
    private ResponseEntity<Collection<Person>> getMembersByMailingName(@PathVariable String mailingName) {

        logger.info("Got path param: " + mailingName);

        applicationMapper.dropPnumTempTableIfExists();
        applicationMapper.createPnumTempTable();

        attributesMapper.getPersonByMailing(mailingName);

        return ResponseEntity.ok(addMailingsCommitteesToPerson(applicationMapper.selectPnumTempTable()));

    }

    private List<Person> addMailingsCommitteesToPerson(List<Person> personList) {

        Map<Integer, Person> personMap = new HashMap<>();

        for (Person p : personList) {
            personMap.put(p.getPnum(), p);
        }

        pnumCommitteesList = applicationMapper.getPnumCommitteeJOIN();
        pnumCommitteesList.forEach((pnumCommittee) -> personMap.get(pnumCommittee.getPnum()).addCommittee(pnumCommittee.getCode()));

        pnumMailingList = applicationMapper.getPnumMailingJOIN();
        pnumMailingList.forEach((pnumMailing) -> personMap.get(pnumMailing.getPnum()).addMailing(pnumMailing.getCode()));

        return personMap.values().stream().collect(Collectors.toList());
    }

    private void printObject(Object val) {
        XStream xs = new XStream();
        System.out.println(xs.toXML(val));
    }

}



