package org.ici.members.controller;

import com.thoughtworks.xstream.XStream;
import org.ici.members.db.mappers.ApplicationMapper;
import org.ici.members.db.mappers.MemberTypeMapper;
import org.ici.members.db.mappers.MemberTypeRequireEmailMapper;
import org.ici.members.db.mappers.UserMapper;
import org.ici.services.domain.dto.component.email.Person;
import org.ici.services.domain.dto.component.email.PnumCommittee;
import org.ici.services.domain.dto.component.email.PnumMailing;
import org.ici.services.domain.dto.component.generic.IntegerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/member", produces = {MediaType.APPLICATION_JSON_VALUE})
public class MemberTypeController {

    private Logger logger = LoggerFactory.getLogger(MemberTypeController.class);
    private List<PnumCommittee> pnumCommitteesList;
    private List<PnumMailing> pnumMailingList;


    @Autowired
    private ApplicationMapper applicationMapper;

    @Autowired
    private MemberTypeMapper memberTypeMapper;

    @Autowired
    private MemberTypeRequireEmailMapper memberTypeRequireEmailMapper;

    @Autowired
    private UserMapper userMapper;

    @RequestMapping(value = "/type/v1.0/{type}", method = RequestMethod.GET)
    private ResponseEntity<Collection<IntegerData>> getMemberTypePnums(@PathVariable("type") String type) {
        switch (type) {
            case "fullmembers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeFullMember());
            case "enterprisesubscribers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeEnterpriseSubscriber());
            case "icistaff":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeICIStaff());
            case "boardofgovernors":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeBoardOfGovernors());
            case "independentdirectors":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeIndependentDirector());
            case "interesteddirectors":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeInterestedDirector());
            case "committeemembers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeCommitteeMember());
            case "associatemembers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeAssociateMember());
            case "globalmembers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeGlobalMember());
            case "broadglobalmembers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeBroadGlobalMember());
            case "globalcommitteemembers":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeGlobalCommitteeMember());
            case "globaldailynewslettermailing":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeGlobalDailyNewsletterMailing());
            case "excludefromiciglobaldailymailing":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeExcludeFromICIGlobalDailyMailing());
            case "excludefromdomesticdailymailing":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeExcludeFromDomesticDailyMailing());
            case "excludefrombrexitbriefing":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeExcludeFromBrexitBriefing());
            case "internationalcommittee":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeInternationalCommittee());
            case "internationalinvestingsubcommittee":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeInternationalInvestingSubcommittee());
            case "internationaloperationsadvisorycommittee":
                return ResponseEntity.ok(memberTypeMapper.getMembershipTypeInternationalOperationsAdvisoryCommittee());
            //case "domesticdaily":
            //    return ResponseEntity.ok(getDomesticDaily());
            default:
                throw new IllegalArgumentException("unable to handle type: " + type);
        }
    }

    @RequestMapping(value = "/type/v1.0/{type}/requireemail", method = RequestMethod.GET)
    private ResponseEntity<Collection<Person>> getMemberTypeRequireEmailPnums(@PathVariable("type") String type) {

        applicationMapper.dropPnumTempTableIfExists();
        applicationMapper.createPnumTempTable();

        switch (type) {
            case "fullmembers":
                memberTypeRequireEmailMapper.getMembershipTypeFullMember();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "enterprisesubscribers":
                memberTypeRequireEmailMapper.getMembershipTypeEnterpriseSubscriber();
                return ResponseEntity.ok(addAttributesToEnterpriseSubscribers(applicationMapper.selectPnumTempTable()));
            case "icistaff":
                memberTypeRequireEmailMapper.getMembershipTypeICIStaff();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "boardofgovernors":
                memberTypeRequireEmailMapper.getMembershipTypeBoardOfGovernors();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "independentdirectors":
                memberTypeRequireEmailMapper.getMembershipTypeIndependentDirector();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "interesteddirectors":
                memberTypeRequireEmailMapper.getMembershipTypeInterestedDirector();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "committeemembers":
                memberTypeRequireEmailMapper.getMembershipTypeCommitteeMember();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "associatemembers":
                memberTypeRequireEmailMapper.getMembershipTypeAssociateMember();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "globalmembers":
                memberTypeRequireEmailMapper.getMembershipTypeGlobalMember();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "broadglobalmembers":
                memberTypeRequireEmailMapper.getMembershipTypeBroadGlobalMember();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "globalcommitteemembers":
                memberTypeRequireEmailMapper.getMembershipTypeGlobalCommitteeMember();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "globaldailynewslettermailing":
                memberTypeRequireEmailMapper.getMembershipTypeGlobalDailyNewsletterMailing();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "excludefromiciglobaldailymailing":
                memberTypeRequireEmailMapper.getMembershipTypeExcludeFromICIGlobalDailyMailing();
                return ResponseEntity.ok(applicationMapper.selectPnumTempTable());
            case "excludefromdomesticdailymailing":
                memberTypeRequireEmailMapper.getMembershipTypeExcludeFromDomesticDailyMailing();
                return ResponseEntity.ok(applicationMapper.selectPnumTempTable());
            case "excludefrombrexitbriefing":
                memberTypeRequireEmailMapper.getMembershipTypeExcludeFromBrexitBriefing();
                return ResponseEntity.ok(applicationMapper.selectPnumTempTable());
            case "excludefromdigestmailing":
                memberTypeRequireEmailMapper.getMembershipTypeExcludeFromDigestMailing();
                return ResponseEntity.ok(applicationMapper.selectPnumTempTable());
            case "internationalcommittee":
                memberTypeRequireEmailMapper.getMembershipTypeInternationalCommittee();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "internationalinvestingsubcommittee":
                memberTypeRequireEmailMapper.getMembershipTypeInternationalInvestingSubcommittee();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            case "internationaloperationsadvisorycommittee":
                memberTypeRequireEmailMapper.getMembershipTypeInternationalOperationsAdvisoryCommittee();
                return ResponseEntity.ok(addAttributesToPerson(applicationMapper.selectPnumTempTable()));
            //case "domesticdaily":
            //    return ResponseEntity.ok(getDomesticDaily());
            default:
                throw new IllegalArgumentException("unable to handle type: " + type);
        }

    }

    @RequestMapping(value = "/type/v1.0/{type}/removedomesticdailydups", method = RequestMethod.GET)
    private ResponseEntity<Collection<IntegerData>> getMemberTypeRemoveDomesticDailyDupsPnums(@PathVariable("type") String type) {
        switch (type) {
            case "fullmembers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeFullMember()));
            case "enterprisesubscribers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeEnterpriseSubscriber()));
            case "icistaff":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeICIStaff()));
            case "boardofgovernors":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeBoardOfGovernors()));
            case "independentdirectors":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeIndependentDirector()));
            case "interesteddirectors":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeInterestedDirector()));
            case "committeemembers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeCommitteeMember()));
            case "associatemembers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeAssociateMember()));
            case "globalmembers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeGlobalMember()));
            case "broadglobalmembers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeBroadGlobalMember()));
            case "globalcommitteemembers":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeGlobalCommitteeMember()));
            case "globaldailynewslettermailing":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeGlobalDailyNewsletterMailing()));
            case "excludefromiciglobaldailymailing":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeExcludeFromICIGlobalDailyMailing()));
            case "excludefromdomesticdailymailing":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeExcludeFromDomesticDailyMailing()));
            case "excludefrombrexitbriefing":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeExcludeFromBrexitBriefing()));
            case "internationalcommittee":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeInternationalCommittee()));
            case "internationalinvestingsubcommittee":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeInternationalInvestingSubcommittee()));
            case "internationaloperationsadvisorycommittee":
                return ResponseEntity.ok(removeDomesticDailyDups(type, memberTypeMapper.getMembershipTypeInternationalOperationsAdvisoryCommittee()));
            default:
                throw new IllegalArgumentException("unable to handle type: " + type);
        }
    }

    private List<Person> addAttributesToPerson(List<Person> personList) {

        Map<Integer, Person> personMap = new HashMap<>();

        for (Person p : personList) {
            personMap.put(p.getPnum(), p);
        }

        pnumCommitteesList = applicationMapper.getPnumCommitteeJOIN();
        pnumCommitteesList.forEach((pnumCommittee) -> personMap.get(pnumCommittee.getPnum()).addCommittee(pnumCommittee.getCode()));

        pnumMailingList = applicationMapper.getPnumMailingJOIN();
        pnumMailingList.forEach((pnumMailing) -> personMap.get(pnumMailing.getPnum()).addMailing(pnumMailing.getCode()));

        return personMap.values().stream().collect(Collectors.toList());
    }

    private List<Person> addAttributesToEnterpriseSubscribers(List<Person> personList) {

        Map<Integer, Person> personMap = new HashMap<>();

        for (Person p : personList) {
            personMap.put(p.getPnum(), p);
        }

        pnumCommitteesList = applicationMapper.getPnumCommitteeJOIN();
        pnumCommitteesList.forEach((pnumCommittee) -> personMap.get(pnumCommittee.getPnum()).addCommittee(pnumCommittee.getCode()));

        pnumMailingList = applicationMapper.getPnumMailingJOIN();
        pnumMailingList.forEach((pnumMailing) -> personMap.get(pnumMailing.getPnum()).addMailing(pnumMailing.getCode()));

        List<PnumMailing> pnumMailings = applicationMapper.getPnumsEnterpriseSubscriberMailings();
        personMap.forEach((pmpnum, pmperson) -> personMap.get(pmpnum).addMailings(pnumMailings.stream().filter(p -> pmpnum.equals(p.getPnum())).map(PnumMailing::getCode).collect(Collectors.toSet())));

        return personMap.values().stream().collect(Collectors.toList());

    }

    private void populateCommitteeList(List<Person> persons) {
        pnumCommitteesList = applicationMapper.getPnumCommitteeIN(persons
                .stream()
                .flatMap(person -> Stream.of(person.getPnum()))
                .collect(Collectors.toList()));

    }

    private void populateMailingList(List<Person> persons) {
        pnumMailingList = applicationMapper.getPnumMailingIN(persons
                .stream()
                .flatMap(person -> Stream.of(person.getPnum()))
                .collect(Collectors.toList()));

    }

    public List<IntegerData> removeDomesticDailyDups(String memberType, List<IntegerData> memberTypePnums) {

        Set<Integer> contacts = new HashSet<>(memberTypePnums
                .stream()
                .flatMap(anA -> Stream.of(anA.getNumber()))
                .collect(Collectors.toSet()));

        List<IntegerData> contactsNonDupe = new ArrayList<>();

//        for (String type : getDomesticIncludes()) {
//
//            if (type.equals(memberType))
//                continue;
//
//            contacts.removeAll(getMemberTypeRequireEmailPnums(type)
//                                .getBody()
//                                .stream()
//                                .flatMap(anA -> Stream.of(anA.getNumber()))
//                                .collect(Collectors.toSet()));
//
//        }

        for (String type : getDomesticExcludes()) {

            contacts.removeAll(getMemberTypePnums(type)
                    .getBody()
                    .stream()
                    .flatMap(anA -> Stream.of(anA.getNumber()))
                    .collect(Collectors.toSet()));

        }

        for (Integer i : contacts) {
            contactsNonDupe.add(new IntegerData(i));
        }

        return contactsNonDupe;
    }

//    private List<IntegerData> getDomesticDaily() {
//
//        Set<Integer> contacts =  new HashSet<>();
//        List<IntegerData> contactsNonDupe = new ArrayList<>();
//
//        for (String type : getDomesticIncludes()) {
//
//            contacts.addAll(getMemberTypeRequireEmailPnums(type)
//                              .getBody()
//                              .stream()
//                              .flatMap(anA -> Stream.of(anA.getNumber()))
//                              .collect(Collectors.toSet()));
//
//        }
//
//        for (String type : getDomesticExcludes()) {
//
//            contacts.removeAll(getMemberTypeRequireEmailPnums(type)
//                                 .getBody()
//                                 .stream()
//                                 .flatMap(anA -> Stream.of(anA.getNumber()))
//                                 .collect(Collectors.toSet()));
//
//        }
//
//        for (Integer i : contacts) {
//            contactsNonDupe.add(new IntegerData(i));
//        }
//
//        return contactsNonDupe;
//
//    }

    private List<String> getDomesticIncludes() {

        //MemberType.FullMembers, MemberType.EnterpriseSubscribers, MemberType.ICIStaff, MemberType.BoardOfGovernors, MemberType.IndependentDirectors, MemberType.InterestedDirectors, MemberType.CommitteeMembers, MemberType.AssociateMembers
        //return Arrays.asList("fullmembers");
        return Arrays.asList("fullmembers", "enterprisesubscribers", "icistaff", "boardofgovernors", "independentdirectors", "interesteddirectors", "committeemembers", "associatemembers");

    }

    private List<String> getDomesticExcludes() {

        return Arrays.asList("excludefromdomesticdailymailing");

    }

    private void printObject(Object val) {
        XStream xs = new XStream();
        System.out.println(xs.toXML(val));
    }

}



