package org.ici.members.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import org.apache.commons.codec.binary.StringUtils;
import org.ici.members.db.domain.membership.*;
import org.ici.members.db.mappers.*;
import org.ici.members.db.mappers.membership.CompanyMapper;
import org.ici.members.services.CRMDynamics365AuthenticationService;
import org.ici.members.services.MemberService;
import org.ici.services.domain.dto.component.email.EmailListWrapper;
import org.ici.services.domain.dto.cvent.CventPersonDatum;
import org.ici.services.domain.dto.cvent.ici.CventCommitteeEventLkup;
import org.ici.services.domain.dto.cvent.ici.CventEventLkup;
import org.ici.services.domain.dto.login.Token;
import org.ici.services.domain.dto.membership.Committee;
import org.ici.services.domain.dto.membership.CventIgnoreUser;
import org.ici.services.domain.users.PersonRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/member", produces = "application/json")
public class MembersController {

    Logger logger = LoggerFactory.getLogger(MembersController.class);

    @Autowired
    private MemberService memberService;

    @Autowired
    private CRMDynamics365AuthenticationService crmService;

    @Autowired
    private ApplicationMapper applicationMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private MailingMapper mailingMapper;

    @Autowired
    private CventIgnoreUserMapper cventIgnoreUserMapper;

    @Autowired
    private CventProcessedMapper cventProcessUserMapper;

    @Autowired
    private CompContactMapper compContactMapper;

    @Value("${drupal.user.url}")
    private String userUrl;

    @Autowired
    private CommitteeMapper committeeMapper;

    @Autowired
    private CompanyMapper companyMapper;

    @RequestMapping(value = "/company/{cnum}", method = RequestMethod.GET)
    public String findCompanyByCnumPnum(@PathVariable("cnum") Integer cnum) {
        logger.warn("Requested CNUM: " + cnum);
        if (cnum == null)
            return "Invalid Company";

        String retval = memberService.buildCompanyProfile(cnum);

        return retval;
    }

    @RequestMapping(value="/getPerson", method = RequestMethod.POST)
    public List<PersonRequest> getPerson(@RequestBody PersonRequest request){
        List<PersonRequest> requests = new ArrayList<>();
        requests.add(request);
        List<PersonRequest> people =  memberService.getPersonFromRequest(requests);
        return people;
    }


    @RequestMapping(value = "/get/{pnum}", method = RequestMethod.GET)
    public ResponseEntity<PersonRequest> findUserByPnum(@PathVariable("pnum") Integer pnum) {
        logger.warn("Requested PNUM: " + pnum);
        if (pnum == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        PersonRequest personRequest = memberService.getPersonRecord(pnum);
        if (personRequest == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return ResponseEntity.ok(personRequest);
    }


    @RequestMapping(value="/getByEmail", method = RequestMethod.POST)
    public ResponseEntity<List<PersonRequest>> findUserByEmail(@RequestBody EmailListWrapper emails){
        logger.info("Request Emails = " + String.join(",", emails.getEmails()));
        if(emails == null || emails.getEmails().size() == 0){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<PersonRequest> personRequest = memberService.getPersonRecords(emails.getEmails());
        if(personRequest == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(personRequest);
    }


    @RequestMapping(value = "addressSuffixLookup", method = RequestMethod.GET)
    public AddressAbbrevWrapper getAddressSuffixDictionary(){
        List<AddressAbbrevLkup> addressAbbrevLkups = userMapper.getAddressAbbrev();
        AddressAbbrevWrapper wrapper = new AddressAbbrevWrapper();
        wrapper.setAddressAbbrevLkupList(addressAbbrevLkups);
        return wrapper;
    }

    //TODO find a way to test this with Dan
    @PostMapping(value = "/set/")
    public ResponseEntity<PersonRequest> setUserData(@RequestBody PersonRequest person) {
        logger.warn("Setting User Data: " + person.toString());

        person.setIciCountryCode(getIciCountryCode(person.getCountry()));

        Field[] fields = person.getClass().getDeclaredFields();
        for(Field field: fields){
            try {
                Field currentField = PersonRequest.class.getDeclaredField(field.getName());
                currentField.setAccessible(true);
                Object fieldVal = currentField.get(person);
                if(currentField.getType().getName().contains("java.lang.String") && fieldVal == null){
                    currentField.set(person, "");
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }


        if (person.getPnum() == 0) {
            logger.info("New Enrollment");
        }
        else {
            logger.info("Update Enrollment");
        }

        logger.info("**************************************************************");
        logger.info(person.toString());
        logger.info("**************************************************************");
        userMapper.addEnrollment(person);

        return ResponseEntity.ok(person);
    }

    private int getIciCountryCode(String country) {
        if (country == null || country.trim().equals(""))
            return 0;

        CountryListDrupalLkup countryListLkup = userMapper.getCountryIciInfo(country);
        if (countryListLkup != null) {
            return countryListLkup.getCode();
        }

        return 0;
    }

    @GetMapping(value = "countryLkup/{id}")
    private CountryListLkup getCountryFromId(@PathVariable("id") Integer id){
        return userMapper.getCountryInfoFromId(id);
    }



    @PostMapping(value = "/getMemberInfoFromCventId")
    public PersonRequest getMemberInfo(@RequestBody CventPersonDatum cventPersonDatum){
        Person personRequest = userMapper.getPersonByCriteria(cventPersonDatum);
        if(personRequest == null){
            return null;
        }else {
            return memberService.getPersonRecord(personRequest.getPnum());
        }
    }

    @RequestMapping(value = "/findPotentialWebsiteUsers/{start}/{end}", method = RequestMethod.GET)
    public String findPotentialWebsiteUsers(@PathVariable("start") Integer start, @PathVariable("end") Integer end) {
        logger.warn("Starting findPotentialWebsiteUsers ");

        String retval = "User List ... \n";

        List<Person> people = userMapper.getPotentialWebsiteUsers(start, end);


        Token authToken = memberService.getToken();

        HttpHeaders header = new HttpHeaders();
        header.setAccept(Collections.singletonList(MediaType.ALL));
        header.add("Authorization", authToken.getToken_type() + " " + authToken.getAccess_token());

        RestTemplate restTemplate = new RestTemplate();
        String skippedList = "";
        String badPnums = "";

        int ct = 0;
        for (Person p : people) {
            logger.info(p.getPnum() + " - " + p.getFname() + " " + p.getLname() + " - " + p.getEmail());
            retval += p.getPnum() + " - " + p.getFname() + " " + p.getLname() + "\n";

            PersonRequest person = memberService.getPersonRecord(p.getPnum());
            if (person == null) {
                logger.warn("    **** Person Record NULL");
                retval += "    **** Skipped! " + p.getPnum() + "\n";
                skippedList += "    **** Skipped! " + p.getPnum() + " - " + p.getFname() + " " + p.getLname() + "\n";
                ct++;
                continue;
            }

            try {
                if (ct / 1000 == 1) {
                    System.out.println("\n\n\n");
                    System.out.println("****************** RESETTING ****************");
                    System.out.println("****************** RESETTING ****************");
                    System.out.println("****************** RESETTING ****************");
                    System.out.println("****************** RESETTING ****************");
                    System.out.println("\n\n\n");
                    authToken = memberService.getToken();

                    header = new HttpHeaders();
                    header.setAccept(Collections.singletonList(MediaType.ALL));
                    header.add("Authorization", authToken.getToken_type() + " " + authToken.getAccess_token());
                    ct = 0;
                }

                HttpEntity<PersonRequest> userEntity = new HttpEntity<>(person, header);
                restTemplate.postForObject(userUrl+p.getPnum(), userEntity, String.class);
                ct++;
            }
            catch (Exception e) {
                badPnums = "," + p.getPnum();
                e.printStackTrace();
            }
        }

        logger.error("\n\n    Bad PNUMS - " + badPnums + "\n\n");
        return retval + "\n\n\nSkipped List ===============\n" + skippedList + "\n BAD = " + badPnums;
    }


    @RequestMapping(value = "/syncUpPnum/{pnum}", method = RequestMethod.GET)
    public String syncUpPnum(@PathVariable("pnum") Integer pnum) {
        logger.warn("Starting syncUpPnum - " + pnum);

        Token authToken = memberService.getToken();

        HttpHeaders header = new HttpHeaders();
//        header.add("accept", "application/json");
        header.add("Authorization", authToken.getToken_type() + " " + authToken.getAccess_token());

        RestTemplate restTemplate = new RestTemplate();

        PersonRequest person = memberService.getPersonRecord(pnum);
        if (person == null) {
            logger.error("    **** Person Record NULL");
            return "Failed";
        }

        try {
            //TODO The person object is NOT the object referenced by drupal this was a one-off POJO that was written...but when stringified it works fine
            // not sure what to do here, but the stringification seems to have fixed the main issue
            ObjectMapper mapper = new ObjectMapper();
            String request = mapper.writeValueAsString(person);
            HttpEntity<String> userEntity = new HttpEntity<>(request, header);
            String returnVal = restTemplate.postForObject(userUrl+pnum, userEntity, String.class);
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        return "Success";
    }

    @PatchMapping(path="updatePerson/{pnum}", consumes = "application/json-patch+json")
    public ResponseEntity<Person> updatePerson(@PathVariable("pnum") String pnum, @RequestBody JsonMergePatch patch){
        Person personRecord = userMapper.getPersonByPnum(Integer.parseInt(pnum));
        Person personPatched = memberService.applyPatchToPerson(patch, personRecord);
        if(personPatched == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The passed in patch object could not be mapped back to a person object ");

        }else{

            int updated = userMapper.updatePerson(personPatched);
            Person personWithUpdatedInfo = userMapper.getPersonByPnum(Integer.parseInt(pnum));
            return ResponseEntity.ok(personWithUpdatedInfo);
        }
    }

    @PatchMapping(path = "updateAddress/{pnum}/{conferenceStartDate}", consumes = "application/json-patch+json")
    public ResponseEntity<Address> updateAddress(@PathVariable("pnum") String pnum,@PathVariable("conferenceStartDate") String conferenceStartDate ,@RequestBody JsonMergePatch patch){
        List<Address> personAddress = userMapper.getAddressByPnum(Integer.parseInt(pnum));
        LocalDate conferenceStartLocalDate = LocalDate.parse(conferenceStartDate);

        Address activePersonAddress = personAddress.stream()
                .filter(address -> address.getBeginDate() == null || (address.getBeginDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(conferenceStartLocalDate)))
                .findFirst()
                .orElse(null);

        Address patchedPersonAddress = memberService.applyPatchToAddresses(patch, activePersonAddress);
        patchedPersonAddress.setBeginDate(activePersonAddress.getBeginDate());
        patchedPersonAddress.setAdtype(activePersonAddress.getAdtype());
        if(patchedPersonAddress == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The passed in patch object could not be mapped back to an address object");
        }else{
            int updated = userMapper.updateAddress(patchedPersonAddress);
            Address updatedAddress = userMapper.getAddressByPnum(Integer.parseInt(pnum)).stream()
                    .filter(address -> address.getBeginDate() == null || (address.getBeginDate().equals(patchedPersonAddress.getBeginDate()) && address.getAdtype().equals(patchedPersonAddress.getAdtype())))
                    .findFirst()
                    .orElse(null);
            return ResponseEntity.ok(updatedAddress);
        }
    }



    @GetMapping(value="get/cventEventLkup")
    public CventEventLkup getCventEventLkup(@RequestParam("eventName") String eventName){
        byte[] bytes = StringUtils.getBytesIso8859_1(eventName);
        String utf8EventName = StringUtils.newStringIso8859_1(bytes);
        CventEventLkup cventEventLkup = mailingMapper.getICICventEventLkup(utf8EventName);
        if(cventEventLkup == null){
            return null;
        }
        else{
            return cventEventLkup;
        }
    }

    @GetMapping(value = "get/cventEventCommitteeLkup")
    public CventCommitteeEventLkup getCventEventCommitteeLkup(@RequestParam(name="eventName") String eventName){
        byte[] bytes = StringUtils.getBytesIso8859_1(eventName);
        String utf8EventName = StringUtils.newStringIso8859_1(bytes);
        CventCommitteeEventLkup cventEventLkup = mailingMapper.getCommitteeEventLkup(utf8EventName);
        if(cventEventLkup == null){
            return null;
        }
        else {
            return cventEventLkup;
        }
    }

    @GetMapping(value="addConferenceMailing/{pnum}/{conferenceName}")
    public CventEventLkup addConferenceMailing(@PathVariable("pnum") Integer pnum, @PathVariable("conferenceName") String conferenceName){
        CventEventLkup eventLkup = mailingMapper.getICICventEventLkup(conferenceName);
        if(eventLkup == null){
            try {
                //The below logic is to handle committee conferences, since there are no mailings just adding an entry
                // to the committee_meeting_attendees table should suffice
                CventCommitteeEventLkup committeeEventLkup = mailingMapper.getCommitteeEventLkup(conferenceName);
                List<Compcontact> compcontactList = compContactMapper.getCompanyContactFromPnum(pnum);
                Integer cnum = -1;
                if(compcontactList.size() > 1){
                    List<Integer> cnums = compcontactList.stream()
                            .map(Compcontact::getCnum)
                            .collect(Collectors.toList());
                    CompanyExample companyExample = new CompanyExample();
                    companyExample.or().andCnumIn(cnums);
                    List<Company> relatedCompanies = companyMapper.selectByExample(companyExample);
                    List<Integer> complexCnums = relatedCompanies.stream()
                            .map(Company::getComplexCnum)
                            .distinct()
                            .collect(Collectors.toList());
                    cnum = complexCnums.get(0);

                }
                else if (compcontactList.size() == 1){
                    cnum = compcontactList.get(0).getCnum();
                }
                else{
                    return null;
                }
                mailingMapper.addCommitteeMeetingAttendee(committeeEventLkup.getMeeting_id().intValue(), pnum, cnum);

            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        else {
            List<Mailrecip> mailrecips = mailingMapper.getMailing(eventLkup.getCode(), pnum);
            if(mailrecips == null || mailrecips.size() == 0) {
                mailingMapper.addMailing(eventLkup.getCode(), pnum);
            }
            return eventLkup;
        }
        return null;
    }

    @GetMapping(value="getIgnoreUsers")
    public List<CventIgnoreUser> getIgnoreUsers(){
        return cventIgnoreUserMapper.getListOfIgnoredUsers();
    }

    @PostMapping(value = "addRecordToIgnore")
    public CventIgnoreUser addUserToIgnore(@RequestBody CventIgnoreUser ignoreUser){
        cventIgnoreUserMapper.addUser(ignoreUser);
        return ignoreUser;
    }

    @PostMapping(value = "cventAddProcessedPerson")
    public CventProcessed addUserToProcessed(@RequestBody CventProcessed processedUser){
        cventProcessUserMapper.addUser(processedUser);
        return processedUser;
    }

    @GetMapping(value = "getProcessed/{eventId}")
    public List<CventProcessed> getProcessedUsers(@PathVariable("eventId") String eventId){
        return cventProcessUserMapper.getUserByEvent(eventId);
    }

    @GetMapping(value="committeeLkupByName/{committeeName}")
    public Committee getCommitteeByMemoRegistration(@PathVariable("committeeName") String committeeName){
        return committeeMapper.committeeLkupByMemoName(committeeName);
    }

    @GetMapping(value="committeeLkupByCode/{committeeCode}")
    public Committee getCommitteeByCode(@PathVariable("committeeCode") Integer committeeCode){
        return committeeMapper.committeeLkupByCode(committeeCode);
    }

    @PostMapping(value="company/lookup")
    public List<Company> getCompanies(@RequestBody Company company){
        return memberService.getCompanies(company);
    }

    @GetMapping(value = "crm/pull/{tableName}")
    public void getCrm(@PathVariable("tableName") String tableName) throws MalformedURLException {
        String tokenCredentialAuthProvider =  crmService.authenticateCrm();
        if(tokenCredentialAuthProvider == null || tokenCredentialAuthProvider.isEmpty()){
            logger.error("MEMBERSHIP CONTROLLER CRM PULL FOR " + tableName + " UNABLE TO AUTHENTICATE AAD!!! ");
        }
        else{
            crmService.getAndSyncEntity(tokenCredentialAuthProvider, tableName);
        }
    }

    //Below exists to flip a large number of records (100k+) from unread to read. This scenario DEFINITELY happens, please don't delete
    @GetMapping(value="crm/blast/{tableName}")
    public void blastCrm(@PathVariable("tableName") String tableName) throws MalformedURLException, JsonProcessingException {
        String tokenCredentialAuthProvider =  crmService.authenticateCrm();
        if(tokenCredentialAuthProvider == null || tokenCredentialAuthProvider.isEmpty()){
            logger.error("Controller CRM blast auth roken has error for " + tableName);
        }
        else{
            crmService.blastEntity(tokenCredentialAuthProvider, tableName);
        }
    }

}



