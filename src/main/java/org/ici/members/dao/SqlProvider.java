package org.ici.members.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class SqlProvider {
    public String findByClause(@Param("whereClause") String whereClause, @Param("tableName") String tableName, @Param("idColumnName") String idColumnName){
        return new SQL(){{
            SELECT(idColumnName);
            FROM(tableName);
            WHERE(whereClause);
        }}.toString();
    }
}
