package org.ici.members.db.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@MapperScan("org.ici.members.db.mappers")
public class DataSourceConfig {
    @Value("${spring.datasource.url}")
    private String datasource;

    @Bean
    public DataSource datasource(){
        return DataSourceBuilder.create()
                .driverClassName("com.sybase.jdbc4.jdbc.SybDataSource")
                .url(datasource)
                .username("bea")
                .password("apps41c1")
                .build();
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(datasource());
        return sqlSessionFactoryBean.getObject();
    }
}
