package org.ici.members.db.domain.membership;

import java.util.ArrayList;
import java.util.List;

public class AddressAbbrevWrapper {
    private List<AddressAbbrevLkup> addressAbbrevLkupList;

    public AddressAbbrevWrapper(){
        addressAbbrevLkupList = new ArrayList<>();
    }

    public List<AddressAbbrevLkup> getAddressAbbrevLkupList() {
        return addressAbbrevLkupList;
    }

    public void setAddressAbbrevLkupList(List<AddressAbbrevLkup> addressAbbrevLkupList) {
        this.addressAbbrevLkupList = addressAbbrevLkupList;
    }

    @Override
    public String toString() {
        StringBuilder listValues = new StringBuilder();
        for(AddressAbbrevLkup lkup:addressAbbrevLkupList){
            listValues.append(lkup.toString());
        }
        return "AddressAbbrevWrapper{" +
                "addressAbbrevLkupList=" + listValues +
                '}';
    }
}
