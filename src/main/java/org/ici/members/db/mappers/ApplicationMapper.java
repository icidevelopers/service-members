package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.ici.services.domain.dto.component.email.Person;
import org.ici.services.domain.dto.component.email.PnumCommittee;
import org.ici.services.domain.dto.component.email.PnumMailing;
import org.ici.services.domain.dto.component.generic.IntegerData;

import java.util.List;

@Mapper
public interface ApplicationMapper {



    @Select("CREATE TABLE #persons ( pnum INT PRIMARY KEY, email VARCHAR(100) NULL, first_name VARCHAR(100) NULL, last_name VARCHAR(100) NULL ) ")
    @Options(statementType = StatementType.CALLABLE)
    void createPnumTempTable();

    @Select("IF OBJECT_ID('tempdb..#persons') IS NOT NULL DROP TABLE #persons ")
    @Options(statementType = StatementType.CALLABLE)
    void dropPnumTempTableIfExists();

    @Select("SELECT pnum, email, first_name, last_name FROM #persons ")
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumMailing.class)
    List<Person> selectPnumTempTable();

    @Select({"<script>" +
            "   SELECT DISTINCT pnum AS pnum, code AS code " +
            "     FROM membership..commitrep cr " +
            "    WHERE cr.pnum IN " +
            "          <foreach item='filterlistItem' index='index' collection='filterlist' open='(' separator=',' close=')'> " +
            "            #{filterlistItem} " +
            "          </foreach> " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumCommittee.class)
    List<PnumCommittee> getPnumCommitteeIN(@Param("filterlist") List<Integer> pnum);

    @Select({"<script>" +
            "   SELECT DISTINCT cr.pnum AS pnum, cr.code AS code " +
            "     FROM membership..commitrep cr, #persons p " +
            "    WHERE cr.pnum = p.pnum " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumCommittee.class)
    List<PnumCommittee> getPnumCommitteeJOIN();

    @Select({"<script>" +
            "   SELECT DISTINCT mr.pnum AS pnum, m.code AS code " +
            "     FROM membership..mailing m, membership..mailrecip mr " +
            "    WHERE mr.pnum IN " +
            "          <foreach item='filterlistItem' index='index' collection='filterlist' open='(' separator=',' close=')'> " +
            "            #{filterlistItem} " +
            "          </foreach> " +
            "      AND mr.code = m.code " +
            "      AND UPPER(m.status) = 'A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumMailing.class)
    List<PnumMailing> getPnumMailingIN(@Param("filterlist") List<Integer> pnum);

    @Select({"<script>" +
            "   SELECT DISTINCT mr.pnum AS pnum, m.code AS code " +
            "     FROM membership..mailing m, membership..mailrecip mr, #persons p " +
            "    WHERE mr.pnum = p.pnum " +
            "      AND mr.code = m.code " +
            "      AND UPPER(m.status) = 'A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumMailing.class)
    List<PnumMailing> getPnumMailingJOIN();

    @Select({"<script>" +
            "   SELECT p.pnum AS pnum, es.code AS code " +
            "     FROM " +
            "       membership..compcontact cc, " +
            "       membership..ESpp es, " +
            "       membership..mailing m, " +
            "       membership..person p " +
            "    WHERE " +
            "          cc.cnum = es.cnum " +
            "      AND es.code = m.code " +
            "      AND UPPER(m.status) = 'A' " +
            "      AND p.pnum = cc.pnum " +
            "      AND upper(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.pnum = #{pnum} " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumMailing.class)
    List<PnumMailing> getPnumEnterpriseSubscriberMailings(Integer pnum);

    @Select({"<script>" +
            "   SELECT p.pnum AS pnum, es.code AS code " +
            "     FROM " +
            "       membership..compcontact cc, " +
            "       membership..ESpp es, " +
            "       membership..mailing m, " +
            "       membership..person p " +
            "    WHERE " +
            "          cc.cnum = es.cnum " +
            "      AND es.code = m.code " +
            "      AND UPPER(m.status) = 'A' " +
            "      AND p.pnum = cc.pnum " +
            "      AND upper(p.enterprise_subscriber_lite) = 'N' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(PnumMailing.class)
    List<PnumMailing> getPnumsEnterpriseSubscriberMailings();

}
