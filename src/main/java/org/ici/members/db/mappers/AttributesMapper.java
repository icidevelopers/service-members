package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.ici.services.domain.dto.component.email.Person;

@Mapper
public interface AttributesMapper {

    @Select({"<script>                            " +
            "   INSERT INTO #persons              " +
            "   SELECT                            " +
            "       p.pnum as pnum,               " +
            "       p.email AS email,             " +
            "       p.fname As first_name,        " +
            "       p.lname AS last_name          " +
            "     FROM                            " +
            "       membership..person p,         " +
            "       membership..mailing m,        " +
            "       membership..mailrecip mr      " +
            "     WHERE                           " +
            "           mr.code = m.code          " +
            "       AND mr.pnum = p.pnum          " +
            "       AND m.name = #{mailingName}   " +
            "       AND IsNull(p.email, '') != '' " +
            "       AND p.status='A'              " +
            "</script>                            "
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    void getPersonByMailing(String mailingName);

}
