package org.ici.members.db.mappers;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.ici.services.domain.dto.membership.Committee;

@Mapper
public interface CommitteeMapper {
    @Select("select code," +
            "type," +
            "name," +
            "appointed," +
            "description," +
            "balance," +
            "coord_enumber," +
            "department," +
            "NOT_USED," +
            "members_per_complex," +
            "print_order," +
            "requested_by," +
            "comments," +
            "status," +
            "status_date," +
            "verification_report_exclude," +
            "finder_visible," +
            "memo_registration_name as 'memoRegistrationName'," +
            "max_mail_designees," +
            "max_web_designees," +
            "coord_enumber2," +
            "invitation_only as invitationOnly," +
            "report_instructions " +
            "from membership..committee " +
            "where " +
            "lower(memo_registration_name) = lower(#{committeeMemoName})")
    public Committee committeeLkupByMemoName(String committeeMemoName);

    @Select("select code," +
            "type," +
            "name," +
            "appointed," +
            "description," +
            "balance," +
            "coord_enumber," +
            "department," +
            "NOT_USED," +
            "members_per_complex," +
            "print_order," +
            "requested_by," +
            "comments," +
            "status," +
            "status_date," +
            "verification_report_exclude," +
            "finder_visible," +
            "memo_registration_name as 'memoRegistrationName'," +
            "max_mail_designees as maxMailDesignees," +
            "max_web_designees," +
            "coord_enumber2," +
            "invitation_only as invitationOnly," +
            "report_instructions " +
            "from membership..committee " +
            "where " +
            "code = #{committeeMemoName}")
    public Committee committeeLkupByCode(Integer committeeCode);
}
