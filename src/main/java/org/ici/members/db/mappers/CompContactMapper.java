package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.ici.members.db.domain.membership.Compcontact;

import java.util.List;

@Mapper
public interface CompContactMapper {

    @Select("select purpose, title, cnum, pnum, entry_date, entry_user, update_date, update_user, elect_date from membership..compcontact where pnum = #{pnum}")
    public List<Compcontact> getCompanyContactFromPnum(Integer pnum);


}
