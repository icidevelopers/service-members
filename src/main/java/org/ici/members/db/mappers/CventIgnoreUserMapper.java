package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.*;
import org.ici.services.domain.dto.membership.CventIgnoreUser;

import java.util.List;
@Mapper
public interface CventIgnoreUserMapper {

    @Select("select event_id, id from membership..cvent_ignore_user")
    @Results(value = {
            @Result(property = "eventId", column="event_id"),
            @Result(property = "id", column = "id")
    })
    public List<CventIgnoreUser> getListOfIgnoredUsers();

    @Insert("insert into membership..cvent_ignore_user (event_id, id) values " +
            "(#{eventId,javaType=String, jdbcType=VARCHAR}," +
            "#{id,javaType=String, jdbcType=VARCHAR})")
    public void addUser(CventIgnoreUser ignoreUser);
}
