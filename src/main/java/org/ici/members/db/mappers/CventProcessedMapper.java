package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.ici.members.db.domain.membership.CventProcessed;
import org.ici.services.domain.dto.membership.CventIgnoreUser;

import java.util.List;

@Mapper
public interface CventProcessedMapper {

    @Insert("insert into membership..cvent_processed (email, eventid) values " +
            "(#{email,javaType=String, jdbcType=VARCHAR}," +
            "#{eventid,javaType=String, jdbcType=VARCHAR})")
    public void addUser(CventProcessed processedUser);

    @Select("select email, eventid, dateprocessed " +
            "from membership..cvent_processed " +
            "where eventid = #{eventId}")
    List<CventProcessed> getUserByEvent(String eventId);
}
