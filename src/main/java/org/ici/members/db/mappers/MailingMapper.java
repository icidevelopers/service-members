package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.ici.members.db.domain.membership.Mailrecip;
import org.ici.services.domain.dto.cvent.ici.CventCommitteeEventLkup;
import org.ici.services.domain.dto.cvent.ici.CventEventLkup;
import org.ici.services.domain.dto.membership.CommitteeMeetingAttendee;

import java.util.List;

@Mapper
public interface MailingMapper {

    @Select("select code, event_title from membership..cvent_event_lkup where event_title = #{eventTitle}")
    public CventEventLkup getICICventEventLkup(String eventTitle);

    @Select("select meeting_id, cvent_title from membership..cvent_committee_meeting_lkup where cvent_title = #{eventTitle}")
    public CventCommitteeEventLkup getCommitteeEventLkup(String eventTitle);

    @Select("select meeting_id, pnum, cnum from membership..committee_meeting_attendees where meeting_id=#{meetingId}")
    public CommitteeMeetingAttendee getCommitteeMeetingAttendee(Integer meetingId);

    @Insert("insert into membership..committee_meeting_attendees (meeting_id, pnum, cnum) values(#{meetingId}, #{pnum}, #{cnum})")
    public void addCommitteeMeetingAttendee(Integer meetingId, Integer pnum, Integer cnum);

    @Insert("insert into membership..mailrecip (pnum, code, copies) values (#{pnum}, #{code}, 1)")
    void addMailing(Integer code, Integer pnum);
    @Select("select     copies,\n" +
            "    cnum,\n" +
            "    billpnum,\n" +
            "    department,\n" +
            "    entry_date as entryDate,\n" +
            "    entry_user as entryUser,\n" +
            "    start_date as startDate,\n" +
            "    end_date as endDate,\n" +
            "    discount,\n" +
            "    complimentary_end_date as complimentaryEndDate,\n" +
            "    complimentary_reason as complimentaryReason,\n" +
            "    comments,\n" +
            "    fee_status as feeStatus,\n" +
            "    billmonth,\n" +
            "    cancel_date as cancelDate,\n" +
            "    initial_date as initialDate from membership..mailrecip where code = #{code} and pnum = #{pnum}")
    List<Mailrecip> getMailing(Integer code, Integer pnum);
}
