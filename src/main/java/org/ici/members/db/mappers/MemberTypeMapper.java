package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.ici.services.domain.dto.component.generic.IntegerData;

import java.util.List;

@Mapper
public interface MemberTypeMapper {

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..company c, " +
            "       membership..current_membership m, " +
            "       membership..compcontact cc, " +
            "       membership..person p " +
            "     WHERE " +
            "           c.cnum = m.cnum " +
            "       AND c.cnum = cc.cnum " +
            "       AND m.type = 1 " +
            "       AND ((c.type = 1 and cc.purpose = 11) " +
            "            OR (c.type = 6 and cc.purpose = 1)) " +
            "       AND p.pnum = cc.pnum " +
            "       AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeFullMember();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc, " +
            "       membership..company c " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND cc.cnum = c.cnum " +
            "      AND c.enterprise_subscriber = 'a' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeEnterpriseSubscriber();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p " +
            "    WHERE " +
            "          (email like '%@ici.org' or " +
            "           email like '%@icimutual.com' or " +
            "           email like '%iciglobal.org') " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeICIStaff();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..commitrep cr, " +
            "       membership..person p " +
            "    WHERE " +
            "          cr.code = 4 " +
            "      AND bogclass IN (1,2,3,4) " +
            "      AND p.pnum = cr.pnum " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeBoardOfGovernors();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND p.block_mailings = 'n' " +
            "      AND cc.purpose IN (8, 17) " +
            "      AND exists (select type from membership..current_membership cm where cm.cnum = cc.cnum and cm.type = 1) " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeIndependentDirector();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND p.block_mailings = 'n' " +
            "      AND cc.purpose IN (9, 17) " +
            "      AND exists (select type from membership..current_membership cm where cm.cnum = cc.cnum and cm.type = 1) " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeInterestedDirector();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p " +
            "    WHERE " +
            "          p.pnum IN (SELECT DISTINCT(pnum) FROM membership..commitrep cr, membership..committee c where cr.code = c.code and c.type not in (5,6)) " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeCommitteeMember();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..company, " +
            "       membership..compcontact, " +
            "       membership..current_membership, " +
            "       membership..person p " +
            "    WHERE ( membership..compcontact.cnum = membership..company.cnum ) AND " +
            "          ( membership..current_membership.cnum = membership..company.cnum ) AND " +
            "          ( p.pnum = membership..compcontact.pnum ) AND " +
            "          ( ( membership..compcontact.purpose = 1 ) AND ( membership..current_membership.type = 2 ) ) " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeAssociateMember();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc, " +
            "       membership..current_membership cm " +
            "    WHERE cc.cnum = cm.cnum " +
            "      AND ( cm.type = 6 or cm.type = 7 ) " +
            "      AND p.pnum = cc.pnum " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeGlobalMember();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc, " +
            "       membership..company c, " +
            "       membership..current_membership cm " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND cc.cnum = c.cnum " +
            "      AND cc.purpose = 11 " +
            "      AND (c.has_global_funds = 'c' OR c.has_global_funds = 'g') " +
            "      AND cc.cnum = cm.cnum " +
            "      AND cm.type = 1 " +
            "      AND cm.member = 'Y' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeBroadGlobalMember();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p " +
            "    WHERE " +
            "          p.pnum IN (SELECT DISTINCT(pnum) FROM membership..commitrep cr, membership..committee c where cr.code = c.code and c.type in (5,6)) " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeGlobalCommitteeMember();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 624 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeGlobalDailyNewsletterMailing();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 615 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeExcludeFromICIGlobalDailyMailing();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 404 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeExcludeFromDomesticDailyMailing();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 1027 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeExcludeFromBrexitBriefing();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..commitrep cr " +
            "    WHERE " +
            "          p.pnum = cr.pnum " +
            "      AND cr.code = 44 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeInternationalCommittee();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..commitrep cr " +
            "    WHERE " +
            "          p.pnum = cr.pnum " +
            "      AND cr.code = 168 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeInternationalInvestingSubcommittee();

    @Select({"<script>" +
            "   SELECT DISTINCT p.pnum as number " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..commitrep cr " +
            "    WHERE " +
            "          p.pnum = cr.pnum " +
            "      AND cr.code = 146 " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(IntegerData.class)
    List<IntegerData> getMembershipTypeInternationalOperationsAdvisoryCommittee();

}
