package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.ici.services.domain.dto.component.email.Person;

import java.util.List;

@Mapper
public interface MemberTypeRequireEmailMapper {

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..company c, " +
            "       membership..current_membership m, " +
            "       membership..compcontact cc, " +
            "       membership..person p " +
            "     WHERE " +
            "           c.cnum = m.cnum " +
            "       AND c.cnum = cc.cnum " +
            "       AND m.type = 1 " +
            "       AND ((c.type = 1 and cc.purpose = 11) " +
            "            OR (c.type = 6 and cc.purpose = 1)) " +
            "       AND p.pnum = cc.pnum " +
            "       AND IsNull(p.email, '') != '' " +
            "       AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "       AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    void getMembershipTypeFullMember();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc, " +
            "       membership..company c " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND cc.cnum = c.cnum " +
            "      AND c.enterprise_subscriber = 'a' " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeEnterpriseSubscriber();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p " +
            "    WHERE " +
            "          (email like '%@ici.org' or " +
            "           email like '%@icimutual.com' or " +
            "           email like '%iciglobal.org') " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeICIStaff();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..commitrep cr, " +
            "       membership..person p " +
            "    WHERE " +
            "          cr.code = 4 " +
            "      AND bogclass IN (1,2,3,4) " +
            "      AND p.pnum = cr.pnum " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeBoardOfGovernors();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND p.block_mailings = 'n' " +
            "      AND cc.purpose IN (8, 17) " +
            "      AND exists (select type from membership..current_membership cm where cm.cnum = cc.cnum and cm.type = 1) " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeIndependentDirector();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND p.block_mailings = 'n' " +
            "      AND cc.purpose IN (9, 17) " +
            "      AND exists (select type from membership..current_membership cm where cm.cnum = cc.cnum and cm.type = 1) " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeInterestedDirector();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p " +
            "    WHERE " +
            "          p.pnum IN (SELECT DISTINCT(pnum) FROM membership..commitrep cr, membership..committee c where cr.code = c.code and c.type not in (5,6)) " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeCommitteeMember();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..company, " +
            "       membership..compcontact, " +
            "       membership..current_membership, " +
            "       membership..person p " +
            "    WHERE ( membership..compcontact.cnum = membership..company.cnum ) AND " +
            "          ( membership..current_membership.cnum = membership..company.cnum ) AND " +
            "          ( p.pnum = membership..compcontact.pnum ) AND " +
            "          ( ( membership..compcontact.purpose = 1 ) AND ( membership..current_membership.type = 2 ) ) " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeAssociateMember();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc, " +
            "       membership..current_membership cm " +
            "    WHERE cc.cnum = cm.cnum " +
            "      AND ( cm.type = 6 or cm.type = 7 ) " +
            "      AND p.pnum = cc.pnum " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeGlobalMember();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..compcontact cc, " +
            "       membership..company c, " +
            "       membership..current_membership cm " +
            "    WHERE " +
            "          p.pnum = cc.pnum " +
            "      AND cc.cnum = c.cnum " +
            "      AND cc.purpose = 11 " +
            "      AND (c.has_global_funds = 'c' OR c.has_global_funds = 'g') " +
            "      AND cc.cnum = cm.cnum " +
            "      AND cm.type = 1 " +
            "      AND cm.member = 'Y' " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeBroadGlobalMember();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p " +
            "    WHERE " +
            "          p.pnum IN (SELECT DISTINCT(pnum) FROM membership..commitrep cr, membership..committee c where cr.code = c.code and c.type in (5,6)) " +
            "      AND IsNull(p.email, '') != '' " +
            "       AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeGlobalCommitteeMember();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 624 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeGlobalDailyNewsletterMailing();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 615 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeExcludeFromICIGlobalDailyMailing();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 404 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeExcludeFromDomesticDailyMailing();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..mailrecip mr " +
            "    WHERE " +
            "          p.pnum = mr.pnum " +
            "      AND mr.code = 1027 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeExcludeFromBrexitBriefing();

    @Select({"<script>" +
             "   INSERT INTO #persons " +
             "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name " +
             "     FROM " +
             "       membership..person p, " +
             "       membership..mailrecip mr " +
             "    WHERE " +
             "          p.pnum = mr.pnum " +
            "       AND mr.code = 778" +
             "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeExcludeFromDigestMailing();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..commitrep cr " +
            "    WHERE " +
            "          p.pnum = cr.pnum " +
            "      AND cr.code = 44 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeInternationalCommittee();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..commitrep cr " +
            "    WHERE " +
            "          p.pnum = cr.pnum " +
            "      AND cr.code = 168 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeInternationalInvestingSubcommittee();

    @Select({"<script>" +
            "   INSERT INTO #persons " +
            "   SELECT DISTINCT p.pnum as pnum, p.email AS email, p.fname As first_name, p.lname AS last_name   " +
            "     FROM " +
            "       membership..person p, " +
            "       membership..commitrep cr " +
            "    WHERE " +
            "          p.pnum = cr.pnum " +
            "      AND cr.code = 146 " +
            "      AND IsNull(p.email, '') != '' " +
            "      AND UPPER(p.enterprise_subscriber_lite) = 'N' " +
            "      AND p.status='A' " +
            "</script>"
    })
    @Options(statementType = StatementType.CALLABLE)
    @ResultType(Person.class)
    List<Person> getMembershipTypeInternationalOperationsAdvisoryCommittee();

}
