package org.ici.members.db.mappers;

import org.apache.ibatis.annotations.*;
import org.ici.members.dao.SqlProvider;
import org.ici.members.db.domain.MailingCode;
import org.ici.members.db.domain.membership.*;
import org.ici.services.domain.dto.cvent.CventPersonDatum;
import org.ici.services.domain.users.PersonRequest;


import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select pnum, prefix, fname, lname, middle, title, party, dcrep, comment, DROP_phone, fax, suffix, " +
            "    department, in_process, email, status, status_date, rcvattach, DROP_complimentary, upperlname, " +
            "    complimentary_end_date, complimentary_reason, phone, person_salutation, entry_date, entry_user, " +
            "    update_date, update_user, complimentary, billpnum, committee_mail_type, other_mail_type, suspend_email, " +
            "    legal_mail_type, mail_type_response, block_mailings, mail_immediate, mail_frequency, " +
            "    assistant_pnum, cell_phone  as cellPhone, enterprise_subscriber_lite " +
            "from membership..person where pnum = #{pnum}")
    Person getPersonByPnum(Integer pnum);

    @Select("select pnum, active, elected, termend, chair, bogclass, code, cnum, title, rank_order, entry_date, " +
            "    entry_user, workgrp_class, designating_pnum, committee_rep_code" +
            " from membership..commitrep where pnum = #{pnum}")
    List<Commitrep> getCommittesByPnum(Integer pnum);

    @Select("select pnum, adtype, street1, street2, city, state, zip, country, cnum, begin_date, end_date, residence, " +
            "    zip5, company_name, company_abname, fundid, parentid, street1_long, street2_long, company_type, " +
            "    entry_date, entry_user, update_date, update_user, phone  " +
            "from membership..address_view where pnum = #{pnum}")
    List<Address> getAddressViewByPnum(Integer pnum);

    @Select("select " +
            "pnum, " +
            "street1, " +
            "street2, " +
            "adtype, " +
            "begin_date as 'beginDate', " +
            "city, " +
            "state, " +
            "zip, " +
            "country, " +
            "cnum, " +
            "end_date, " +
            "residence, " +
            "zip5, " +
            "company_name, " +
            "company_abname, " +
            "fundid, " +
            "parentid, " +
            "street1_long, " +
            "street2_long, " +
            "company_type,  " +
            "entry_date, " +
            "entry_user, " +
            "update_date, " +
            "update_user, " +
            "phone " +
            "from membership..address_view where pnum = #{pnum}")
    List<Address> getAddressByPnum(Integer pnum);

    @Select("select distinct m.code, m.name from membership..mailrecip mr, membership..mailing m where mr.code = m.code and upper(m.status) = 'A' and mr.pnum = #{pnum}")
    List<MailingCode> getMailingsByPnum(Integer pnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, membership..address a " +
            "where p.pnum = a.pnum and a.cnum in (9028, 25198) and upper(p.status) = 'A' and p.pnum = #{pnum}")
    Person isEmployee(Integer pnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..company c,  " +
            "    membership..current_membership m,  " +
            "    membership..compcontact cc,  " +
            "    membership..person p  " +
            "where c.cnum = m.cnum  " +
            "    and c.cnum = cc.cnum  " +
            "    and m.type = 1  " +
            "    and (c.type = 1 and cc.purpose = 11)  " +
            "    and p.pnum = cc.pnum  " +
            "    and upper(p.status) = 'A' " +
            "    and p.pnum = #{pnum}")
    Person isMember(Integer pnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, " +
            "    membership..compcontact cc " +
            "where p.pnum = cc.pnum  " +
            "    and cc.purpose in (8, 9, 17)  " +
            "    and exists (select type from  membership..current_membership cm where cm.cnum = cc.cnum and cm.type = 1)   " +
            "    and upper(p.status) = 'A' " +
            "    and p.pnum = #{pnum}")
    Person isIndependentDirector(Integer pnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..company c,  " +
            "    membership..compcontact cc,  " +
            "    membership..current_membership cm,  " +
            "    membership..person p  " +
            "where( cc.cnum = c.cnum )  " +
            "    and cm.cnum = c.cnum " +
            "    and cc.purpose = 1 " +
            "    and cm.type = 2 " +
            "    and cc.pnum = p.pnum " +
            "    and upper(p.status) = 'A' " +
            "    and p.pnum = #{pnum}")
    Person isAssociateMember(Integer pnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p,  " +
            "    membership..compcontact cc,  " +
            "    membership..company c  " +
            "where upper(p.status) = 'A'  " +
            "    and p.pnum = cc.pnum   " +
            "    and cc.cnum = c.cnum  " +
            "    and upper(c.enterprise_subscriber) = 'A'  " +
            "    and upper(p.enterprise_subscriber_lite) = 'N' " +
            "    and p.pnum = #{pnum}")
    Person isEnterpriseSubscriber(Integer pnum);

    @Select("select mr.pnum, mr.code, mr.copies, mr.cnum, mr.billpnum, mr.department, mr.entry_date, mr.entry_user, mr.start_date, " +
            "    mr.end_date, mr.discount, mr.complimentary_end_date, mr.complimentary_reason, mr.comments, mr.fee_status, " +
            "    mr.billmonth, mr.cancel_date, mr.initial_date  " +
            "from membership..mailrecip mr, " +
            "    membership..mailing m " +
            "where mr.code = m.code " +
            "    and m.mailing_type = 2 " +
            "    and upper(m.status) = 'A' " +
            "    and mr.pnum = #{pnum}")
    List<Mailrecip> getStatisticalSubscribtions(Integer pnum);

    @Select("select mr.pnum, mr.code, mr.copies, mr.cnum, mr.billpnum, mr.department, mr.entry_date, mr.entry_user, mr.start_date, " +
            "    mr.end_date, mr.discount, mr.complimentary_end_date, mr.complimentary_reason, mr.comments, mr.fee_status, " +
            "    mr.billmonth, mr.cancel_date, mr.initial_date  " +
            "from membership..mailrecip mr, " +
            "    membership..mailing m " +
            "where mr.code = m.code " +
            "    and m.mailing_type = 9 " +
            "    and upper(m.status) = 'A' " +
            "    and mr.pnum = #{pnum}")
    List<Mailrecip> getPersonEvents(Integer pnum);

    @Select("select es.code, m.name " +
            "from membership..compcontact cc,  " +
            "    membership..ESpp es, " +
            "    membership..mailing m, " +
            "    membership..person p " +
            "where cc.cnum = es.cnum  " +
            "    and es.code = m.code " +
            "    and upper(m.status) = 'A' " +
            "    and p.pnum = cc.pnum " +
            "    and upper(p.enterprise_subscriber_lite) = 'N' " +
            "    and cc.pnum = #{pnum}")
    List<MailingCode> getEnterpriseSubscriberMailings(int pnum);

    @Insert("insert into membership..person_request (pnum, " +
            "   prefix, " +
            "   fname, " +
            "   mname, " +
            "   lname, " +
            "   street1, " +
            "   street2, " +
            "   city, " +
            "   state, " +
            "   zip, " +
            "   phone, " +
            "   mobile, " +
            "   email, " +
            "   company_name, " +
            "   title, " +
            "   entry_date," +
            "   status," +
            "   country) " +
            "values (#{pnum}," +
            "   #{prefix}," +
            "   #{fname}," +
            "   #{mname}," +
            "   #{lname}," +
            "   #{street1}," +
            "   #{street2}," +
            "   #{city}," +
            "   #{state}," +
            "   #{zip}," +
            "   #{phone}," +
            "   #{mobile}," +
            "   #{email}," +
            "   #{companyName}," +
            "   #{title}," +
            "   getdate()," +
            "   0," +
            "   #{iciCountryCode})")
    void addEnrollment(PersonRequest person);

    @Select("select distinct c.name, c.cnum " +
            "from membership..compcontact cc, membership..company c " +
            "where cc.cnum = c.cnum and " +
            "    cc.pnum = #{pnum} " +
            "    and cc.purpose = 1")
    List<Company> getCompanyByPnum(int pnum);

    @Select("select c.name, c.cnum from membership..company c where lower(c.active) = 'y' and c.cnum = #{cnum}")
    Company getCompanyByCnum(Integer cnum);

    @Select("SELECT distinct c.name, c.cnum " +
            "FROM membership..company c,   " +
            "     membership..current_membership cm " +
            "WHERE cm.cnum = c.cnum " +
            "     and c.type = 1 " +
            "     and cm.type = 1")
    Company getCompleteCompanyList();

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, research..surveys s, research..csurvey c " +
            "where s.snum = c.snum and   " +
            "    c.pnum = p.pnum and " +
            "    s.snum in (20,24,44,46) and " +
            "    p.pnum =  #{pnum}")
    Person getHasVerifyByPnum(Integer pnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..compcontact c, membership..person p " +
            "where c.pnum = p.pnum " +
            "  and c.purpose = 2 " +
            "  and lower(p.status) = 'a' " +
            "  and c.cnum = #{cnum}")
    Person getCompanyPrimaryContact(Integer cnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..compcontact c, membership..person p " +
            "where c.pnum = p.pnum " +
            "  and c.purpose = 10 " +
            "  and lower(p.status) = 'a' " +
            "  and c.cnum = #{cnum}")
    Person getCompanyCEO(Integer cnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p , membership..commitrep cr, membership..company c " +
            "where cr.code = 4  " +
            "  and cr.bogclass in (1,2,3,4)  " +
            "  and cr.cnum = c.cnum " +
            "  and c.type = 1  " +
            "  and cr.pnum = p.pnum  " +
            "  and lower(p.status) = 'a' " +
            "  and cr.cnum = #{cnum}")
    List<Person> getCompanyBoardMembers(Integer cnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, membership..commitrep cr " +
            "where cr.code = 46 " +
            "  and cr.pnum = p.pnum " +
            "  and lower(cr.active) = 'y'  " +
            "  and lower(p.status) = 'a' " +
            "  and cr.cnum = #{cnum}")
    List<Person> getCompanyIdcCouncil(Integer cnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, membership..commitrep cr " +
            "where cr.code = 219 " +
            "  and cr.pnum = p.pnum " +
            "  and lower(cr.active) = 'y'  " +
            "  and lower(p.status) = 'a' " +
            "  and cr.cnum = #{cnum}")
    List<Person> getCompanyAtlanticCouncil(Integer cnum);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, membership..commitrep cr " +
            "where cr.code = 218 " +
            "  and cr.pnum = p.pnum " +
            "  and lower(cr.active) = 'y'  " +
            "  and lower(p.status) = 'a' " +
            "  and cr.cnum = #{cnum}")
    List<Person> getCompanyPacificCouncil(Integer cnum);

    @Select("select distinct c.name as committee, p.fname, p.lname " +
            "from membership..person p, membership..commitrep cr, membership..committee c " +
            "where cr.code = c.code " +
            "  and cr.pnum = p.pnum  " +
            "  and lower(cr.active) = 'y'  " +
            "  and lower(p.status) = 'a' " +
            "  and lower(c.status) = 'a' " +
            "  and cr.cnum = #{cnum}")
    List<CommitteePerson> getCompanyCommitteeMembers(Integer cnum);


    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p, " +
            "  membership..company c, " +
            "  membership..compcontact cc " +
            "where cc.cnum = c.cnum " +
            "  and p.pnum = cc.pnum " +
            "  and upper(p.status) = 'A' " +
            "  and isnull(p.email, '') != ''" +
            "  and p.pnum >= #{start}" +
            "  and p.pnum <= #{end} " +
            "order by p.pnum ")
    List<Person> getPotentialWebsiteUsers(Integer start, Integer end);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..compcontact c, membership..person p " +
            "where c.pnum = p.pnum " +
            "    and c.purpose = 1 " +
            "    and lower(p.status) = 'a' " +
            "    and c.cnum = 7846 " +
            "    and c.pnum = #{pnum}")
    List<Person> isIciMutualMember(int pnum);


    @Select("select " +
            "pnum, " +
            "prefix, " +
            "fname, " +
            "lname, " +
            "middle, " +
            "title, " +
            "party, " +
            "dcrep, " +
            "comment, " +
            "DROP_phone, " +
            "fax, " +
            "suffix, " +
            "department, " +
            "in_process, " +
            "email, " +
            "status, " +
            "status_date, " +
            "rcvattach, " +
            "DROP_complimentary, " +
            "upperlname, " +
            "complimentary_end_date, " +
            "complimentary_reason, " +
            "phone, " +
            "person_salutation, " +
            "entry_date, " +
            "entry_user, " +
            "update_date, " +
            "update_user, " +
            "complimentary, " +
            "billpnum, " +
            "committee_mail_type, " +
            "other_mail_type, " +
            "suspend_email, " +
            "legal_mail_type, " +
            "mail_type_response, " +
            "block_mailings, " +
            "mail_immediate, " +
            "mail_frequency, " +
            "assistant_pnum, " +
            "cell_phone as cellPhone, " +
            "enterprise_subscriber_lite " +
            "from membership..person " +
            "where upper(status) = 'A' " +
            "and email = #{email}")
    Person getPersonByEmail(String email);

    @Select("select primary_street_suffix_name as 'primaryStreetSuffixName'," +
            "       common_street_suffix_name as 'commonStreetSuffixName'," +
            "       postal_street_suffix_abbrev as 'postalStreetSuffixAbbrev'," +
            "       address_abbrev_key as 'addressAbbrevKey'" +
            "       from membership..address_abbrev_lkup")
    List<AddressAbbrevLkup> getAddressAbbrev();

    @Select("select code,value from membership..country_list_lkup where value = #{country}")
    CountryListLkup getCountryInfo(String country);

    @Select("select code,value,accronym from membership..country_list_drupal_lkup where value = #{country}")
    CountryListDrupalLkup getCountryDrupalInfo(String country);

    @Select("select code,value,accronym from membership..country_list_drupal_lkup where accronym = #{country}")
    CountryListDrupalLkup getCountryIciInfo(String country);

    @Select("select code,value from membership..country_list_lkup where code = #{countryId}")
    CountryListLkup getCountryInfoFromId(int countryId);

    @Select("select distinct p.pnum, p.prefix, p.fname, p.lname, p.middle, p.title, p.party, p.dcrep, p.comment, p.DROP_phone, p.fax, p.suffix, " +
            "    p.department, p.in_process, p.email, p.status, p.status_date, p.rcvattach, p.DROP_complimentary, p.upperlname, " +
            "    p.complimentary_end_date, p.complimentary_reason, p.phone, p.person_salutation, p.entry_date, p.entry_user, " +
            "    p.update_date, p.update_user, p.complimentary, p.billpnum, p.committee_mail_type, p.other_mail_type, p.suspend_email, " +
            "    p.legal_mail_type, p.mail_type_response, p.block_mailings, p.mail_immediate, p.mail_frequency, " +
            "    p.assistant_pnum, p.cell_phone  as cellPhone, p.enterprise_subscriber_lite " +
            "from membership..person p " +
            "where p.fname = #{cventPersonDatum.firstName} " +
            "and p.lname = #{cventPersonDatum.lastName} " +
            "and p.title = #{cventPersonDatum.title}" +
            "and p.middle = substring(#{cventPersonDatum.middleName},1,1)" )
    Person getPersonByCriteria(CventPersonDatum cventPersonDatum);

    @Update("update membership..person set " +
            "prefix = #{prefix, javaType=String, jdbcType=VARCHAR},\n" +
            "fname= #{fname, javaType=String, jdbcType=VARCHAR},\n" +
            "lname= #{lname, javaType=String, jdbcType=VARCHAR},\n" +
            "middle= #{middle, javaType=String, jdbcType=VARCHAR},\n" +
            "title= #{title, javaType=String, jdbcType=VARCHAR},\n" +
            "email= #{email, javaType=String, jdbcType=VARCHAR},\n" +
            "cell_phone= #{cellPhone, javaType=String, jdbcType=VARCHAR},\n" +
            "phone= #{phone, javaType=String, jdbcType=VARCHAR}\n" +
            "where pnum = #{pnum}")
    int updatePerson(Person person);

    @Update("update membership..address set " +
            "street1 = #{street1,javaType=String, jdbcType=VARCHAR},\n" +
            "street2= #{street2,javaType=String, jdbcType=VARCHAR},\n" +
            "city = #{city,javaType=String, jdbcType=VARCHAR},\n" +
            "state = #{state,javaType=String, jdbcType=VARCHAR},\n" +
            "zip = #{zip,javaType=String, jdbcType=VARCHAR},\n" +
            "country = #{country,javaType=String, jdbcType=VARCHAR}\n" +
            "where pnum = #{pnum} \n" +
            "and adtype = #{adtype} \n" +
            "and begin_date = #{beginDate,javaType=java.util.Date, jdbcType=TIMESTAMP}")
    int updateAddress(Address address);

    @SelectProvider(type= SqlProvider.class, method = "findByClause")
    List<Integer> getPersonByRequest(String whereClause, String tableName, String idColumnName);
}
