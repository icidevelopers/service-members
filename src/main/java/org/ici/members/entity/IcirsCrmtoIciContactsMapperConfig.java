package org.ici.members.entity;

import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTSES.Value;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.extensions.spring.SpringMapperConfig;

//@MapperConfig(componentModel="spring", uses = ConversionService)
@SpringMapperConfig(conversionServiceAdapterPackage = "org.ici.members.entity")
public interface IcirsCrmtoIciContactsMapperConfig {

}
