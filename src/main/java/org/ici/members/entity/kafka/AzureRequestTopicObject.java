package org.ici.members.entity.kafka;

import org.springframework.http.HttpEntity;

public class AzureRequestTopicObject {
    private String url;
    private HttpEntity entity;
    private String idVal;
    public AzureRequestTopicObject(){}

    public AzureRequestTopicObject(String url, HttpEntity entity, String idValue) {
        this.url = url;
        this.entity = entity;
        this.idVal = idValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpEntity getEntity() {
        return entity;
    }

    public void setEntity(HttpEntity entity) {
        this.entity = entity;
    }

    public String getIdVal() {
        return idVal;
    }

    public void setIdVal(String idVal) {
        this.idVal = idVal;
    }
}
