package org.ici.members.entity.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class AzureRequestTopicObjectDeserializer implements Deserializer<AzureRequestTopicObject> {
    private ObjectMapper objectMapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(AzureRequestTopicObjectDeserializer.class);

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Deserializer.super.configure(configs, isKey);
    }

    @Override
    public AzureRequestTopicObject deserialize(String s, byte[] bytes) {
        try {
        if(bytes == null){
            logger.error("input byte array is null");
            return null;
        }
            return objectMapper.readValue(new String(bytes, "UTF-8"), AzureRequestTopicObject.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        Deserializer.super.close();
    }
}
