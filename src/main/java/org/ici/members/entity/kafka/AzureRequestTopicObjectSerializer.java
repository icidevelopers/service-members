package org.ici.members.entity.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


public class AzureRequestTopicObjectSerializer implements Serializer<AzureRequestTopicObject> {
    private final ObjectMapper mapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(AzureRequestTopicObjectSerializer.class);

    @Override
    public void configure(Map configs, boolean isKey){}


    @Override
    public byte[] serialize(String s, AzureRequestTopicObject azureRequestTopicObject) {
        try {
        if(azureRequestTopicObject == null){
            logger.error("AzureRequestTopicObject is null");
            return null;
        }
            return mapper.writeValueAsBytes(azureRequestTopicObject);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        Serializer.super.close();
    }
}
