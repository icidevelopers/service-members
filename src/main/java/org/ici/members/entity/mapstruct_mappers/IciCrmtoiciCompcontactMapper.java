package org.ici.members.entity.mapstruct_mappers;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import org.ici.members.entity.staging_entity.BoardCommitteeStagingEntity;
import org.ici.members.entity.staging_entity.CommitteeMeetingsStagingEntity;
import org.ici.members.entity.staging_entity.CompcontactStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Mapper(componentModel = "spring")
public interface IciCrmtoiciCompcontactMapper extends Converter<Value, CompcontactStagingEntity> {

    @Mappings({

    @Mapping(target = "dropCancelled", source = "icirsDropCancelled"),
    @Mapping(target = "purpose",source="icirsPurpose"),
    @Mapping(target = "title",source = "icirsTitle"),
    @Mapping(target = "cnum",source = "icirsCnum"),
    @Mapping(target = "pnum",source = "icirsPnum"),
    @Mapping(target = "entryDate",source = "icirsEntryDate", qualifiedByName = "convertTimestamp"),
    @Mapping(target = "entryUser",source = "icirsEntryUser"),
    @Mapping(target = "updateDate",source = "icirsUpdateDate", qualifiedByName = "convertTimestamp"),
    @Mapping(target = "updateUser",source = "icirsUpdateUser"),
    @Mapping(target = "electDate",source = "icirsElectDate", qualifiedByName = "convertDate"),
    @Mapping(target = "recordOpertaion",source = "icirsRecordoperation")

})
    CompcontactStagingEntity convert(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.Value value);



    @Named("convertDate")
    public static Date convertDate(String timestamp) {
        timestamp = timestamp == null || timestamp.isEmpty() || timestamp.isBlank() ||timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        timestamp = timestamp.split(" ")[0];
        return timestamp.isEmpty() ?null:Date.valueOf(timestamp);
    }

    @Named("convertTimestamp")
    public static Timestamp convertTimestamp(String timestamp){
        try {
            if(timestamp == null || timestamp.isEmpty() || timestamp.contains("@")){
                return null;
            }
            timestamp = timestamp.substring(0,timestamp.length()-1);
            if(timestamp.length()>1){
                timestamp = timestamp.replaceAll("T", " ");
            }
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            java.util.Date date = formatter.parse(timestamp);
            java.sql.Timestamp dateTimeStamp = new java.sql.Timestamp(date.getTime());

            return dateTimeStamp;

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
