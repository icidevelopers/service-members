package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.BoardCommitteeStagingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoBoardcommitteeMapper extends Converter<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEES.Value, BoardCommitteeStagingEntity>{
            @Mappings({
            @Mapping(target="boardCommitteeNum",source="icirsBoardcommitteenum"),
            @Mapping(target="name",source="icirsName"),
            @Mapping(target="boardCommitteeType",source="icirsBoardcommitteetype"),
            @Mapping(target="appointed",source="icirsAppointed",qualifiedByName="convertTimestamp"),
            @Mapping(target="status",source="icirsStatus"),
            @Mapping(target="statusDate",source="icirsStatusDate",qualifiedByName="convertTimestamp"),
            @Mapping(target="boardCnum",source="icirsBoardCnum"),
            @Mapping(target="entryDate",source="icirsEntryDate",qualifiedByName="convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName="convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
                    })
            BoardCommitteeStagingEntity convert(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEES.Value boardCommitteeValue);

            @Named("convertTimestamp")
            public static String convertTimestamp(String timestamp){
                    String finalTimestamp = timestamp == null?"":timestamp.substring(0,timestamp.length()-1);
                    finalTimestamp = finalTimestamp.replace("T", " ");
                    return finalTimestamp;
            }
}
