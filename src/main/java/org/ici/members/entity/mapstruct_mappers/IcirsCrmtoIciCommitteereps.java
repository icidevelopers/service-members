package org.ici.members.entity.mapstruct_mappers;


import org.ici.members.entity.staging_entity.CommitrepStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITREPS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoIciCommitteereps extends Converter<Value, CommitrepStagingEntity> {

    @Mappings({
            @Mapping(target="pnum",source="icirsPnum"),
            @Mapping(target="active",source="icirsActive"),
            @Mapping(target="elected",source="icirsElected",qualifiedByName = "convertDate"),
            @Mapping(target="termend",source="icirsTermend",qualifiedByName = "convertDate"),
            @Mapping(target="chair",source="icirsChair"),
            @Mapping(target="bogclass",source="icirsBogclass"),
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="cnum",source="icirsCnum"),
            @Mapping(target="title",source="icirsTitle"),
            @Mapping(target="rankOrder",source="icirsRankOrder"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="workgrpClass",source="icirsWorkgrpClass"),
            @Mapping(target="designatingPnum",source="icirsDesignatingPnum"),
            @Mapping(target="committeeRepCode",source="icirsCommitteeRepCode"),
            @Mapping(target="recordOperation",source="icirsRecordoperation")
    })
    CommitrepStagingEntity convert(Value value);

    @Named("convertDate")
    public static Date convertDate(String timestamp) {
        if(timestamp == null || timestamp.isEmpty() || timestamp.contains("@")){
            return null;
        }
        if(timestamp.contains(",")){
//            timestamp = timestamp.split("\\,")[0];
            DateFormat df = new SimpleDateFormat("MM/dd/yy, HH:mm:ss a");
            try {
               java.util.Date parsedDate = df.parse(timestamp);
               java.sql.Date sqlDate = new java.sql.Date(parsedDate.getTime());
               return sqlDate;
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

        }
        else {
            timestamp = timestamp.substring(0, timestamp.length() - 1);
            if (timestamp.length() > 1) {
                timestamp = timestamp.replaceAll("T", " ");
            }
            timestamp = timestamp.split(" ")[0];
            return timestamp.isEmpty() ? null : Date.valueOf(timestamp);
        }
    }

    @Named("convertTimestamp")
    public static Timestamp convertTimestamp(String timestamp){
        try {
        if(timestamp == null || timestamp.isEmpty() || timestamp.contains("@")){
            return null;
        }
        timestamp = timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        java.util.Date date = formatter.parse(timestamp);
        java.sql.Timestamp dateTimeStamp = new java.sql.Timestamp(date.getTime());

        return dateTimeStamp;

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
