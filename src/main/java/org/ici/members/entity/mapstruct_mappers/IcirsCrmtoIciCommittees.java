package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CommitteeStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEES.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoIciCommittees extends Converter<Value, CommitteeStagingEntity> {

    @Mappings({
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="type",source="icirsType"),
            @Mapping(target="name",source="icirsName"),
            @Mapping(target="appointed",source="icirsAppointed", qualifiedByName = "formatTimeString"),
            @Mapping(target="description",source="icirsDescription"),
            @Mapping(target="balance",source="icirsBalance"),
            @Mapping(target="coordEnumber",source="icirsCoordEnumber"),
            @Mapping(target="department",source="icirsDepartment"),
            @Mapping(target="notUsed",source="icirsNotUsed"),
            @Mapping(target="membersPerComplex",source="icirsMembersPerComplex"),
            @Mapping(target="printOrder",source="icirsPrintOrder"),
            @Mapping(target="requestedBy",source="icirsRequestedBy"),
            @Mapping(target="comments",source="icirsComments"),
            @Mapping(target="status",source="icirsStatus"),
            @Mapping(target="statusDate",source="icirsStatusDate", qualifiedByName = "formatTimeString"),
            @Mapping(target="verificationReportExclude",source="icirsVerificationReportExclude"),
            @Mapping(target="finderVisible",source="icirsFinderVisible"),
            @Mapping(target="memoRegistrationName",source="icirsMemoRegistrationName"),
            @Mapping(target="maxMailDesignees",source="icirsMaxMailDesignees"),
            @Mapping(target="maxWebDesignees",source="icirsMaxWebDesignees"),
            @Mapping(target="coordEnumber2",source="icirsCoordEnumber2"),
            @Mapping(target="invitationOnly",source="icirsInvitationOnly"),
            @Mapping(target="reportInstructions",source="icirsReportInstructions"),
            @Mapping(target= "recordOperation",source="icirsRecordoperation")
    })
    CommitteeStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static Timestamp convertTimestamp(String timestamp){
        try {
            if(timestamp == null || timestamp.isEmpty() || timestamp.contains("@")){
                return null;
            }
            timestamp = timestamp.substring(0,timestamp.length()-1);
            if(timestamp.length()>1){
                timestamp = timestamp.replaceAll("T", " ");
            }
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss[.fffffffff]");

            java.util.Date date = formatter.parse(timestamp);
            java.sql.Timestamp dateTimeStamp = new java.sql.Timestamp(date.getTime());

            return dateTimeStamp;

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Named("formatTimeString")
    public static String formatTimeString(String timestamp){
        try {
            if(timestamp == null || timestamp.isEmpty() || timestamp.contains("@")){
                return null;
            }
            if(timestamp.length()>1){
                timestamp = timestamp.replaceAll("T", " ");
            }
            timestamp = timestamp.substring(0,timestamp.length()-1);
            return timestamp;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
