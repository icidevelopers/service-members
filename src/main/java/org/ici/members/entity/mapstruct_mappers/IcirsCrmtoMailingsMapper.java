package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.MailingStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoMailingsMapper extends Converter<Value, MailingStagingEntity> {
    @Mappings({
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="name",source="icirsName"),
            @Mapping(target="department",source="icirsDepartment"),
            @Mapping(target="description",source="icirsDescription"),
            @Mapping(target="fmo",source="icirsFmo"),
            @Mapping(target="committee",source="icirsCommittee"),
            @Mapping(target="mailingType",source="icirsMailingType"),
            @Mapping(target="mailingDescription",source="icirsMailingDescription"),
            @Mapping(target="groupDiscount",source="icirsGroupDiscount"),
            @Mapping(target="verificationReportExclude",source="icirsVerificationReportExclude"),
            @Mapping(target="sendAttachments",source="icirsSendAttachments"),
            @Mapping(target="finderVisible",source="icirsFinderVisible"),
            @Mapping(target="icinetAvailable",source="icirsIcinetAvailable"),
            @Mapping(target="billedSeparately",source="icirsBilledSeparately"),
            @Mapping(target="oneFreePerMemberComplex",source="icirsOneFreePerMemberComplex"),
            @Mapping(target="status",source="icirsStatus"),
            @Mapping(target="statusDate",source="icirsStatusDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="primaryContact",source="icirsPrimaryContact"),
            @Mapping(target="accountNumber",source="icirsAccountNumber"),
            @Mapping(target="includeMemberProfile",source="icirsIncludeMemberProfile"),
            @Mapping(target="excludeList",source="icirsExcludeList"),
            @Mapping(target="eventDate",source="icirsEventDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    MailingStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        timestamp = timestamp == null || timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        return timestamp;

    }
}
