package org.ici.members.entity.mapstruct_mappers;


import org.ici.members.entity.staging_entity.BoardCommitrepStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEEREPS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoiciBoardCommitrepMapper extends Converter<Value, BoardCommitrepStagingEntity> {
    @Mappings({
            @Mapping(target="pnum",source="icirsPnum"),
            @Mapping(target = "boardCommitteeNum",source = "icirsBoardcommitteenum"),
            @Mapping(target="boardChair",source="icirsBoardchair"),
            @Mapping(target="active",source="icirsActive"),
            @Mapping(target="elected",source="icirsElected",qualifiedByName = "convertTimestamp"),
            @Mapping(target="termend",source="icirsTermend",qualifiedByName = "termEndConvertTimestamp"),
            @Mapping(target="boardCnum",source="icirsBoardcnum"),
            @Mapping(target="title",source="icirsTitle"),
            @Mapping(target="entryDate",source="icirsEntryDate",qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    BoardCommitrepStagingEntity convert(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEEREPS.Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        String finalTimestamp = timestamp == null?"":timestamp.substring(0,timestamp.length()-1);
        finalTimestamp = finalTimestamp.replace("T", " ");
        return finalTimestamp;
    }
    @Named("termEndConvertTimestamp")
    public static String termEndConvertTimestamp(String timestamp){
        if(timestamp == null){
            return null;
        }else{
            String finalTimestamp = timestamp.substring(0,timestamp.length()-1);
            finalTimestamp = finalTimestamp.replace("T", " ");
            return finalTimestamp;
        }
    }
}
