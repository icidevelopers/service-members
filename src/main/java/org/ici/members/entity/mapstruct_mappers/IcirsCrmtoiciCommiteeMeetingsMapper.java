package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CommitteeMeetingsStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEEMEETINGSES.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoiciCommiteeMeetingsMapper extends Converter<Value, CommitteeMeetingsStagingEntity> {

    @Mappings({
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="beginDate",source="icirsBeginDate", qualifiedByName = "convertDate"),
            @Mapping(target="endDate",source="icirsEndDate", qualifiedByName = "convertDate"),
            @Mapping(target="purpose",source="icirsPurpose"),
            @Mapping(target="meetingId",source="icirsMeetingId"),
            @Mapping(target="recordOperation",source="icirsRecordoperation")
    })
    CommitteeMeetingsStagingEntity convert(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEEMEETINGSES.Value value);

    @Named("convertDate")
    public static Date convertDate(String timestamp) {
        timestamp = timestamp == null || timestamp.isEmpty() || timestamp.isBlank() ||timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        timestamp = timestamp.split(" ")[0];
        return timestamp.isEmpty() ?null:Date.valueOf(timestamp);
    }

    @Named("convertTimestamp")
    public static Timestamp convertTimestamp(String timestamp){
        try {
            if(timestamp == null || timestamp.isEmpty() || timestamp.contains("@")){
                return null;
            }
            timestamp = timestamp.substring(0,timestamp.length()-1);
            if(timestamp.length()>1){
                timestamp = timestamp.replaceAll("T", " ");
            }
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            java.util.Date date = formatter.parse(timestamp);
            java.sql.Timestamp dateTimeStamp = new java.sql.Timestamp(date.getTime());

            return dateTimeStamp;

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
