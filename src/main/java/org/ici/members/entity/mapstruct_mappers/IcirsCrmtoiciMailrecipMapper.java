package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.MailrecipStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGRECIPS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoiciMailrecipMapper extends Converter<Value, MailrecipStagingEntity> {

    @Mappings({
            @Mapping(target="pnum",source="icirsPnum"),
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="copies",source="icirsCopies"),
            @Mapping(target="cnum",source="icirsCnum"),
            @Mapping(target="billpnum",source="icirsBillPnum"),
            @Mapping(target="department",source="icirsDepartment"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="startDate",source="icirsStartDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="endDate",source="icirsEndDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="discount",source="icirsDiscount"),
            @Mapping(target="complimentaryEndDate",source="icirsComplimentaryEndDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="complimentaryReason",source="icirsComplimentaryReason"),
            @Mapping(target="comments",source="icirsComments"),
            @Mapping(target="feeStatus",source="icirsFeeStatus"),
            @Mapping(target="billmonth",source="icirsBillMonth"),
            @Mapping(target="cancelDate",source="icirsCancelDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="initialDate",source="icirsInitialDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    MailrecipStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        String finalTimestamp = timestamp == null?"":timestamp.substring(0,timestamp.length()-1);
        finalTimestamp = finalTimestamp.replace("T", " ");
        return finalTimestamp;
    }
}
