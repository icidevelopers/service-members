package org.ici.members.entity.mapstruct_mappers;


import org.ici.members.entity.staging_entity.AddressStagingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoiciaddressesMapper extends Converter<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.Value, AddressStagingEntity> {

    @Mappings({
            @Mapping(target = "pnum", source = "icirsPnum"),
            @Mapping(target = "adtype", source = "icirsAdtype"),
            @Mapping(target = "beginDate", source = "icirsBeginDate"),
            @Mapping(target="street1",source="icirsStreet1"),
            @Mapping(target="street2",source="icirsStreet2"),
            @Mapping(target="city",source="icirsCity"),
            @Mapping(target="state",source="icirsState"),
            @Mapping(target="zip",source="icirsZip"),
            @Mapping(target="country",source="cr3fdCountry"),
            @Mapping(target="cnum",source="icirsCnum"),
            @Mapping(target="endDate",source="icirsEndDate", qualifiedByName = "convertTimestamp"),
//            @Mapping(target="residence",source="res"),
            @Mapping(target="zip5",source="icirsZip"),
            @Mapping(target="companyName",source="icirsCompanyname"),
            @Mapping(target="companyAbname",source="icirsCompanyabname"),
            @Mapping(target="fundid",source="icirsFundid"),
            @Mapping(target="parentid",source="icirsParentid"),
            @Mapping(target="street1Long",source="icirsStreet1Long"),
            @Mapping(target="street2Long",source="icirsStreet2Long"),
            @Mapping(target="companyType",source="icirsCompanytype"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="phone",source="icirsPhone"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation"),
            @Mapping(target = "prevAdType", source="icirsPrevAdType"),
            @Mapping(target = "prevBeginDate", source="icirsPrevBeginDate", qualifiedByName = "convertTimestamp")
    })
            AddressStagingEntity convert(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.Value icirsCrmtoiciaddresses);
        @Named("convertTimestamp")
        public static String convertTimestamp(String timestamp){
                String finalTimestamp = timestamp == null?"":timestamp.substring(0,timestamp.length()-1);
                finalTimestamp = finalTimestamp.replaceAll("T"," ");
                return finalTimestamp;
        }
}
