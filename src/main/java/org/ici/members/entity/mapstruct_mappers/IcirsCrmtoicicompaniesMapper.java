package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CompanyStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANIESES.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoicicompaniesMapper extends Converter<Value, CompanyStagingEntity> {
    @Mappings({
            @Mapping(target = "cnum", source = "icirsCnumInteger"),
            @Mapping(target = "type", source = "icirsType"),
            @Mapping(target = "description", source = "icirsDescription"),
            @Mapping(target = "name", source = "icirsName"),
            @Mapping(target = "article", source = "icirsArticle"),
            @Mapping(target = "abname", source = "icirsAbname"),
            @Mapping(target = "srvtype", source = "icirsSrvtype"),
            @Mapping(target = "registered", source = "icirsRegistered"),
            @Mapping(target = "comments", source = "icirsComments"),
            @Mapping(target = "began", source = "icirsBegan", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "active", source = "icirsActive"),
            @Mapping(target = "hasuit", source = "icirsHasuit"),
            @Mapping(target = "dropSponsor", source = "icirsDropSponsor"),
            @Mapping(target = "employees", source = "icirsEmployees"),
            @Mapping(target = "ownership", source = "icirsOwnership"),
            @Mapping(target = "owner", source = "icirsOwner"),
            @Mapping(target = "nonUs", source = "icirsNonus"),
            @Mapping(target = "recruit", source = "icirsRecruit"),
            @Mapping(target = "tollfree1", source = "icirsTollfree1"),
            @Mapping(target = "tollfree2", source = "icirsTollfree2"),
            @Mapping(target = "tf2Note", source = "icirsTf2note"),
            @Mapping(target = "dropPhone", source = "icirsDropPhone"),
            @Mapping(target = "street1", source = "icirsStreet1"),
            @Mapping(target = "street2", source = "icirsStreet2"),
            @Mapping(target = "city", source = "icirsCity"),
            @Mapping(target = "dropState", source = "icirsDropState"),
            @Mapping(target = "zip", source = "icirsZip"),
            @Mapping(target = "country", source = "icirsCountry"),
            @Mapping(target = "complexCnum", source = "icirsComplexCnum"),
            @Mapping(target = "partnership", source = "icirsPartnership"),
            @Mapping(target = "uppername", source = "icirsUppername"),
            @Mapping(target = "upperabname", source = "icirsAbname"),
            @Mapping(target = "legalStructure", source = "icirsLegalStructure"),
            @Mapping(target = "state", source = "icirsState"),
            @Mapping(target = "URL", source = "icirsUrl"),
            @Mapping(target = "entryDate", source = "icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "entryUser", source = "icirsEntryUser"),
            @Mapping(target = "fax", source = "icirsFax"),
            @Mapping(target = "inProcess", source = "icirsInProcess"),
            @Mapping(target = "phone", source = "icirsPhone"),
            @Mapping(target = "updateDate", source = "icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "updateUser", source = "icirsUpdateUser", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "wantOtherDirectorMailings", source = "icirsWantOtherDirectorMailings"),
            @Mapping(target = "enterpriseSubscriber", source = "icirsEnterpriseSubscriber"),
            @Mapping(target = "enterpriseSubscribeDate", source = "icirsEnterpriseSubscribeDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "enterpriseUnsubscribeDate", source = "icirsEnterpriseUnsubscribeDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "URL_2", source = "icirsUrl2"),
            @Mapping(target = "enterpriseSubscriberUsers", source = "icirsEnterpriseSubscriberUsers"),
            @Mapping(target = "urlText", source = "icirsUrlText"),
            @Mapping(target = "regionId", source = "icirsRegionId"),
            @Mapping(target = "hasGlobalFunds", source = "icirsHasGlobalFunds"),
            @Mapping(target = "icimInsured", source = "icirsIcimInsured"),
            @Mapping(target = "multiSeriesTrust", source = "icirsMultiSeriesTrust"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    CompanyStagingEntity convert(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANIESES.Value icirsCrmtoicicompanieses);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
            timestamp = timestamp == null || timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
            if(timestamp.length()>1){
                timestamp = timestamp.replaceAll("T", " ");
            }
            return timestamp;
    }

}
