package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CompanyCommunicationStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYCOMMUNICATIONS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoicicompanycommunicationMapper extends Converter<Value, CompanyCommunicationStagingEntity> {
    @Mappings({
            @Mapping(target="complexCnum",source="icirsComplexCnum"),
            @Mapping(target="communicationDate",source="icirsCommunicationdate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="contactLn",source="icirsContactln"),
            @Mapping(target="contactFn",source="icirsContactfirstname"),
            @Mapping(target="communicationType",source="icirsCommunicationtype"),
            @Mapping(target="comment",source="icirsComment"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="pnum",source="icirsPnum"),
            @Mapping(target="companyType",source="icirsCompanytype"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    CompanyCommunicationStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        timestamp = timestamp == null || timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        return timestamp;
    }
}
