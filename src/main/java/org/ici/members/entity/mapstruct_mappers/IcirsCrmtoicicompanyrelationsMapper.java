package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CompanyCommunicationStagingEntity;
import org.ici.members.entity.staging_entity.CompanyRelationStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYRELATIONS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoicicompanyrelationsMapper extends Converter<Value, CompanyRelationStagingEntity> {
    @Mappings({
            @Mapping(target="cnum",source="icirsCnum"),
            @Mapping(target="type",source="icirsType"),
            @Mapping(target="relationCnum",source="icirsRelationCnum"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    CompanyRelationStagingEntity convert(Value value);
}
