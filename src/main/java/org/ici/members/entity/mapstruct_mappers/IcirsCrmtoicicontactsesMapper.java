package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.PersonStagingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTSES.Value;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface IcirsCrmtoicicontactsesMapper extends Converter<Value, PersonStagingEntity>{
    @Mappings({
            @Mapping(target = "pnum", source = "icirsPnum"),
            @Mapping(target = "assistantPnum", source = "icirsAssistantPnum"),
            @Mapping(target = "billpnum", source = "icirsBillpnum"),
            @Mapping(target = "cellPhone", source = "icirsCellPhone"),
            @Mapping(target = "comment", source = "icirsComment"),
            @Mapping(target = "committeeMailType", source = "icirsCommitteeMailType"),
            @Mapping(target = "complimentary", source = "icirsComplimentary"),
            @Mapping(target = "complimentaryReason", source = "icirsComplimentaryReason"),
            @Mapping(target = "department", source = "icirsDepartment"),
            @Mapping(target = "email", source = "icirsEmail"),
            @Mapping(target = "enterpriseSubscriberLite", source = "icirsEnterpriseSubscriberLite"),
            @Mapping(target = "entryDate", source = "icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "entryUser", source = "icirsEntryUser"),
            @Mapping(target = "fax", source = "icirsFax"),
            @Mapping(target = "fname", source = "icirsFname"),
            @Mapping(target = "inProcess", source = "icirsInProcess"),
            @Mapping(target = "legalMailType", source = "icirsLegalMailType"),
            @Mapping(target = "lname", source = "icirsLname"),
            @Mapping(target = "mailTypeResponse", source = "icirsMailTypeResponse"),
            @Mapping(target = "middle", source = "icirsMiddle"),
            @Mapping(target = "otherMailType", source = "icirsOtherMailType"),
            @Mapping(target = "phone", source = "icirsPhone"),
            @Mapping(target = "prefix", source = "icirsPrefix"),
            @Mapping(target = "rcvattach", source = "icirsRcvattach"),
            @Mapping(target = "status", source = "icirsStatus"),
            @Mapping(target = "statusDate", source = "icirsStatusDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target = "suffix", source = "icirsSuffix"),
            @Mapping(target = "suspendEmail", source = "icirsSuspendEmail"),
            @Mapping(target = "title", source = "icirsTitle"),
            @Mapping(target = "updateDate", source = "icirsUpdateDate"),
            @Mapping(target = "updateUser", source = "icirsUpdateUser", qualifiedByName = "convertTimestamp"),
            @Mapping(target="recordOperation",source="icirsRecordoperation"),
            @Mapping(target="blockMailings",source="icirsBlockMailings")
    })
    PersonStagingEntity convert(Value icirsCrmtoicicontactses);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        if(timestamp == null){
            return "";
        }else{
            timestamp = timestamp.replace("T", " ");
            timestamp = timestamp.replace("Z", " ");
            return timestamp;
        }
    }

}
