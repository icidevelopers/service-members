package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.ESppStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIESPPS.Value;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoiciespps extends Converter<Value, ESppStagingEntity> {
    @Mappings({
            @Mapping(target="cnum",source="icirsCnum"),
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    ESppStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        timestamp = timestamp == null || timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        return timestamp;
    }
}
