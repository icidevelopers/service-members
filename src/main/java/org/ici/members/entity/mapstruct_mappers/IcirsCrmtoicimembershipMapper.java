package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CurrentMembershipStagingEntity;
import org.ici.members.entity.staging_entity.MailingStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMEMBERSHIPS.Value;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoicimembershipMapper extends Converter<Value, CurrentMembershipStagingEntity> {
    @Mappings({
            @Mapping(target="cnum",source="icirsCnum"),
            @Mapping(target="member",source="icirsMember"),
            @Mapping(target="inactdt",source="icirsInactdt", qualifiedByName = "convertTimestamp"),
            @Mapping(target="type",source="icirsType"),
            @Mapping(target="indirect",source="icirsIndirect"),
            @Mapping(target="anniv",source="icirsAnniv", qualifiedByName = "convertTimestamp"),
            @Mapping(target="billmo",source="icirsBillmo"),
            @Mapping(target="comments",source="icirsComments"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    CurrentMembershipStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        String finalTimestamp = timestamp == null?"":timestamp.substring(0,timestamp.length()-1);
        finalTimestamp = finalTimestamp.replace("T", " ");
        return finalTimestamp;
    }
}
