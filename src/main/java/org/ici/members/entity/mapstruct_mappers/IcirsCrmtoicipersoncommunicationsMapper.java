package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.CurrentMembershipStagingEntity;
import org.ici.members.entity.staging_entity.PersonCommunicationStagingEntity;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIPERSONCOMMUNICATIONS.Value;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoicipersoncommunicationsMapper extends Converter<Value, PersonCommunicationStagingEntity> {
    @Mappings({
            @Mapping(target="pcId",source="icirsPcId"),
            @Mapping(target="pnum",source="icirsPnum"),
            @Mapping(target="communicationDate",source="icirsCommunicationDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="communicationType",source="icirsCommunicationType"),
            @Mapping(target="comment",source="icirsComment"),
            @Mapping(target="entryDate",source="icirsEntryDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="entryUser",source="icirsEntryUser"),
            @Mapping(target="updateDate",source="icirsUpdateDate", qualifiedByName = "convertTimestamp"),
            @Mapping(target="updateUser",source="icirsUpdateUser"),
            @Mapping(target="recordOperation",source="icirsRecordoperation")
    })
    PersonCommunicationStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        timestamp = timestamp == null || timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        return timestamp;
    }
}
