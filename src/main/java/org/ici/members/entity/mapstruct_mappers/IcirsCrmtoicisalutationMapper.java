package org.ici.members.entity.mapstruct_mappers;

import org.ici.members.entity.staging_entity.PersonCommunicationStagingEntity;
import org.ici.members.entity.staging_entity.SalutationStagingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICISALUTATIONS.Value;

@Mapper(componentModel ="spring")
public interface IcirsCrmtoicisalutationMapper extends Converter<Value, SalutationStagingEntity> {
    @Mappings({
            @Mapping(target="salutation",source="icirsSalutation"),
            @Mapping(target="pnum",source="icirsPnum"),
            @Mapping(target="code",source="icirsCode"),
            @Mapping(target="impacSalutation",source="icirsImpacSalutation"),
            @Mapping(target="recordOpertaion",source="icirsRecordoperation")
    })
    SalutationStagingEntity convert(Value value);

    @Named("convertTimestamp")
    public static String convertTimestamp(String timestamp){
        return timestamp == null?"":timestamp.substring(0,timestamp.length()-1);
    }
}
