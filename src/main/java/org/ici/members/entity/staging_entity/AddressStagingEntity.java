package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "address_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(AddressStagingId.class)
public class AddressStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name = "adtype", nullable = false)
    @Id
    private short adtype;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "street1", nullable = true, length = 50)
    private String street1;
    @Basic
    @Column(name = "street2", nullable = true, length = 50)
    private String street2;
    @Basic
    @Column(name = "city", nullable = true, length = 40)
    private String city;
    @Basic
    @Column(name = "state", nullable = true, length = 30)
    private String state;
    @Basic
    @Column(name = "zip", nullable = true, length = 15)
    private String zip;
    @Basic
    @Column(name = "country", nullable = true, length = 50)
    private String country;
    @Basic
    @Column(name = "cnum", nullable = true)
    private Integer cnum;
    @Basic
    @Column(name = "begin_date", nullable = false)
    @Id
    private Date beginDate;
    @Basic
    @Column(name = "end_date", nullable = true)
    private String endDate;
    @Basic
    @Column(name = "residence", nullable = true, length = 1)
    private String residence;
    @Basic
    @Column(name = "zip5", nullable = true, length = 5)
    private String zip5;
    @Basic
    @Column(name = "company_name", nullable = true, length = 84)
    private String companyName;
    @Basic
    @Column(name = "company_abname", nullable = true, length = 84)
    private String companyAbname;
    @Basic
    @Column(name = "fundid", nullable = true)
    private Integer fundid;
    @Basic
    @Column(name = "parentid", nullable = true)
    private Integer parentid;
    @Basic
    @Column(name = "street1_long", nullable = true, length = 50)
    private String street1Long;
    @Basic
    @Column(name = "street2_long", nullable = true, length = 50)
    private String street2Long;
    @Basic
    @Column(name = "company_type", nullable = true)
    private Short companyType;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "phone", nullable = true, length = 255)
    private String phone;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;
    @Basic
    @Column(name = "previous_adtype", nullable = true)
    private Integer prevAdType;
    @Basic
    @Column(name = "previous_begin_date", nullable = true)
    private String prevBeginDate;

    public Integer getPrevAdType() {
        return prevAdType;
    }

    public void setPrevAdType(Integer prevAdType) {
        this.prevAdType = prevAdType;
    }

    public String getPrevBeginDate() {
        return prevBeginDate;
    }

    public void setPrevBeginDate(String prevBeginDate) {
        this.prevBeginDate = prevBeginDate;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public short getAdtype() {
        return adtype;
    }

    public void setAdtype(short adtype) {
        this.adtype = adtype;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getCnum() {
        return cnum;
    }

    public void setCnum(Integer cnum) {
        this.cnum = cnum;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getZip5() {
        return zip5;
    }

    public void setZip5(String zip5) {
        this.zip5 = zip5;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAbname() {
        return companyAbname;
    }

    public void setCompanyAbname(String companyAbname) {
        this.companyAbname = companyAbname;
    }

    public Integer getFundid() {
        return fundid;
    }

    public void setFundid(Integer fundid) {
        this.fundid = fundid;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getStreet1Long() {
        return street1Long;
    }

    public void setStreet1Long(String street1Long) {
        this.street1Long = street1Long;
    }

    public String getStreet2Long() {
        return street2Long;
    }

    public void setStreet2Long(String street2Long) {
        this.street2Long = street2Long;
    }

    public Short getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Short companyType) {
        this.companyType = companyType;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressStagingEntity that = (AddressStagingEntity) o;
        return pnum == that.pnum && adtype == that.adtype && Objects.equals(street1, that.street1) && Objects.equals(street2, that.street2) && Objects.equals(city, that.city) && Objects.equals(state, that.state) && Objects.equals(zip, that.zip) && Objects.equals(country, that.country) && Objects.equals(cnum, that.cnum) && Objects.equals(beginDate, that.beginDate) && Objects.equals(endDate, that.endDate) && Objects.equals(residence, that.residence) && Objects.equals(zip5, that.zip5) && Objects.equals(companyName, that.companyName) && Objects.equals(companyAbname, that.companyAbname) && Objects.equals(fundid, that.fundid) && Objects.equals(parentid, that.parentid) && Objects.equals(street1Long, that.street1Long) && Objects.equals(street2Long, that.street2Long) && Objects.equals(companyType, that.companyType) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser) && Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, adtype, street1, street2, city, state, zip, country, cnum, beginDate, endDate, residence, zip5, companyName, companyAbname, fundid, parentid, street1Long, street2Long, companyType, entryDate, entryUser, updateDate, updateUser, phone);
    }
}
