package org.ici.members.entity.staging_entity;

import java.sql.Date;
import java.util.Objects;

public class AddressStagingId {

    private int pnum;
    private short adtype;
    private Date beginDate;
    private int revision;

    public AddressStagingId(int pnum, short adtype, Date beginDate, int revision) {
        this.pnum = pnum;
        this.adtype = adtype;
        this.beginDate = beginDate;
        this.revision = revision;
    }

    public AddressStagingId() {
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getAdtype() {
        return adtype;
    }

    public void setAdtype(short adtype) {
        this.adtype = adtype;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressStagingId that = (AddressStagingId) o;
        return pnum == that.pnum && adtype == that.adtype && Objects.equals(beginDate, that.beginDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, adtype, beginDate);
    }
}
