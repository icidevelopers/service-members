package org.ici.members.entity.staging_entity;

import java.util.Objects;

public class BoardCommitRepStagingKey {

    private int pnum;
    private int boardCommitteeNum;
    private int revision;

    public BoardCommitRepStagingKey() {
    }

    public BoardCommitRepStagingKey(int pnum, int boardCommitteeNum, int revision) {
        this.pnum = pnum;
        this.boardCommitteeNum = boardCommitteeNum;
        this.revision = revision;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getBoardCommitteeNum() {
        return boardCommitteeNum;
    }

    public void setBoardCommitteeNum(int boardCommitteeNum) {
        this.boardCommitteeNum = boardCommitteeNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardCommitRepStagingKey that = (BoardCommitRepStagingKey) o;
        return pnum == that.pnum && boardCommitteeNum == that.boardCommitteeNum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, boardCommitteeNum);
    }
}
