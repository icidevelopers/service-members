package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "boardCommitrep_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(BoardCommitRepStagingKey.class)
public class BoardCommitrepStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name = "boardCommitteeNum", nullable = false)
    @Id
    private int boardCommitteeNum;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "boardChair", nullable = true, length = 1)
    private String boardChair;
    @Basic
    @Column(name = "active", nullable = true, length = 1)
    private String active;
    @Basic
    @Column(name = "elected", nullable = true)
    private String elected;
    @Basic
    @Column(name = "termend", nullable = true)
    private String termend;
    @Basic
    @Column(name = "board_cnum", nullable = true)
    private Integer boardCnum;
    @Basic
    @Column(name = "title", nullable = true, length = 60)
    private String title;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getBoardCommitteeNum() {
        return boardCommitteeNum;
    }

    public void setBoardCommitteeNum(int boardCommitteeNum) {
        this.boardCommitteeNum = boardCommitteeNum;
    }

    public String getBoardChair() {
        return boardChair;
    }

    public void setBoardChair(String boardChair) {
        this.boardChair = boardChair;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getElected() {
        return elected;
    }

    public void setElected(String elected) {
        this.elected = elected;
    }

    public String getTermend() {
        return termend;
    }

    public void setTermend(String termend) {
        this.termend = termend;
    }

    public Integer getBoardCnum() {
        return boardCnum;
    }

    public void setBoardCnum(Integer boardCnum) {
        this.boardCnum = boardCnum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardCommitrepStagingEntity that = (BoardCommitrepStagingEntity) o;
        return pnum == that.pnum && boardCommitteeNum == that.boardCommitteeNum && Objects.equals(boardChair, that.boardChair) && Objects.equals(active, that.active) && Objects.equals(elected, that.elected) && Objects.equals(termend, that.termend) && Objects.equals(boardCnum, that.boardCnum) && Objects.equals(title, that.title) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, boardCommitteeNum, boardChair, active, elected, termend, boardCnum, title, entryDate, entryUser, updateDate, updateUser);
    }
}
