package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "boardCommittee_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(BoardCommitteeStagingKey.class)
public class BoardCommitteeStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "boardCommitteeNum", nullable = false)
    @Id
    private int boardCommitteeNum;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "name", nullable = true, length = 100)
    private String name;
    @Basic
    @Column(name = "boardCommitteeType", nullable = true)
    private String boardCommitteeType;
    @Basic
    @Column(name = "appointed", nullable = true)
    private String appointed;
    @Basic
    @Column(name = "status", nullable = true, length = 1)
    private String status;
    @Basic
    @Column(name = "status_date", nullable = true)
    private String statusDate;
    @Basic
    @Column(name = "board_cnum", nullable = true)
    private Integer boardCnum;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getBoardCommitteeNum() {
        return boardCommitteeNum;
    }

    public void setBoardCommitteeNum(int boardCommitteeNum) {
        this.boardCommitteeNum = boardCommitteeNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBoardCommitteeType() {
        return boardCommitteeType;
    }

    public void setBoardCommitteeType(String boardCommitteeType) {
        this.boardCommitteeType = boardCommitteeType;
    }

    public String getAppointed() {
        return appointed;
    }

    public void setAppointed(String appointed) {
        this.appointed = appointed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public Integer getBoardCnum() {
        return boardCnum;
    }

    public void setBoardCnum(Integer boardCnum) {
        this.boardCnum = boardCnum;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardCommitteeStagingEntity that = (BoardCommitteeStagingEntity) o;
        return boardCommitteeNum == that.boardCommitteeNum && Objects.equals(name, that.name) && Objects.equals(boardCommitteeType, that.boardCommitteeType) && Objects.equals(appointed, that.appointed) && Objects.equals(status, that.status) && Objects.equals(statusDate, that.statusDate) && Objects.equals(boardCnum, that.boardCnum) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(boardCommitteeNum, name, boardCommitteeType, appointed, status, statusDate, boardCnum, entryDate, entryUser, updateDate, updateUser);
    }
}
