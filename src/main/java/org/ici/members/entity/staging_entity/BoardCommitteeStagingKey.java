package org.ici.members.entity.staging_entity;

public class BoardCommitteeStagingKey {
    private int boardCommitteeNum;
    private int revision;

    public BoardCommitteeStagingKey(int boardCommitteeNum, int revision) {
        this.boardCommitteeNum = boardCommitteeNum;
        this.revision = revision;
    }

    public BoardCommitteeStagingKey() {
    }

    public int getBoardCommitteeNum() {
        return boardCommitteeNum;
    }

    public void setBoardCommitteeNum(int boardCommitteeNum) {
        this.boardCommitteeNum = boardCommitteeNum;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}
