package org.ici.members.entity.staging_entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import jakarta.persistence.*;

@Entity
@Table(name = "commitrep_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CommitrepId.class)
public class CommitrepStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name="revision", nullable = false)
    private int revision;
    @Basic
    @Column(name = "active", nullable = true, length = 1)
    private String active;
    @Basic
    @Column(name = "elected", nullable = true)
    private Date elected;
    @Basic
    @Column(name = "termend", nullable = true)
    private Date termend;
    @Basic
    @Column(name = "chair", nullable = true, length = 1)
    private String chair;
    @Basic
    @Column(name = "bogclass", nullable = true)
    private Boolean bogclass;
    @Basic
    @Column(name = "code", nullable = false)
    @Id
    private int code;
    @Basic
    @Column(name = "cnum", nullable = true)
    private Integer cnum;
    @Basic
    @Column(name = "title", nullable = true, length = 60)
    private String title;
    @Basic
    @Column(name = "rank_order", nullable = true)
    private Integer rankOrder;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private Timestamp entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private Timestamp updateDate;
    @Basic
    @Column(name = "workgrp_class", nullable = true)
    private Integer workgrpClass;
    @Basic
    @Column(name = "designating_pnum", nullable = true)
    private Integer designatingPnum;
    @Basic
    @Column(name = "committee_rep_code", nullable = true)
    private Integer committeeRepCode;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 10)
    private String recordOperation;

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }
    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getElected() {
        return elected;
    }

    public void setElected(Date elected) {
        this.elected = elected;
    }

    public Date getTermend() {
        return termend;
    }

    public void setTermend(Date termend) {
        this.termend = termend;
    }

    public String getChair() {
        return chair;
    }

    public void setChair(String chair) {
        this.chair = chair;
    }

    public Boolean getBogclass() {
        return bogclass;
    }

    public void setBogclass(Boolean bogclass) {
        this.bogclass = bogclass;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Integer getCnum() {
        return cnum;
    }

    public void setCnum(Integer cnum) {
        this.cnum = cnum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRankOrder() {
        return rankOrder;
    }

    public void setRankOrder(Integer rankOrder) {
        this.rankOrder = rankOrder;
    }

    public Timestamp getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Timestamp entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public Integer getWorkgrpClass() {
        return workgrpClass;
    }

    public void setWorkgrpClass(Integer workgrpClass) {
        this.workgrpClass = workgrpClass;
    }

    public Integer getDesignatingPnum() {
        return designatingPnum;
    }

    public void setDesignatingPnum(Integer designatingPnum) {
        this.designatingPnum = designatingPnum;
    }

    public Integer getCommitteeRepCode() {
        return committeeRepCode;
    }

    public void setCommitteeRepCode(Integer committeeRepCode) {
        this.committeeRepCode = committeeRepCode;
    }

    public String getRecordOperation() {
        return recordOperation;
    }

    public void setRecordOperation(String recordOperation) {
        this.recordOperation = recordOperation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitrepStagingEntity that = (CommitrepStagingEntity) o;
        return pnum == that.pnum && code == that.code && Objects.equals(active, that.active) && Objects.equals(elected, that.elected) && Objects.equals(termend, that.termend) && Objects.equals(chair, that.chair) && Objects.equals(bogclass, that.bogclass) && Objects.equals(cnum, that.cnum) && Objects.equals(title, that.title) && Objects.equals(rankOrder, that.rankOrder) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(workgrpClass, that.workgrpClass) && Objects.equals(designatingPnum, that.designatingPnum) && Objects.equals(committeeRepCode, that.committeeRepCode) && Objects.equals(recordOperation, that.recordOperation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, active, elected, termend, chair, bogclass, code, cnum, title, rankOrder, entryDate, entryUser, workgrpClass, designatingPnum, committeeRepCode, recordOperation);
    }
}
