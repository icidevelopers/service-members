package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "committee_meeting_attendees_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CommitteeMeetingsAttendeesStagingId.class)
public class CommitteeMeetingAttendeesStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "meeting_id", nullable = false)
    @Id
    private int meetingId;
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;

    @Basic
    @Column(name = "cnum", nullable = true)
    private Integer cnum;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public Integer getCnum() {
        return cnum;
    }

    public void setCnum(Integer cnum) {
        this.cnum = cnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitteeMeetingAttendeesStagingEntity that = (CommitteeMeetingAttendeesStagingEntity) o;
        return meetingId == that.meetingId && pnum == that.pnum && Objects.equals(cnum, that.cnum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meetingId, pnum, cnum);
    }
}
