package org.ici.members.entity.staging_entity;

import java.util.Objects;

public class CommitteeMeetingsAttendeesStagingId {

    private int meetingId;
    private int pnum;
    private int revision;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitteeMeetingsAttendeesStagingId that = (CommitteeMeetingsAttendeesStagingId) o;
        return meetingId == that.meetingId && pnum == that.pnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(meetingId, pnum);
    }
}
