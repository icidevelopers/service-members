package org.ici.members.entity.staging_entity;


import jakarta.persistence.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "committee_meetings_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CommitteeMeetingsStagingId.class)
public class CommitteeMeetingsStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "code", nullable = false)
    @Id
    private int code;
    @Basic
    @Column(name = "begin_date", nullable = false)
    @Id
    private Date beginDate;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "end_date", nullable = true)
    private Date endDate;
    @Basic
    @Column(name = "purpose", nullable = true, length = 255)
    private String purpose;
    @Basic
    @Column(name = "meeting_id", nullable = true)
    private Integer meetingId;
    @Basic
    @Column(name = "record_operation", nullable = false, length = 10)
    private String recordOperation;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Integer getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(Integer meetingId) {
        this.meetingId = meetingId;
    }

    public String getRecordOperation() {
        return recordOperation;
    }

    public void setRecordOperation(String recordOperation) {
        this.recordOperation = recordOperation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitteeMeetingsStagingEntity that = (CommitteeMeetingsStagingEntity) o;
        return code == that.code && Objects.equals(beginDate, that.beginDate) && Objects.equals(endDate, that.endDate) && Objects.equals(purpose, that.purpose) && Objects.equals(meetingId, that.meetingId) && Objects.equals(recordOperation, that.recordOperation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, beginDate, endDate, purpose, meetingId, recordOperation);
    }
}
