package org.ici.members.entity.staging_entity;

import java.sql.Date;
import java.util.Objects;

public class CommitteeMeetingsStagingId {
    private int code;
    private Date beginDate;
    private int revision;

    public CommitteeMeetingsStagingId(int code, Date beginDate, int revision) {
        this.code = code;
        this.beginDate = beginDate;
        this.revision = revision;
    }

    public CommitteeMeetingsStagingId() {
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitteeMeetingsStagingId that = (CommitteeMeetingsStagingId) o;
        return code == that.code && Objects.equals(beginDate, that.beginDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, beginDate);
    }
}
