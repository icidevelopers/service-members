package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "committee_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CommitteeStagingKey.class)
public class CommitteeStagingEntity extends StagingEntity implements Serializable {
    @Basic
    @Column(name = "code", nullable = false)
    @Id
    private int code;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "type", nullable = false)
    private short type;
    @Basic
    @Column(name = "name", nullable = false, length = 100)
    private String name;
    @Basic
    @Column(name = "appointed", nullable = true)
    private String appointed;
    @Basic
    @Column(name = "description", nullable = true, length = -1)
    private String description;
    @Basic
    @Column(name = "balance", nullable = true)
    private Double balance;
    @Basic
    @Column(name = "coord_enumber", nullable = true)
    private Integer coordEnumber;
    @Basic
    @Column(name = "department", nullable = true)
    private Integer department;
    @Basic
    @Column(name = "NOT_USED", nullable = true)
    private Integer notUsed;
    @Basic
    @Column(name = "members_per_complex", nullable = true)
    private Short membersPerComplex;
    @Basic
    @Column(name = "print_order", nullable = true)
    private Integer printOrder;
    @Basic
    @Column(name = "requested_by", nullable = true, length = 255)
    private String requestedBy;
    @Basic
    @Column(name = "comments", nullable = true, length = 500)
    private String comments;
    @Basic
    @Column(name = "status", nullable = true, length = 1)
    private String status;
    @Basic
    @Column(name = "status_date", nullable = true)
    private String statusDate;
    @Basic
    @Column(name = "verification_report_exclude", nullable = true, length = 1)
    private String verificationReportExclude;
    @Basic
    @Column(name = "finder_visible", nullable = true, length = 1)
    private String finderVisible;
    @Basic
    @Column(name = "memo_registration_name", nullable = true, length = 80)
    private String memoRegistrationName;
    @Basic
    @Column(name = "max_mail_designees", nullable = true)
    private Integer maxMailDesignees;
    @Basic
    @Column(name = "max_web_designees", nullable = true)
    private Integer maxWebDesignees;
    @Basic
    @Column(name = "coord_enumber2", nullable = true)
    private Integer coordEnumber2;
    @Basic
    @Column(name = "invitation_only", nullable = true, length = 1)
    private String invitationOnly;
    @Basic
    @Column(name = "report_instructions", nullable = true, length = -1)
    private String reportInstructions;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOperation;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOperation() {
        return recordOperation;
    }

    public void setRecordOperation(String recordOperation) {
        this.recordOperation = recordOperation;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppointed() {
        return appointed;
    }

    public void setAppointed(String appointed) {
        this.appointed = appointed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getCoordEnumber() {
        return coordEnumber;
    }

    public void setCoordEnumber(Integer coordEnumber) {
        this.coordEnumber = coordEnumber;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public Integer getNotUsed() {
        return notUsed;
    }

    public void setNotUsed(Integer notUsed) {
        this.notUsed = notUsed;
    }

    public Short getMembersPerComplex() {
        return membersPerComplex;
    }

    public void setMembersPerComplex(Short membersPerComplex) {
        this.membersPerComplex = membersPerComplex;
    }

    public Integer getPrintOrder() {
        return printOrder;
    }

    public void setPrintOrder(Integer printOrder) {
        this.printOrder = printOrder;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getVerificationReportExclude() {
        return verificationReportExclude;
    }

    public void setVerificationReportExclude(String verificationReportExclude) {
        this.verificationReportExclude = verificationReportExclude;
    }

    public String getFinderVisible() {
        return finderVisible;
    }

    public void setFinderVisible(String finderVisible) {
        this.finderVisible = finderVisible;
    }

    public String getMemoRegistrationName() {
        return memoRegistrationName;
    }

    public void setMemoRegistrationName(String memoRegistrationName) {
        this.memoRegistrationName = memoRegistrationName;
    }

    public Integer getMaxMailDesignees() {
        return maxMailDesignees;
    }

    public void setMaxMailDesignees(Integer maxMailDesignees) {
        this.maxMailDesignees = maxMailDesignees;
    }

    public Integer getMaxWebDesignees() {
        return maxWebDesignees;
    }

    public void setMaxWebDesignees(Integer maxWebDesignees) {
        this.maxWebDesignees = maxWebDesignees;
    }

    public Integer getCoordEnumber2() {
        return coordEnumber2;
    }

    public void setCoordEnumber2(Integer coordEnumber2) {
        this.coordEnumber2 = coordEnumber2;
    }

    public String getInvitationOnly() {
        return invitationOnly;
    }

    public void setInvitationOnly(String invitationOnly) {
        this.invitationOnly = invitationOnly;
    }

    public String getReportInstructions() {
        return reportInstructions;
    }

    public void setReportInstructions(String reportInstructions) {
        this.reportInstructions = reportInstructions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitteeStagingEntity that = (CommitteeStagingEntity) o;
        return code == that.code && type == that.type && Objects.equals(name, that.name) && Objects.equals(appointed, that.appointed) && Objects.equals(description, that.description)
                /*&& Objects.equals(balance, that.balance)*/
                && Objects.equals(coordEnumber, that.coordEnumber) && Objects.equals(department, that.department) && Objects.equals(notUsed, that.notUsed) && Objects.equals(membersPerComplex, that.membersPerComplex) && Objects.equals(printOrder, that.printOrder) && Objects.equals(requestedBy, that.requestedBy) && Objects.equals(comments, that.comments) && Objects.equals(status, that.status) && Objects.equals(statusDate, that.statusDate) && Objects.equals(verificationReportExclude, that.verificationReportExclude) && Objects.equals(finderVisible, that.finderVisible) && Objects.equals(memoRegistrationName, that.memoRegistrationName) && Objects.equals(maxMailDesignees, that.maxMailDesignees) && Objects.equals(maxWebDesignees, that.maxWebDesignees) && Objects.equals(coordEnumber2, that.coordEnumber2) && Objects.equals(invitationOnly, that.invitationOnly) && Objects.equals(reportInstructions, that.reportInstructions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, type, name, appointed, description,
                /*balance,*/
                coordEnumber, department, notUsed, membersPerComplex, printOrder, requestedBy, comments, status, statusDate, verificationReportExclude, finderVisible, memoRegistrationName, maxMailDesignees, maxWebDesignees, coordEnumber2, invitationOnly, reportInstructions);
    }
}
