package org.ici.members.entity.staging_entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Id;

public class CompContactStagingId {

    private short purpose;
    private int cnum;
    private int pnum;
    private int revision;

    public CompContactStagingId(short purpose, int cnum, int pnum, int revision) {
        this.purpose = purpose;
        this.cnum = cnum;
        this.pnum = pnum;
        this.revision = revision;
    }

    public CompContactStagingId() {
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public short getPurpose() {
        return purpose;
    }

    public void setPurpose(short purpose) {
        this.purpose = purpose;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }
}
