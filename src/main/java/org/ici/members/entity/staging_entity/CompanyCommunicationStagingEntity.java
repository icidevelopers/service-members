package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "company_communication_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CompanyCommunicationStagingId.class)
public class CompanyCommunicationStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "complex_cnum", nullable = false)
    @Id
    private int complexCnum;
    @Basic
    @Column(name = "communication_date", nullable = false)
    @Id
    private String communicationDate;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "contact_ln", nullable = true, length = 30)
    private String contactLn;
    @Basic
    @Column(name = "contact_fn", nullable = true, length = 30)
    private String contactFn;
    @Basic
    @Column(name = "communication_type", nullable = true)
    private Integer communicationType;
    @Basic
    @Column(name = "comment", nullable = true, length = -1)
    private String comment;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "pnum", nullable = true)
    private Integer pnum;
    @Basic
    @Column(name = "company_type", nullable = false)
    private int companyType;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getComplexCnum() {
        return complexCnum;
    }

    public void setComplexCnum(int complexCnum) {
        this.complexCnum = complexCnum;
    }

    public String getCommunicationDate() {
        return communicationDate;
    }

    public void setCommunicationDate(String communicationDate) {
        this.communicationDate = communicationDate;
    }

    public String getContactLn() {
        return contactLn;
    }

    public void setContactLn(String contactLn) {
        this.contactLn = contactLn;
    }

    public String getContactFn() {
        return contactFn;
    }

    public void setContactFn(String contactFn) {
        this.contactFn = contactFn;
    }

    public Integer getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(Integer communicationType) {
        this.communicationType = communicationType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getPnum() {
        return pnum;
    }

    public void setPnum(Integer pnum) {
        this.pnum = pnum;
    }

    public int getCompanyType() {
        return companyType;
    }

    public void setCompanyType(int companyType) {
        this.companyType = companyType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCommunicationStagingEntity that = (CompanyCommunicationStagingEntity) o;
        return complexCnum == that.complexCnum && companyType == that.companyType && Objects.equals(communicationDate, that.communicationDate) && Objects.equals(contactLn, that.contactLn) && Objects.equals(contactFn, that.contactFn) && Objects.equals(communicationType, that.communicationType) && Objects.equals(comment, that.comment) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser) && Objects.equals(pnum, that.pnum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(complexCnum, communicationDate, contactLn, contactFn, communicationType, comment, entryDate, entryUser, updateDate, updateUser, pnum, companyType);
    }
}
