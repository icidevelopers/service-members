package org.ici.members.entity.staging_entity;

import java.sql.Date;
import java.util.Objects;

public class CompanyCommunicationStagingId {

    private int complexCnum;
    private String communicationDate;
    private int revision;

    public CompanyCommunicationStagingId() {
    }

    public CompanyCommunicationStagingId(int complexCnum, String communicationDate, int revision) {
        this.complexCnum = complexCnum;
        this.communicationDate = communicationDate;
        this.revision = revision;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getComplexCnum() {
        return complexCnum;
    }

    public void setComplexCnum(int complexCnum) {
        this.complexCnum = complexCnum;
    }

    public String getCommunicationDate() {
        return communicationDate;
    }

    public void setCommunicationDate(String communicationDate) {
        this.communicationDate = communicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyCommunicationStagingId that = (CompanyCommunicationStagingId) o;
        return complexCnum == that.complexCnum && Objects.equals(communicationDate, that.communicationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(complexCnum, communicationDate);
    }
}
