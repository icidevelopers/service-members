package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "company_relation_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CompanyRelationStagingId.class)
public class CompanyRelationStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "cnum", nullable = false)
    @Id
    private int cnum;
    @Basic
    @Column(name = "type", nullable = false)
    @Id
    private short type;
    @Basic
    @Column(name = "relation_cnum", nullable = false)
    @Id
    private int relationCnum;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public int getRelationCnum() {
        return relationCnum;
    }

    public void setRelationCnum(int relationCnum) {
        this.relationCnum = relationCnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyRelationStagingEntity that = (CompanyRelationStagingEntity) o;
        return cnum == that.cnum && type == that.type && relationCnum == that.relationCnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cnum, type, relationCnum);
    }
}
