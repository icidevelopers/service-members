package org.ici.members.entity.staging_entity;

import java.util.Objects;

public class CompanyRelationStagingId {

    private int cnum;

    private short type;

    private int relationCnum;

    private int revision;

    public CompanyRelationStagingId() {
    }

    public CompanyRelationStagingId(int cnum, short type, int relationCnum) {
        this.cnum = cnum;
        this.type = type;
        this.relationCnum = relationCnum;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public int getRelationCnum() {
        return relationCnum;
    }

    public void setRelationCnum(int relationCnum) {
        this.relationCnum = relationCnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyRelationStagingId that = (CompanyRelationStagingId) o;
        return cnum == that.cnum && type == that.type && relationCnum == that.relationCnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cnum, type, relationCnum);
    }
}
