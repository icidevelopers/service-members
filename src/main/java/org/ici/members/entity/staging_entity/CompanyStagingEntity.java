package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;
import org.ici.members.entity.Icirs_Sync;

import java.util.Objects;

@Entity
@Table(name = "company_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CompanyStagingId.class)
public class CompanyStagingEntity extends StagingEntity implements Icirs_Sync {
    @Basic
    @Column(name = "cnum", nullable = false)
    @Id
    private int cnum;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "type", nullable = false)
    private short type;
    @Basic
    @Column(name = "description", nullable = true)
    private Short description;
    @Basic
    @Column(name = "name", nullable = false, length = 80)
    private String name;
    @Basic
    @Column(name = "article", nullable = true, length = 3)
    private String article;
    @Basic
    @Column(name = "abname", nullable = true, length = 80)
    private String abname;
    @Basic
    @Column(name = "srvtype", nullable = true)
    private Short srvtype;
    @Basic
    @Column(name = "registered", nullable = true, length = 1)
    private String registered;
    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    private String comments;
    @Basic
    @Column(name = "began", nullable = true)
    private String began;
    @Basic
    @Column(name = "active", nullable = true, length = 1)
    private String active;
    @Basic
    @Column(name = "hasuit", nullable = true, length = 1)
    private String hasuit;
    @Basic
    @Column(name = "DROP_sponsor", nullable = true, length = 1)
    private String DROPSponsor;
    @Basic
    @Column(name = "employees", nullable = true)
    private Integer employees;
    @Basic
    @Column(name = "ownership", nullable = true)
    private Short ownership;
    @Basic
    @Column(name = "owner", nullable = true, length = 50)
    private String owner;
    @Basic
    @Column(name = "non_us", nullable = true, length = 1)
    private String nonUs;
    @Basic
    @Column(name = "recruit", nullable = true)
    private Short recruit;
    @Basic
    @Column(name = "tollfree1", nullable = true, length = 15)
    private String tollfree1;
    @Basic
    @Column(name = "tollfree2", nullable = true, length = 15)
    private String tollfree2;
    @Basic
    @Column(name = "tf2note", nullable = true, length = 10)
    private String tf2Note;
    @Basic
    @Column(name = "DROP_phone", nullable = true, length = 15)
    private String DROPPhone;
    @Basic
    @Column(name = "fax", nullable = true, length = 15)
    private String fax;
    @Basic
    @Column(name = "street1", nullable = true, length = 50)
    private String street1;
    @Basic
    @Column(name = "street2", nullable = true, length = 50)
    private String street2;
    @Basic
    @Column(name = "city", nullable = true, length = 20)
    private String city;
    @Basic
    @Column(name = "DROP_state", nullable = true, length = 2)
    private String DROPState;
    @Basic
    @Column(name = "zip", nullable = true, length = 15)
    private String zip;
    @Basic
    @Column(name = "country", nullable = true, length = 30)
    private String country;
    @Basic
    @Column(name = "complex_cnum", nullable = true)
    private Integer complexCnum;
    @Basic
    @Column(name = "partnership", nullable = true, length = 1)
    private String partnership;
    @Basic
    @Column(name = "uppername", nullable = true, length = 80)
    private String uppername;
    @Basic
    @Column(name = "upperabname", nullable = true, length = 80)
    private String upperabname;
    @Basic
    @Column(name = "in_process", nullable = true, length = 1)
    private String inProcess;
    @Basic
    @Column(name = "legal_structure", nullable = true)
    private Integer legalStructure;
    @Basic
    @Column(name = "phone", nullable = true, length = 40)
    private String phone;
    @Basic
    @Column(name = "state", nullable = true, length = 30)
    private String state;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "URL", nullable = true, length = 255)
    private String URL;
    @Basic
    @Column(name = "want_other_director_mailings", nullable = true, length = 1)
    private String wantOtherDirectorMailings;
    @Basic
    @Column(name = "enterprise_subscriber", nullable = true, length = 1)
    private String enterpriseSubscriber;
    @Basic
    @Column(name = "enterprise_subscribe_date", nullable = true)
    private String enterpriseSubscribeDate;
    @Basic
    @Column(name = "enterprise_unsubscribe_date", nullable = true)
    private String enterpriseUnsubscribeDate;
    @Basic
    @Column(name = "URL_2", nullable = true, length = 255)
    private String URL_2;
    @Basic
    @Column(name = "enterprise_subscriber_users", nullable = true)
    private Integer enterpriseSubscriberUsers;
    @Basic
    @Column(name = "url_text", nullable = true, length = -1)
    private String urlText;
    @Basic
    @Column(name = "region_id", nullable = true)
    private Integer regionId;
    @Basic
    @Column(name = "has_global_funds", nullable = true, length = 1)
    private String hasGlobalFunds;
    @Basic
    @Column(name = "icim_insured", nullable = true, length = 1)
    private String icimInsured;
    @Basic
    @Column(name = "multi_series_trust", nullable = true, length = 1)
    private String multiSeriesTrust;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getCnum() {return cnum;}
    public void setCnum(int cnum) {this.cnum = cnum;}
    public short getType() {return type;}
    public void setType(short type) {        this.type = type;
    }    public Short getDescription() {        return description;
    }    public void setDescription(Short description) {        this.description = description;
    }    public String getName() {        return name;
    }    public void setName(String name) {        this.name = name;
    }    public String getArticle() {        return article;
    }    public void setArticle(String article) {        this.article = article;
    }    public String getAbname() {        return abname;
    }    public void setAbname(String abname) {        this.abname = abname;
    }    public Short getSrvtype() {        return srvtype;
    }    public void setSrvtype(Short srvtype) {        this.srvtype = srvtype;
    }    public String getRegistered() {        return registered;
    }    public void setRegistered(String registered) {        this.registered = registered;
    }    public String getComments() {        return comments;
    }    public void setComments(String comments) {        this.comments = comments;
    }    public String getBegan() {        return began;
    }    public void setBegan(String began) {        this.began = began;
    }    public String getActive() {        return active;
    }    public void setActive(String active) {        this.active = active;
    }    public String getHasuit() {        return hasuit;
    }    public void setHasuit(String hasuit) {        this.hasuit = hasuit;
    }    public String getDropSponsor() {        return DROPSponsor;
    }    public void setDropSponsor(String DROPSponsor) {        this.DROPSponsor = DROPSponsor;
    }    public Integer getEmployees() {        return employees;
    }    public void setEmployees(Integer employees) {        this.employees = employees;
    }    public Short getOwnership() {        return ownership;
    }    public void setOwnership(Short ownership) {        this.ownership = ownership;
    }    public String getOwner() {        return owner;
    }    public void setOwner(String owner) {        this.owner = owner;
    }    public String getNonUs() {        return nonUs;
    }    public void setNonUs(String nonUs) {        this.nonUs = nonUs;
    }    public Short getRecruit() {        return recruit;
    }    public void setRecruit(Short recruit) {        this.recruit = recruit;
    }    public String getTollfree1() {        return tollfree1;
    }    public void setTollfree1(String tollfree1) {        this.tollfree1 = tollfree1;
    }    public String getTollfree2() {        return tollfree2;
    }    public void setTollfree2(String tollfree2) {        this.tollfree2 = tollfree2;
    }    public String getTf2Note() {        return tf2Note;
    }    public void setTf2Note(String tf2Note) {        this.tf2Note = tf2Note;
    }    public String getDropPhone() {        return DROPPhone;
    }    public void setDropPhone(String DROPPhone) {        this.DROPPhone = DROPPhone;
    }    public String getFax() {        return fax;
    }    public void setFax(String fax) {        this.fax = fax;
    }    public String getStreet1() {        return street1;
    }    public void setStreet1(String street1) {        this.street1 = street1;
    }    public String getStreet2() {        return street2;
    }    public void setStreet2(String street2) {        this.street2 = street2;
    }    public String getCity() {        return city;
    }    public void setCity(String city) {        this.city = city;
    }    public String getDropState() {        return DROPState;
    }    public void setDropState(String DROPState) {        this.DROPState = DROPState;
    }    public String getZip() {        return zip;
    }    public void setZip(String zip) {        this.zip = zip;
    }    public String getCountry() {        return country;
    }    public void setCountry(String country) {        this.country = country;
    }    public Integer getComplexCnum() {        return complexCnum;
    }    public void setComplexCnum(Integer complexCnum) {        this.complexCnum = complexCnum;
    }    public String getPartnership() {        return partnership;
    }    public void setPartnership(String partnership) {        this.partnership = partnership;
    }    public String getUppername() {        return uppername;
    }    public void setUppername(String uppername) {        this.uppername = uppername;
    }    public String getUpperabname() {        return upperabname;
    }    public void setUpperabname(String upperabname) {        this.upperabname = upperabname;
    }    public String getInProcess() {        return inProcess;
    }    public void setInProcess(String inProcess) {        this.inProcess = inProcess;
    }    public Integer getLegalStructure() {        return legalStructure;
    }    public void setLegalStructure(Integer legalStructure) {        this.legalStructure = legalStructure;
    }    public String getPhone() {        return phone;
    }    public void setPhone(String phone) {        this.phone = phone;
    }    public String getState() {        return state;
    }    public void setState(String state) {        this.state = state;
    }    public String getEntryDate() {        return entryDate;
    }    public void setEntryDate(String entryDate) {        this.entryDate = entryDate;
    }    public String getEntryUser() {        return entryUser;
    }    public void setEntryUser(String entryUser) {        this.entryUser = entryUser;
    }    public String getUpdateDate() {        return updateDate;
    }    public void setUpdateDate(String updateDate) {        this.updateDate = updateDate;
    }    public String getUpdateUser() {        return updateUser;
    }    public void setUpdateUser(String updateUser) {        this.updateUser = updateUser;
    }    public String getURL() {        return URL;
    }    public void setURL(String URL) {        this.URL = URL;
    }    public String getWantOtherDirectorMailings() {        return wantOtherDirectorMailings;
    }    public void setWantOtherDirectorMailings(String wantOtherDirectorMailings) {        this.wantOtherDirectorMailings = wantOtherDirectorMailings;
    }    public String getEnterpriseSubscriber() {        return enterpriseSubscriber;
    }    public void setEnterpriseSubscriber(String enterpriseSubscriber) {        this.enterpriseSubscriber = enterpriseSubscriber;
    }    public String getEnterpriseSubscribeDate() {        return enterpriseSubscribeDate;
    }    public void setEnterpriseSubscribeDate(String enterpriseSubscribeDate) {        this.enterpriseSubscribeDate = enterpriseSubscribeDate;
    }    public String getEnterpriseUnsubscribeDate() {        return enterpriseUnsubscribeDate;
    }    public void setEnterpriseUnsubscribeDate(String enterpriseUnsubscribeDate) {        this.enterpriseUnsubscribeDate = enterpriseUnsubscribeDate;
    }    public String getURL_2() {        return URL_2;
    }    public void setURL_2(String URL_2) {        this.URL_2 = URL_2;
    }    public Integer getEnterpriseSubscriberUsers() {        return enterpriseSubscriberUsers;
    }    public void setEnterpriseSubscriberUsers(Integer enterpriseSubscriberUsers) {        this.enterpriseSubscriberUsers = enterpriseSubscriberUsers;
    }    public String getUrlText() {        return urlText;
    }    public void setUrlText(String urlText) {        this.urlText = urlText;
    }    public Integer getRegionId() {        return regionId;
    }    public void setRegionId(Integer regionId) {        this.regionId = regionId;
    }    public String getHasGlobalFunds() {        return hasGlobalFunds;
    }    public void setHasGlobalFunds(String hasGlobalFunds) {        this.hasGlobalFunds = hasGlobalFunds;
    }    public String getIcimInsured() {        return icimInsured;
    }    public void setIcimInsured(String icimInsured) {        this.icimInsured = icimInsured;
    }    public String getMultiSeriesTrust() {        return multiSeriesTrust;
    }    public void setMultiSeriesTrust(String multiSeriesTrust) {        this.multiSeriesTrust = multiSeriesTrust;
    }    @Override    public boolean equals(Object o) {        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyStagingEntity that = (CompanyStagingEntity) o;
        return cnum == that.cnum && type == that.type && Objects.equals(description, that.description) && Objects.equals(name, that.name) && Objects.equals(article, that.article) && Objects.equals(abname, that.abname) && Objects.equals(srvtype, that.srvtype) && Objects.equals(registered, that.registered) && Objects.equals(comments, that.comments) && Objects.equals(began, that.began) && Objects.equals(active, that.active) && Objects.equals(hasuit, that.hasuit) && Objects.equals(DROPSponsor, that.DROPSponsor) && Objects.equals(employees, that.employees) && Objects.equals(ownership, that.ownership) && Objects.equals(owner, that.owner) && Objects.equals(nonUs, that.nonUs) && Objects.equals(recruit, that.recruit) && Objects.equals(tollfree1, that.tollfree1) && Objects.equals(tollfree2, that.tollfree2) && Objects.equals(tf2Note, that.tf2Note) && Objects.equals(DROPPhone, that.DROPPhone) && Objects.equals(fax, that.fax) && Objects.equals(street1, that.street1) && Objects.equals(street2, that.street2) && Objects.equals(city, that.city) && Objects.equals(DROPState, that.DROPState) && Objects.equals(zip, that.zip) && Objects.equals(country, that.country) && Objects.equals(complexCnum, that.complexCnum) && Objects.equals(partnership, that.partnership) && Objects.equals(uppername, that.uppername) && Objects.equals(upperabname, that.upperabname) && Objects.equals(inProcess, that.inProcess) && Objects.equals(legalStructure, that.legalStructure) && Objects.equals(phone, that.phone) && Objects.equals(state, that.state) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser) && Objects.equals(URL, that.URL) && Objects.equals(wantOtherDirectorMailings, that.wantOtherDirectorMailings) && Objects.equals(enterpriseSubscriber, that.enterpriseSubscriber) && Objects.equals(enterpriseSubscribeDate, that.enterpriseSubscribeDate) && Objects.equals(enterpriseUnsubscribeDate, that.enterpriseUnsubscribeDate) && Objects.equals(URL_2, that.URL_2) && Objects.equals(enterpriseSubscriberUsers, that.enterpriseSubscriberUsers) && Objects.equals(urlText, that.urlText) && Objects.equals(regionId, that.regionId) && Objects.equals(hasGlobalFunds, that.hasGlobalFunds) && Objects.equals(icimInsured, that.icimInsured) && Objects.equals(multiSeriesTrust, that.multiSeriesTrust);
    }    @Override    public int hashCode() {        return Objects.hash(cnum, type, description, name, article, abname, srvtype, registered, comments, began, active, hasuit, DROPSponsor, employees, ownership, owner, nonUs, recruit, tollfree1, tollfree2, tf2Note, DROPPhone, fax, street1, street2, city, DROPState, zip, country, complexCnum, partnership, uppername, upperabname, inProcess, legalStructure, phone, state, entryDate, entryUser, updateDate, updateUser, URL, wantOtherDirectorMailings, enterpriseSubscriber, enterpriseSubscribeDate, enterpriseUnsubscribeDate, URL_2, enterpriseSubscriberUsers, urlText, regionId, hasGlobalFunds, icimInsured, multiSeriesTrust);
    }}




