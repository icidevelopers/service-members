package org.ici.members.entity.staging_entity;

public class CompanyStagingId {
    private int cnum;
    private int revision;

    public CompanyStagingId(int cnum, int revision) {
        this.cnum = cnum;
        this.revision = revision;
    }

    public CompanyStagingId() {
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}

