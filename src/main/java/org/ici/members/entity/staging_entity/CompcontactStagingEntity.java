package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "compcontact_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CompContactStagingId.class)
public class CompcontactStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "DROP_cancelled", nullable = true)
    private Date dropCancelled;
    @Basic
    @Column(name = "purpose", nullable = false)
    @Id
    private short purpose;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "title", nullable = true, length = 64)
    private String title;
    @Basic
    @Column(name = "cnum", nullable = false)
    @Id
    private int cnum;
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private Timestamp entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private Timestamp updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "elect_date", nullable = true)
    private Date electDate;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public Date getDropCancelled() {
        return dropCancelled;
    }

    public void setDropCancelled(Date dropCancelled) {
        this.dropCancelled = dropCancelled;
    }

    public short getPurpose() {
        return purpose;
    }

    public void setPurpose(short purpose) {
        this.purpose = purpose;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public Timestamp getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Timestamp entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getElectDate() {
        return electDate;
    }

    public void setElectDate(Date electDate) {
        this.electDate = electDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompcontactStagingEntity that = (CompcontactStagingEntity) o;
        return purpose == that.purpose && cnum == that.cnum && pnum == that.pnum && Objects.equals(dropCancelled, that.dropCancelled) && Objects.equals(title, that.title) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser) && Objects.equals(electDate, that.electDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dropCancelled, purpose, title, cnum, pnum, entryDate, entryUser, updateDate, updateUser, electDate);
    }
}
