package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "current_membership_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(CurrentMembershipStagingId.class)
public class CurrentMembershipStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "cnum", nullable = false)
    @Id
    private int cnum;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "member", nullable = false, length = 1)
    private String member;
    @Basic
    @Column(name = "inactdt", nullable = true)
    private String inactdt;
    @Basic
    @Column(name = "type", nullable = false)
    private short type;
    @Basic
    @Column(name = "indirect", nullable = true, length = 1)
    private String indirect;
    @Basic
    @Column(name = "anniv", nullable = true)
    private String anniv;
    @Basic
    @Column(name = "billmo", nullable = true)
    private Boolean billmo;
    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    private String comments;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getInactdt() {
        return inactdt;
    }

    public void setInactdt(String inactdt) {
        this.inactdt = inactdt;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public String getIndirect() {
        return indirect;
    }

    public void setIndirect(String indirect) {
        this.indirect = indirect;
    }

    public String getAnniv() {
        return anniv;
    }

    public void setAnniv(String anniv) {
        this.anniv = anniv;
    }

    public Boolean getBillmo() {
        return billmo;
    }

    public void setBillmo(Boolean billmo) {
        this.billmo = billmo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrentMembershipStagingEntity that = (CurrentMembershipStagingEntity) o;
        return cnum == that.cnum && type == that.type && Objects.equals(member, that.member) && Objects.equals(inactdt, that.inactdt) && Objects.equals(indirect, that.indirect) && Objects.equals(anniv, that.anniv) && Objects.equals(billmo, that.billmo) && Objects.equals(comments, that.comments) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cnum, member, inactdt, type, indirect, anniv, billmo, comments, entryDate, entryUser, updateDate, updateUser);
    }
}
