package org.ici.members.entity.staging_entity;

import java.util.Objects;

public class ESppStagingEntityKey {

    private int cnum;
    private int code;
    private int revision;

    public ESppStagingEntityKey() {
    }

    public ESppStagingEntityKey(int cnum, int code, int revision) {
        this.cnum = cnum;
        this.code = code;
        this.revision = revision;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getCnum() {
        return cnum;
    }

    public void setCnum(int cnum) {
        this.cnum = cnum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ESppStagingEntityKey that = (ESppStagingEntityKey) o;
        return cnum == that.cnum && code == that.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cnum, code);
    }
}
