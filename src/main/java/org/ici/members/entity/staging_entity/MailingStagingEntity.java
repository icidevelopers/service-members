package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "mailing_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(MailingStagingEntityKey.class)
public class MailingStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "code", nullable = false)
    @Id
    private int code;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "name", nullable = false, length = 255)
    private String name;
    @Basic
    @Column(name = "department", nullable = false)
    private int department;
    @Basic
    @Column(name = "description", nullable = true, length = 255)
    private String description;
    @Basic
    @Column(name = "fmo", nullable = true, length = 1)
    private String fmo;
    @Basic
    @Column(name = "committee", nullable = true)
    private Integer committee;
    @Basic
    @Column(name = "mailing_type", nullable = true)
    private Short mailingType;
    @Basic
    @Column(name = "mailing_description", nullable = true, length = -1)
    private String mailingDescription;
    @Basic
    @Column(name = "group_discount", nullable = true, length = 1)
    private String groupDiscount;
    @Basic
    @Column(name = "verification_report_exclude", nullable = true, length = 1)
    private String verificationReportExclude;
    @Basic
    @Column(name = "send_attachments", nullable = true, length = 1)
    private String sendAttachments;
    @Basic
    @Column(name = "finder_visible", nullable = true, length = 1)
    private String finderVisible;
    @Basic
    @Column(name = "icinet_available", nullable = true, length = 1)
    private String icinetAvailable;
    @Basic
    @Column(name = "billed_separately", nullable = true, length = 1)
    private String billedSeparately;
    @Basic
    @Column(name = "one_free_per_member_complex", nullable = true, length = 1)
    private String oneFreePerMemberComplex;
    @Basic
    @Column(name = "status", nullable = true, length = 1)
    private String status;
    @Basic
    @Column(name = "status_date", nullable = true)
    private String statusDate;
    @Basic
    @Column(name = "primary_contact", nullable = true)
    private Integer primaryContact;
    @Basic
    @Column(name = "account_number", nullable = true, length = 32)
    private String accountNumber;
    @Basic
    @Column(name = "include_member_profile", nullable = true, length = 1)
    private String includeMemberProfile;
    @Basic
    @Column(name = "exclude_list", nullable = true, length = 1)
    private String excludeList;
    @Basic
    @Column(name = "event_date", nullable = true)
    private String eventDate;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFmo() {
        return fmo;
    }

    public void setFmo(String fmo) {
        this.fmo = fmo;
    }

    public Integer getCommittee() {
        return committee;
    }

    public void setCommittee(Integer committee) {
        this.committee = committee;
    }

    public Short getMailingType() {
        return mailingType;
    }

    public void setMailingType(Short mailingType) {
        this.mailingType = mailingType;
    }

    public String getMailingDescription() {
        return mailingDescription;
    }

    public void setMailingDescription(String mailingDescription) {
        this.mailingDescription = mailingDescription;
    }

    public String getGroupDiscount() {
        return groupDiscount;
    }

    public void setGroupDiscount(String groupDiscount) {
        this.groupDiscount = groupDiscount;
    }

    public String getVerificationReportExclude() {
        return verificationReportExclude;
    }

    public void setVerificationReportExclude(String verificationReportExclude) {
        this.verificationReportExclude = verificationReportExclude;
    }

    public String getSendAttachments() {
        return sendAttachments;
    }

    public void setSendAttachments(String sendAttachments) {
        this.sendAttachments = sendAttachments;
    }

    public String getFinderVisible() {
        return finderVisible;
    }

    public void setFinderVisible(String finderVisible) {
        this.finderVisible = finderVisible;
    }

    public String getIcinetAvailable() {
        return icinetAvailable;
    }

    public void setIcinetAvailable(String icinetAvailable) {
        this.icinetAvailable = icinetAvailable;
    }

    public String getBilledSeparately() {
        return billedSeparately;
    }

    public void setBilledSeparately(String billedSeparately) {
        this.billedSeparately = billedSeparately;
    }

    public String getOneFreePerMemberComplex() {
        return oneFreePerMemberComplex;
    }

    public void setOneFreePerMemberComplex(String oneFreePerMemberComplex) {
        this.oneFreePerMemberComplex = oneFreePerMemberComplex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public Integer getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(Integer primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIncludeMemberProfile() {
        return includeMemberProfile;
    }

    public void setIncludeMemberProfile(String includeMemberProfile) {
        this.includeMemberProfile = includeMemberProfile;
    }

    public String getExcludeList() {
        return excludeList;
    }

    public void setExcludeList(String excludeList) {
        this.excludeList = excludeList;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailingStagingEntity that = (MailingStagingEntity) o;
        return code == that.code && department == that.department && Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(fmo, that.fmo) && Objects.equals(committee, that.committee) && Objects.equals(mailingType, that.mailingType) && Objects.equals(mailingDescription, that.mailingDescription) && Objects.equals(groupDiscount, that.groupDiscount) && Objects.equals(verificationReportExclude, that.verificationReportExclude) && Objects.equals(sendAttachments, that.sendAttachments) && Objects.equals(finderVisible, that.finderVisible) && Objects.equals(icinetAvailable, that.icinetAvailable) && Objects.equals(billedSeparately, that.billedSeparately) && Objects.equals(oneFreePerMemberComplex, that.oneFreePerMemberComplex) && Objects.equals(status, that.status) && Objects.equals(statusDate, that.statusDate) && Objects.equals(primaryContact, that.primaryContact) && Objects.equals(accountNumber, that.accountNumber) && Objects.equals(includeMemberProfile, that.includeMemberProfile) && Objects.equals(excludeList, that.excludeList) && Objects.equals(eventDate, that.eventDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, department, description, fmo, committee, mailingType, mailingDescription, groupDiscount, verificationReportExclude, sendAttachments, finderVisible, icinetAvailable, billedSeparately, oneFreePerMemberComplex, status, statusDate, primaryContact, accountNumber, includeMemberProfile, excludeList, eventDate);
    }
}
