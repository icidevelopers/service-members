package org.ici.members.entity.staging_entity;

public class MailingStagingEntityKey {
    private int code;
    private int revision;

    public MailingStagingEntityKey(int code, int revision) {
        this.code = code;
        this.revision = revision;
    }

    public MailingStagingEntityKey() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}
