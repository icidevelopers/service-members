package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "mailrecip_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(MailrecipStagingEntityKey.class)
public class MailrecipStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name = "code", nullable = false)
    @Id
    private int code;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "copies", nullable = false)
    private short copies;
    @Basic
    @Column(name = "cnum", nullable = true)
    private Integer cnum;
    @Basic
    @Column(name = "billpnum", nullable = true)
    private Integer billpnum;
    @Basic
    @Column(name = "department", nullable = true)
    private Integer department;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "start_date", nullable = true)
    private String startDate;
    @Basic
    @Column(name = "end_date", nullable = true)
    private String endDate;
    @Basic
    @Column(name = "discount", nullable = true, length = 1)
    private String discount;
    @Basic
    @Column(name = "complimentary_end_date", nullable = true)
    private String complimentaryEndDate;
    @Basic
    @Column(name = "complimentary_reason", nullable = true, length = 255)
    private String complimentaryReason;
    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    private String comments;
    @Basic
    @Column(name = "fee_status", nullable = true)
    private Integer feeStatus;
    @Basic
    @Column(name = "billmonth", nullable = true)
    private Integer billmonth;
    @Basic
    @Column(name = "cancel_date", nullable = true)
    private String cancelDate;
    @Basic
    @Column(name = "initial_date", nullable = true)
    private String initialDate;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public short getCopies() {
        return copies;
    }

    public void setCopies(short copies) {
        this.copies = copies;
    }

    public Integer getCnum() {
        return cnum;
    }

    public void setCnum(Integer cnum) {
        this.cnum = cnum;
    }

    public Integer getBillpnum() {
        return billpnum;
    }

    public void setBillpnum(Integer billpnum) {
        this.billpnum = billpnum;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getComplimentaryEndDate() {
        return complimentaryEndDate;
    }

    public void setComplimentaryEndDate(String complimentaryEndDate) {
        this.complimentaryEndDate = complimentaryEndDate;
    }

    public String getComplimentaryReason() {
        return complimentaryReason;
    }

    public void setComplimentaryReason(String complimentaryReason) {
        this.complimentaryReason = complimentaryReason;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getFeeStatus() {
        return feeStatus;
    }

    public void setFeeStatus(Integer feeStatus) {
        this.feeStatus = feeStatus;
    }

    public Integer getBillmonth() {
        return billmonth;
    }

    public void setBillmonth(Integer billmonth) {
        this.billmonth = billmonth;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(String initialDate) {
        this.initialDate = initialDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailrecipStagingEntity that = (MailrecipStagingEntity) o;
        return pnum == that.pnum && code == that.code && copies == that.copies && Objects.equals(cnum, that.cnum) && Objects.equals(billpnum, that.billpnum) && Objects.equals(department, that.department) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(startDate, that.startDate) && Objects.equals(endDate, that.endDate) && Objects.equals(discount, that.discount) && Objects.equals(complimentaryEndDate, that.complimentaryEndDate) && Objects.equals(complimentaryReason, that.complimentaryReason) && Objects.equals(comments, that.comments) && Objects.equals(feeStatus, that.feeStatus) && Objects.equals(billmonth, that.billmonth) && Objects.equals(cancelDate, that.cancelDate) && Objects.equals(initialDate, that.initialDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, code, copies, cnum, billpnum, department, entryDate, entryUser, startDate, endDate, discount, complimentaryEndDate, complimentaryReason, comments, feeStatus, billmonth, cancelDate, initialDate);
    }
}
