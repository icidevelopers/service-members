package org.ici.members.entity.staging_entity;

public class MailrecipStagingEntityKey {

    private int pnum;
    private int code;
    private int revision;

    public MailrecipStagingEntityKey() {
    }

    public MailrecipStagingEntityKey(int pnum, int code, int revision) {
        this.pnum = pnum;
        this.code = code;
        this.revision = revision;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
