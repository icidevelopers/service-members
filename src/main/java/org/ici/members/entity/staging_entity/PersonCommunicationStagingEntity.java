package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;


import java.util.Objects;

@Entity
@Table(name = "person_communication_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(PersonCommunicationStagingKey.class)
public class PersonCommunicationStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "pc_id", nullable = false)
    @Id
    private int pcId;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "pnum", nullable = false)
    private int pnum;
    @Basic
    @Column(name = "communication_date", nullable = false)
    private String communicationDate;
    @Basic
    @Column(name = "communication_type", nullable = true)
    private Integer communicationType;
    @Basic
    @Column(name = "comment", nullable = true, length = -1)
    private String comment;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private String updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 30)
    private String recordOperation;

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getRecordOperation() {
        return recordOperation;
    }

    public void setRecordOperation(String recordOperation) {
        this.recordOperation = recordOperation;
    }

    public int getPcId() {
        return pcId;
    }

    public void setPcId(int pcId) {
        this.pcId = pcId;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public String getCommunicationDate() {
        return communicationDate;
    }

    public void setCommunicationDate(String communicationDate) {
        this.communicationDate = communicationDate;
    }

    public Integer getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(Integer communicationType) {
        this.communicationType = communicationType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonCommunicationStagingEntity that = (PersonCommunicationStagingEntity) o;
        return pcId == that.pcId && pnum == that.pnum && Objects.equals(communicationDate, that.communicationDate) && Objects.equals(communicationType, that.communicationType) && Objects.equals(comment, that.comment) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pcId, pnum, communicationDate, communicationType, comment, entryDate, entryUser, updateDate, updateUser);
    }
}
