package org.ici.members.entity.staging_entity;

public class PersonCommunicationStagingKey {
    private int pcId;
    private int revision;

    public PersonCommunicationStagingKey(int pc_id, int revision) {
        this.pcId = pc_id;
        this.revision = revision;
    }

    public PersonCommunicationStagingKey() {
    }

    public int getPcId() {
        return pcId;
    }

    public void setPcId(int pcId) {
        this.pcId = pcId;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}
