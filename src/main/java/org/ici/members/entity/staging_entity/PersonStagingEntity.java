package org.ici.members.entity.staging_entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "person_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(PersonStagingEntityKey.class)
public class PersonStagingEntity extends StagingEntity {
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name="revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "prefix", nullable = true, length = 20)
    private String prefix;
    @Basic
    @Column(name = "fname", nullable = true, length = 20)
    private String fname;
    @Basic
    @Column(name = "lname", nullable = false, length = 30)
    private String lname;
    @Basic
    @Column(name = "middle", nullable = true, length = 15)
    private String middle;
    @Basic
    @Column(name = "title", nullable = true, length = 100)
    private String title;
    @Basic
    @Column(name = "party", nullable = true, length = 1)
    private String party;
    @Basic
    @Column(name = "dcrep", nullable = true, length = 1)
    private String dcrep;
    @Basic
    @Column(name = "comment", nullable = true, length = 255)
    private String comment;
    @Basic
    @Column(name = "DROP_phone", nullable = true, length = 25)
    private String dropPhone;
    @Basic
    @Column(name = "fax", nullable = true, length = 25)
    private String fax;
    @Basic
    @Column(name = "suffix", nullable = true, length = 20)
    private String suffix;
    @Basic
    @Column(name = "department", nullable = true)
    private Integer department;
    @Basic
    @Column(name = "in_process", nullable = true, length = 1)
    private String inProcess;
    @Basic
    @Column(name = "email", nullable = true, length = 100)
    private String email;
    @Basic
    @Column(name = "status", nullable = true, length = 1)
    private String status;
    @Basic
    @Column(name = "status_date", nullable = true)
    private String statusDate;
    @Basic
    @Column(name = "rcvattach", nullable = true, length = 1)
    private String rcvattach;
    @Basic
    @Column(name = "DROP_complimentary", nullable = true, length = 1)
    private String dropComplimentary;
    @Basic
    @Column(name = "upperlname", nullable = true, length = 30)
    private String upperlname;
    @Basic
    @Column(name = "complimentary_end_date", nullable = true)
    private Date complimentaryEndDate;
    @Basic
    @Column(name = "complimentary_reason", nullable = true, length = 255)
    private String complimentaryReason;
    @Basic
    @Column(name = "phone", nullable = true, length = 255)
    private String phone;
    @Basic
    @Column(name = "person_salutation", nullable = true, length = 30)
    private String personSalutation;
    @Basic
    @Column(name = "entry_date", nullable = true)
    private String entryDate;
    @Basic
    @Column(name = "entry_user", nullable = true, length = 30)
    private String entryUser;
    @Basic
    @Column(name = "update_date", nullable = true)
    private Timestamp updateDate;
    @Basic
    @Column(name = "update_user", nullable = true, length = 30)
    private String updateUser;
    @Basic
    @Column(name = "complimentary", nullable = true)
    private Short complimentary;
    @Basic
    @Column(name = "billpnum", nullable = true)
    private Integer billpnum;
    @Basic
    @Column(name = "committee_mail_type", nullable = true, length = 1)
    private String committeeMailType;
    @Basic
    @Column(name = "other_mail_type", nullable = true, length = 1)
    private String otherMailType;
    @Basic
    @Column(name = "suspend_email", nullable = true, length = 1)
    private String suspendEmail;
    @Basic
    @Column(name = "legal_mail_type", nullable = true, length = 1)
    private String legalMailType;
    @Basic
    @Column(name = "mail_type_response", nullable = true)
    private Short mailTypeResponse;
    @Basic
    @Column(name = "block_mailings", nullable = true, length = 1)
    private String blockMailings;
    @Basic
    @Column(name = "mail_immediate", nullable = true, length = 1)
    private String mailImmediate;
    @Basic
    @Column(name = "mail_frequency", nullable = true, length = 1)
    private String mailFrequency;
    @Basic
    @Column(name = "assistant_pnum", nullable = true)
    private Integer assistantPnum;
    @Basic
    @Column(name = "cell_phone", nullable = true, length = 75)
    private String cellPhone;
    @Basic
    @Column(name = "enterprise_subscriber_lite", nullable = true, length = 1)
    private String enterpriseSubscriberLite;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 10)
    private String recordOperation;
    @Basic
    @Column(name = "update_membership", nullable = true, length = 1)
    private String updateMembership;
    @Basic
    @Column(name = "previous_pnum", nullable = true)
    private Integer previousPnum;


    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParty() {
        return party;
    }

    public void setParty(String party) {
        this.party = party;
    }

    public String getDcrep() {
        return dcrep;
    }

    public void setDcrep(String dcrep) {
        this.dcrep = dcrep;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDropPhone() {
        return dropPhone;
    }

    public void setDropPhone(String dropPhone) {
        this.dropPhone = dropPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getInProcess() {
        return inProcess;
    }

    public void setInProcess(String inProcess) {
        this.inProcess = inProcess;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getRcvattach() {
        return rcvattach;
    }

    public void setRcvattach(String rcvattach) {
        this.rcvattach = rcvattach;
    }

    public String getDropComplimentary() {
        return dropComplimentary;
    }

    public void setDropComplimentary(String dropComplimentary) {
        this.dropComplimentary = dropComplimentary;
    }

    public String getUpperlname() {
        return upperlname;
    }

    public void setUpperlname(String upperlname) {
        this.upperlname = upperlname;
    }

    public Date getComplimentaryEndDate() {
        return complimentaryEndDate;
    }

    public void setComplimentaryEndDate(Date complimentaryEndDate) {
        this.complimentaryEndDate = complimentaryEndDate;
    }

    public String getComplimentaryReason() {
        return complimentaryReason;
    }

    public void setComplimentaryReason(String complimentaryReason) {
        this.complimentaryReason = complimentaryReason;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonSalutation() {
        return personSalutation;
    }

    public void setPersonSalutation(String personSalutation) {
        this.personSalutation = personSalutation;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryUser() {
        return entryUser;
    }

    public void setEntryUser(String entryUser) {
        this.entryUser = entryUser;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Short getComplimentary() {
        return complimentary;
    }

    public void setComplimentary(Short complimentary) {
        this.complimentary = complimentary;
    }

    public Integer getBillpnum() {
        return billpnum;
    }

    public void setBillpnum(Integer billpnum) {
        this.billpnum = billpnum;
    }

    public String getCommitteeMailType() {
        return committeeMailType;
    }

    public void setCommitteeMailType(String committeeMailType) {
        this.committeeMailType = committeeMailType;
    }

    public String getOtherMailType() {
        return otherMailType;
    }

    public void setOtherMailType(String otherMailType) {
        this.otherMailType = otherMailType;
    }

    public String getSuspendEmail() {
        return suspendEmail;
    }

    public void setSuspendEmail(String suspendEmail) {
        this.suspendEmail = suspendEmail;
    }

    public String getLegalMailType() {
        return legalMailType;
    }

    public void setLegalMailType(String legalMailType) {
        this.legalMailType = legalMailType;
    }

    public Short getMailTypeResponse() {
        return mailTypeResponse;
    }

    public void setMailTypeResponse(Short mailTypeResponse) {
        this.mailTypeResponse = mailTypeResponse;
    }

    public String getBlockMailings() {
        return blockMailings;
    }

    public void setBlockMailings(String blockMailings) {
        this.blockMailings = blockMailings;
    }

    public String getMailImmediate() {
        return mailImmediate;
    }

    public void setMailImmediate(String mailImmediate) {
        this.mailImmediate = mailImmediate;
    }

    public String getMailFrequency() {
        return mailFrequency;
    }

    public void setMailFrequency(String mailFrequency) {
        this.mailFrequency = mailFrequency;
    }

    public Integer getAssistantPnum() {
        return assistantPnum;
    }

    public void setAssistantPnum(Integer assistantPnum) {
        this.assistantPnum = assistantPnum;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEnterpriseSubscriberLite() {
        return enterpriseSubscriberLite;
    }

    public void setEnterpriseSubscriberLite(String enterpriseSubscriberLite) {
        this.enterpriseSubscriberLite = enterpriseSubscriberLite;
    }

    public String getRecordOperation() {
        return recordOperation;
    }

    public void setRecordOperation(String recordOperation) {
        this.recordOperation = recordOperation;
    }

    public String getUpdateMembership() {
        return updateMembership;
    }

    public void setUpdateMembership(String updateMembership) {
        this.updateMembership = updateMembership;
    }

    public Integer getPreviousPnum() {
        return previousPnum;
    }

    public void setPreviousPnum(Integer previousPnum) {
        this.previousPnum = previousPnum;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonStagingEntity that = (PersonStagingEntity) o;
        return pnum == that.pnum && revision == that.revision && Objects.equals(prefix, that.prefix) && Objects.equals(fname, that.fname) && Objects.equals(lname, that.lname) && Objects.equals(middle, that.middle) && Objects.equals(title, that.title) && Objects.equals(party, that.party) && Objects.equals(dcrep, that.dcrep) && Objects.equals(comment, that.comment) && Objects.equals(dropPhone, that.dropPhone) && Objects.equals(fax, that.fax) && Objects.equals(suffix, that.suffix) && Objects.equals(department, that.department) && Objects.equals(inProcess, that.inProcess) && Objects.equals(email, that.email) && Objects.equals(status, that.status) && Objects.equals(statusDate, that.statusDate) && Objects.equals(rcvattach, that.rcvattach) && Objects.equals(dropComplimentary, that.dropComplimentary) && Objects.equals(upperlname, that.upperlname) && Objects.equals(complimentaryEndDate, that.complimentaryEndDate) && Objects.equals(complimentaryReason, that.complimentaryReason) && Objects.equals(phone, that.phone) && Objects.equals(personSalutation, that.personSalutation) && Objects.equals(entryDate, that.entryDate) && Objects.equals(entryUser, that.entryUser) && Objects.equals(updateDate, that.updateDate) && Objects.equals(updateUser, that.updateUser) && Objects.equals(complimentary, that.complimentary) && Objects.equals(billpnum, that.billpnum) && Objects.equals(committeeMailType, that.committeeMailType) && Objects.equals(otherMailType, that.otherMailType) && Objects.equals(suspendEmail, that.suspendEmail) && Objects.equals(legalMailType, that.legalMailType) && Objects.equals(mailTypeResponse, that.mailTypeResponse) && Objects.equals(blockMailings, that.blockMailings) && Objects.equals(mailImmediate, that.mailImmediate) && Objects.equals(mailFrequency, that.mailFrequency) && Objects.equals(assistantPnum, that.assistantPnum) && Objects.equals(cellPhone, that.cellPhone) && Objects.equals(enterpriseSubscriberLite, that.enterpriseSubscriberLite) && Objects.equals(recordOperation, that.recordOperation) && Objects.equals(updateMembership, that.updateMembership) && Objects.equals(previousPnum, that.previousPnum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, prefix, fname, lname, middle, title, party, dcrep, comment, dropPhone, fax, suffix, department, inProcess, email, status, statusDate, rcvattach, dropComplimentary, upperlname, complimentaryEndDate, complimentaryReason, phone, personSalutation, entryDate, entryUser, updateDate, updateUser, complimentary, billpnum, committeeMailType, otherMailType, suspendEmail, legalMailType, mailTypeResponse, blockMailings, mailImmediate, mailFrequency, assistantPnum, cellPhone, enterpriseSubscriberLite, recordOperation, updateMembership, previousPnum, revision);
    }
}
