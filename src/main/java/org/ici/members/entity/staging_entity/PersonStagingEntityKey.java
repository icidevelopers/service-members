package org.ici.members.entity.staging_entity;

public class PersonStagingEntityKey {
    private int pnum;
    private int revision;

    public PersonStagingEntityKey(int pnum, int revision) {
        this.pnum = pnum;
        this.revision = revision;
    }
    public PersonStagingEntityKey() {}

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}
