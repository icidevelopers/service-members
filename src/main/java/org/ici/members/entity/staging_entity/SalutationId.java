package org.ici.members.entity.staging_entity;

import java.util.Objects;

public class SalutationId {
    private int pnum;
    private int code;
    private int revision;

    public SalutationId() {
    }

    public SalutationId(int pnum, int code, int revision) {
        this.pnum = pnum;
        this.code = code;
        this.revision = revision;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SalutationId that = (SalutationId) o;
        return pnum == that.pnum && code == that.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnum, code);
    }
}
