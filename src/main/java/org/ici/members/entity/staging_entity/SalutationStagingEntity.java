package org.ici.members.entity.staging_entity;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "salutation_staging", schema = "dbo", catalog = "membership_staging_crm")
@IdClass(SalutationId.class)
public class SalutationStagingEntity extends StagingEntity{
    @Basic
    @Column(name = "salutation", nullable = false, length = 30)
    private String salutation;
    @Basic
    @Column(name = "pnum", nullable = false)
    @Id
    private int pnum;
    @Basic
    @Column(name = "revision", nullable = false)
    @Id
    private int revision;
    @Basic
    @Column(name = "code", nullable = false)
    @Id
    private int code;
    @Basic
    @Column(name = "impac_salutation", nullable = true, length = 1)
    private String impacSalutation;
    @Basic
    @Column(name = "record_operation", nullable = true, length = 255)
    private String recordOpertaion;

    public String getRecordOpertaion() {
        return recordOpertaion;
    }

    public void setRecordOpertaion(String recordOpertaion) {
        this.recordOpertaion = recordOpertaion;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public int getPnum() {
        return pnum;
    }

    public void setPnum(int pnum) {
        this.pnum = pnum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getImpacSalutation() {
        return impacSalutation;
    }

    public void setImpacSalutation(String impacSalutation) {
        this.impacSalutation = impacSalutation;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SalutationStagingEntity that = (SalutationStagingEntity) o;
        return pnum == that.pnum && code == that.code && Objects.equals(salutation, that.salutation) && Objects.equals(impacSalutation, that.impacSalutation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(salutation, pnum, code, impacSalutation);
    }
}
