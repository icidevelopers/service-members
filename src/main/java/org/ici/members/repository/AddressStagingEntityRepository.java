package org.ici.members.repository;

import org.ici.members.entity.staging_entity.AddressStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressStagingEntityRepository extends JpaRepository<AddressStagingEntity, Integer> {
}