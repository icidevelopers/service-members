package org.ici.members.repository;

import org.ici.members.entity.staging_entity.BoardCommitRepStagingKey;
import org.ici.members.entity.staging_entity.BoardCommitrepStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardCommitrepStagingEntityRepository extends JpaRepository<BoardCommitrepStagingEntity, BoardCommitRepStagingKey> {
}