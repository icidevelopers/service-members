package org.ici.members.repository;

import org.ici.members.entity.staging_entity.BoardCommitteeStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardCommitteeStagingEntityRepository extends JpaRepository<BoardCommitteeStagingEntity, Integer> {
}