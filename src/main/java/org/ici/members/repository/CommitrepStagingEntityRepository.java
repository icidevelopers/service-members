package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CommitrepId;
import org.ici.members.entity.staging_entity.CommitrepStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommitrepStagingEntityRepository extends JpaRepository<CommitrepStagingEntity, CommitrepId> {
}