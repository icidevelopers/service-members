package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CommitteeMeetingAttendeesStagingEntity;
import org.ici.members.entity.staging_entity.CommitteeMeetingsAttendeesStagingId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommitteeMeetingsAttendeesStagingEntityRepository extends JpaRepository<CommitteeMeetingAttendeesStagingEntity, CommitteeMeetingsAttendeesStagingId> {
}
