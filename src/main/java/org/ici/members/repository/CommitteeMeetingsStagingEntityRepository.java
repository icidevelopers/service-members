package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CommitteeMeetingsStagingEntity;
import org.ici.members.entity.staging_entity.CommitteeMeetingsStagingId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommitteeMeetingsStagingEntityRepository extends JpaRepository<CommitteeMeetingsStagingEntity, CommitteeMeetingsStagingId> {
}