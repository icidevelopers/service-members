package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CommitteeStagingEntity;
import org.ici.members.entity.staging_entity.CommitteeStagingKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommitteeStagingEntityRepository extends JpaRepository<CommitteeStagingEntity, CommitteeStagingKey> {
}