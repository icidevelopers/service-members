package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CompContactStagingId;
import org.ici.members.entity.staging_entity.CompcontactStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CompContactStagingEntityRepository extends JpaRepository<CompcontactStagingEntity, CompContactStagingId> {
    @Query("select c from CompcontactStagingEntity c where c.cnum = :#{#compcontactstagingid.cnum} and c.pnum = :#{#compcontactstagingid.pnum} and c.purpose = :#{#compcontactstagingid.purpose}")
    public List<CompcontactStagingEntity> findExistingCompContacts(@Param("compcontactstagingid") CompContactStagingId compContactStagingId);
}
