package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CompanyCommunicationStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyCommunicationStagingEntityRepository extends JpaRepository<CompanyCommunicationStagingEntity, Integer> {
}