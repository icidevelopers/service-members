package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CompanyRelationStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRelationStagingEntityRepository extends JpaRepository<CompanyRelationStagingEntity, Integer> {
}