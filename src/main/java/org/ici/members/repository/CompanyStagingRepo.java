package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CompanyStagingEntity;
import org.ici.members.entity.staging_entity.CompanyStagingId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface CompanyStagingRepo extends JpaRepository<CompanyStagingEntity, CompanyStagingId>{
    @Query("SELECT c FROM CompanyStagingEntity c where c.cnum = :cnum")
    public List<CompanyStagingEntity> findByCnum(@Param("cnum") Integer cnum);
}
