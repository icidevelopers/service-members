package org.ici.members.repository;

import org.ici.members.entity.staging_entity.CurrentMembershipStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrentMembershipStagingEntityRepository extends JpaRepository<CurrentMembershipStagingEntity, Integer> {
}