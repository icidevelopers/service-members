package org.ici.members.repository;

import org.ici.members.entity.staging_entity.ESppStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ESppStagingEntityRepository extends JpaRepository<ESppStagingEntity, Integer> {
}