package org.ici.members.repository;

import org.ici.members.entity.staging_entity.MailingStagingEntity;
import org.ici.members.entity.staging_entity.MailingStagingEntityKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailingStagingEntityRepository extends JpaRepository<MailingStagingEntity, MailingStagingEntityKey> {
}