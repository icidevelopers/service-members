package org.ici.members.repository;

import org.ici.members.db.domain.membership.MailrecipKey;
import org.ici.members.entity.staging_entity.MailrecipStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailrecipStagingRepo extends JpaRepository<MailrecipStagingEntity, MailrecipKey> {
}
