package org.ici.members.repository;

import org.ici.members.entity.staging_entity.PersonCommunicationStagingEntity;
import org.ici.members.entity.staging_entity.PersonCommunicationStagingKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonCommunicationStagingEntityRepository extends JpaRepository<PersonCommunicationStagingEntity, PersonCommunicationStagingKey> {
}