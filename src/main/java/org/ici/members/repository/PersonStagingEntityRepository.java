package org.ici.members.repository;

import org.ici.members.entity.staging_entity.PersonStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonStagingEntityRepository extends JpaRepository<PersonStagingEntity, Integer> {
}