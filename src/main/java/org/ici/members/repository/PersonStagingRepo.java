package org.ici.members.repository;


import org.ici.members.entity.staging_entity.PersonStagingEntity;
import org.ici.members.entity.staging_entity.PersonStagingEntityKey;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface PersonStagingRepo extends JpaRepository<PersonStagingEntity, PersonStagingEntityKey> {

}
