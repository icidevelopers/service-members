package org.ici.members.repository;

import org.ici.members.entity.staging_entity.SalutationStagingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalutationStagingEntityRepository extends JpaRepository<SalutationStagingEntity, Integer> {
}