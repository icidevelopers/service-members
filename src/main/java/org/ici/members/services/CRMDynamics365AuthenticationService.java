package org.ici.members.services;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.aad.msal4j.ClientCredentialFactory;
import com.microsoft.aad.msal4j.ClientCredentialParameters;
import com.microsoft.aad.msal4j.ConfidentialClientApplication;
import com.microsoft.aad.msal4j.IClientCredential;
import com.microsoft.graph.requests.GraphServiceClient;
import jakarta.persistence.Id;
import org.ici.members.entity.kafka.AzureRequestTopicObject;
import org.ici.members.entity.staging_entity.*;
import org.ici.members.repository.*;
import org.ici.services.domain.dto.crm.ICIRSParent;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.IcirsCrmtoiciaddresses;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEEREPS.IcirsCrmtoiciboardcommitteereps;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEES.IcirsCrmtoiciboardcommittees;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITREPS.IcirsCrmtoicicommitreps;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEEMEETINGSES.IcirsCrmtoicicommitteemeetingses;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEES.IcirsCrmtoicicommittees;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEE_MEETING_ATTENDEESES.IcirsCrmtoicicommitteeMeetingAttendeeses;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANIESES.IcirsCrmtoicicompanieses;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYCOMMUNICATIONS.IcirsCrmtoicicompanycommunications;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYRELATIONS.IcirsCrmtoicicompanyrelations;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.IcirsCrmtoicicontactpurposes;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTSES.IcirsCrmtoicicontactses;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIESPPS.IcirsCrmtoiciespps;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGRECIPS.IcirsCrmtoicimailingrecips;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGS.IcirsCrmtoicimailings;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMEMBERSHIPS.IcirsCrmtoicimemberships;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICIPERSONCOMMUNICATIONS.IcirsCrmtoicipersoncommunications;
import org.ici.services.domain.dto.crm.ICIRS_CRMTOICISALUTATIONS.IcirsCrmtoicisalutations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.RestTemplate;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.capitalize;

@Service
public class CRMDynamics365AuthenticationService {
    @Value("${aad.tenantId}")
    private String TENANT_ID;

    @Value("${crm.clientId}")
    private String CLIENT_ID;

    @Value("${crm.secret}")
    private String SECRET;

    @Value("${crm.scope}")
    private String SCOPE;

    @Autowired
    private PersonStagingRepo personStagingRepo;
    @Autowired
    private CompanyStagingRepo companyStagingRepo;
    @Autowired
    private ConversionService conversionService;
    @Autowired
    private KafkaTemplate<String, AzureRequestTopicObject> kafkaTemplate;

    private static GraphServiceClient graphServiceClient;

    private static final String AZURE_CRM_ICI_CONTACTSES = "icirs_crmtoicicontactses";
    private static final String AZURE_CRM_ICI_ADDRESSES = "icirs_crmtoiciaddresses";
    private static final String AZURE_CRM_ICI_BOARD_COMMITTEES = "icirs_crmtoiciboardcommittees";
    private static final String AZURE_CRM_ICI_BOARD_COMMITTEES_REP = "icirs_crmtoiciboardcommitteereps";
    private static final String AZURE_CRM_ICI_COMMIT_REPS = "icirs_crmtoicicommitreps";
    private static final String AZURE_CRM_ICI_COMMITTEES = "icirs_crmtoicicommittees";
    private static final String AZURE_CRM_ICI_COMPANIES = "icirs_crmtoicicompanieses";
    private static final String AZURE_CRM_ICI_COMPANY_COMMUNICATIONS = "icirs_crmtoicicompanycommunications";
    private static final String AZURE_CRM_ICI_COMPANY_RELATIONS = "icirs_crmtoicicompanyrelations";
    private static final String AZURE_CRM_ICI_CONTACT_PURPOSES = "icirs_crmtoicicontactpurposes";
    private static final String AZURE_CRM_ICI_ESPPS = "icirs_crmtoiciespps";
    private static final String AZURE_CRM_ICI_MAILING_RECIPS = "icirs_crmtoicimailingrecips";
    private static final String AZURE_CRM_ICI_MAILINGS = "icirs_crmtoicimailings";
    private static final String AZURE_CRM_ICI_MEMBERSHIP = "icirs_crmtoicimemberships";
    private static final String AZURE_CRM_ICI_PERSON_COMMUNICATIONS = "icirs_crmtoicipersoncommunications";
    private static final String AZURE_CRM_ICI_SALUTATIONS = "icirs_crmtoicisalutations";
    private static final String AZURE_CRM_ICI_COMMITTEE_MEETINGS = "icirs_crmtoicicommitteemeetingses";
    private static final String AZURE_CRM_ICI__COMMITTEE_MEETINGS_ATTENDEES = "icirs_crmtoicicommittee_meeting_attendeeses";

    private Logger log = LoggerFactory.getLogger(CRMDynamics365AuthenticationService.class);
    @Autowired
    private AddressStagingEntityRepository addressStagingEntityRepository;
    @Autowired
    private BoardCommitteeStagingEntityRepository boardCommitteeStagingEntityRepository;
    @Autowired
    private CommitteeStagingEntityRepository committeeStagingEntityRepository;
    @Autowired
    private BoardCommitrepStagingEntityRepository boardCommitrepStagingEntityRepository;
    @Autowired
    private ESppStagingEntityRepository eSppStagingEntityRepository;
    @Autowired
    private MailingStagingEntityRepository mailingStagingEntityRepository;
    @Autowired
    private CurrentMembershipStagingEntityRepository currentMembershipStagingEntityRepository;
    @Autowired
    private PersonStagingEntityRepository personStagingEntityRepository;
    @Autowired
    private PersonCommunicationStagingEntityRepository personCommunicationStagingEntityRepository;
    @Autowired
    private SalutationStagingEntityRepository salutationStagingEntityRepository;
    @Autowired
    private CompanyCommunicationStagingEntityRepository companyCommunicationStagingEntityRepository;
    @Autowired
    private CompanyRelationStagingEntityRepository companyRelationStagingEntityRepository;
    @Autowired
    private CommitrepStagingEntityRepository commitrepStagingEntityRepository;
    @Autowired
    private MailrecipStagingRepo mailrecipStagingRepo;
    @Autowired
    private CommitteeMeetingsStagingEntityRepository committeeMeetingsStagingEntityRepository;
    @Autowired
    private CommitteeMeetingsAttendeesStagingEntityRepository committeeMeetingsAttendeesStagingEntityRepository;
    @Autowired
    private CompContactStagingEntityRepository CompcontactStagingEntityRepository;
    @Autowired
    private CompContactStagingEntityRepository compcontactStagingEntityRepository;

    public String authenticateCrm() throws MalformedURLException {
        String authToken =  "";
        IClientCredential credential = ClientCredentialFactory.createFromSecret(SECRET);
        ConfidentialClientApplication cca = ConfidentialClientApplication.builder(CLIENT_ID, credential)
                .authority("https://login.microsoftonline.com/157aaf47-a05a-4f22-9ee0-7367b740ec6a")
                .build();
        Set<String> scope = Collections.singleton(SCOPE);
        ClientCredentialParameters parameters = ClientCredentialParameters.builder(scope).build();
        authToken = cca.acquireToken(parameters).join().accessToken();

        return authToken;
    }

    private Boolean convertObj(ResponseEntity<String> response, Class<? extends ICIRSParent> class1, Class<? extends StagingEntity> class2, ObjectMapper objectMapper, ConversionService converter, JpaRepository repository){
        try {
           ICIRSParent parentType = objectMapper.readValue(response.getBody(), class1);
           List parentTypeList = parentType.getValue();
           List stagingEntityVal = new ArrayList();
           for (int i =0; i<parentTypeList.size(); i++){
                 stagingEntityVal.add(converter.convert(parentTypeList.get(i), class2));
           }

          Map<Object, List<Object>> offset = findRevisionOffset(class2, repository, stagingEntityVal);
          List savedEntities = new ArrayList();
          for(Object key: offset.keySet()){
              List<Object> updatedObjects = offset.get(key);
              //NOTE using the individual save here instead of the batch save since the larger batches made the save operation choke
              for(Object updatedObject: updatedObjects){
                  repository.save(updatedObject);
              }
//              repository.saveAll(updatedObjects);
              for(Object updatedObj: updatedObjects){
                  savedEntities.add(updatedObj);
              }
          }

          return !savedEntities.isEmpty() && savedEntities.size() == stagingEntityVal.size();

    } catch (JsonProcessingException | InstantiationException | IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<Object, List<Object>> findRevisionOffset(Class<? extends StagingEntity> class2, JpaRepository repository, List stagingEntityVal) throws InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        Map<Object,List<Object>> result = new HashMap<>();

        List<Field> lookupFields = new ArrayList<>();
        List<Field> ignoreFields = new ArrayList<>();
        for(Field field: class2.getDeclaredFields()){
            if(field.isAnnotationPresent(Id.class) && !field.getName().equalsIgnoreCase("revision")){
                lookupFields.add(field);
            }
            else{
                ignoreFields.add(field);
            }
        }



        for(int i =0; i < stagingEntityVal.size(); i++){
            Object obj = class2.newInstance();
            Object objSrc = stagingEntityVal.get(i);
            for(Field field: lookupFields){
                Field decSrcField = objSrc.getClass().getDeclaredField(field.getName());
                decSrcField.setAccessible(true);
                String getterName = "get" + capitalize(field.getName());
                Method getter = obj.getClass().getMethod(getterName);
                String setterName = "set" + capitalize(field.getName());
                Method setter = obj.getClass().getMethod(setterName, field.getType());
                setter.invoke(obj, getter.invoke(objSrc));
            }

            if(result.get(obj) == null){
                List<String> ignoreColumnNames = ignoreFields
                        .stream()
                        .map(Field::getName)
                        .toList();
                ExampleMatcher objExampleMatcher = ExampleMatcher.matching()
                        .withIgnorePaths(ignoreColumnNames.toArray(new String[0]));

                Example objExample = Example.of(obj, objExampleMatcher);
                List objResult = repository.findAll(objExample);
                int currentOffset = 1 + objResult.size();
                currentOffset += getCurrentListKeyOffset(obj, stagingEntityVal, i);
                String revisionSetterStr = "setRevision";
                Method revisionSetter = objSrc.getClass().getMethod(revisionSetterStr, int.class);
                revisionSetter.invoke(objSrc, currentOffset);
                List updatedRevisionList = new ArrayList();
                updatedRevisionList.add(objSrc);
                result.put(obj, updatedRevisionList);
            } else{
                List resultVal = result.get(obj);
                Object resultValObj = resultVal.get(resultVal.size() - 1);
                String revisionGetterStr = "getRevision";
                resultValObj.getClass().getDeclaredField("revision").setAccessible(true);
                Method revisionGetter = resultValObj.getClass().getMethod(revisionGetterStr);

                int currentOffset = (Integer)revisionGetter.invoke(resultValObj) + 1;
                String revisionSetterStr = "setRevision";
                Method revisionSetter = objSrc.getClass().getMethod(revisionSetterStr, int.class);
                revisionSetter.invoke(objSrc, currentOffset);

                resultVal.add(objSrc);
                result.put(obj, resultVal);
            }

        }

        return result;
    }

    private int getCurrentListKeyOffset(Object key, List stagingEntityVal, int currentIdx){
        int resultOffset = 0;
        for(int i =0; i < currentIdx; i++){
            if(stagingEntityVal.get(i).equals(key)){
                resultOffset ++;
            }
        }
        return resultOffset;
    }

    private List<Integer> findMatches(Object obj, List<Field> idFields ,List stagingEntityVal) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<Integer> matchingIdx = new ArrayList<>();

        for(int i =0; i < stagingEntityVal.size(); i++){
            List<Boolean> declaredFieldsMatch = new ArrayList<>();
            for(Field field: idFields){
                stagingEntityVal.get(i).getClass().getDeclaredField(field.getName()).setAccessible(true);
                obj.getClass().getDeclaredField(field.getName()).setAccessible(true);
                String getterName = "get" + capitalize(field.getName());
                Method getter = obj.getClass().getMethod(getterName);

                Object stagingEntityKey = getter.invoke(stagingEntityVal.get(i));
                Object objKey = getter.invoke(obj);
                if(obj.getClass().getDeclaredField(field.getName()) != null && stagingEntityKey.equals(objKey)){
                    declaredFieldsMatch.add(true);
                }
            }
            if(declaredFieldsMatch.size() == idFields.size()){
                matchingIdx.add(i);
            }
        }
        return matchingIdx;
    }

    public void getAndSyncEntity(String tokenCredentialAuthProvider, String tableName) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + tokenCredentialAuthProvider);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            URL scopeUrl = new URL(SCOPE);
            String callUrl = scopeUrl.getProtocol() + "://" + scopeUrl.getHost() + "/api/data/v9.2/" + tableName + "?$filter=icirs_ici_push eq false&$orderby=modifiedon asc";
            ResponseEntity<String> response = restTemplate.exchange(callUrl, HttpMethod.GET, entity, String.class);

            if (response.getStatusCode().is2xxSuccessful()) {
                ObjectMapper objectMapper = new ObjectMapper();
                switch (tableName.toLowerCase()) {
                    case AZURE_CRM_ICI_CONTACTSES:
                        Boolean contactConversion = convertObj(response, IcirsCrmtoicicontactses.class, PersonStagingEntity.class, objectMapper, conversionService, personStagingRepo);
                        if(contactConversion){
                            IcirsCrmtoicicontactses contacts = objectMapper.readValue(response.getBody(), IcirsCrmtoicicontactses.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTSES.Value> contactsValue = contacts.getValue();

                            List<String> azureIds = contactsValue.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTSES.Value::getIcirsCrmtoicicontactsid)
                                    .toList();
                            for (String azureId : azureIds) {
                                updateAzureFlag(tableName, callUrl, azureId, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_ADDRESSES:
                        Boolean addressConversion = convertObj(response, IcirsCrmtoiciaddresses.class, AddressStagingEntity.class, objectMapper, conversionService, addressStagingEntityRepository);
                        if (addressConversion) {
                            IcirsCrmtoiciaddresses icirsCrmtoiciaddresses = objectMapper.readValue(response.getBody(), IcirsCrmtoiciaddresses.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.Value> addresses = icirsCrmtoiciaddresses.getValue();

                            List<String> idValStrAdd = addresses.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.Value::getIcirsCrmtoiciaddressid)
                                    .toList();
                            for (String id : idValStrAdd) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_BOARD_COMMITTEES:
                        Boolean boardCommitteesConversion = convertObj(response, IcirsCrmtoiciboardcommittees.class, BoardCommitteeStagingEntity.class, objectMapper, conversionService, boardCommitteeStagingEntityRepository);
                        if (boardCommitteesConversion) {
                            IcirsCrmtoiciboardcommittees icirsCrmtoiciboardcommittees = objectMapper.readValue(response.getBody(), IcirsCrmtoiciboardcommittees.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEES.Value> boardCommittees = icirsCrmtoiciboardcommittees.getValue();
                            List<String> idValStr = boardCommittees.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEES.Value::getIcirsCrmtoiciboardcommitteeid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_COMMIT_REPS:
                        Boolean committeerepConversion = convertObj(response, IcirsCrmtoicicommitreps.class, CommitrepStagingEntity.class, objectMapper, conversionService, commitrepStagingEntityRepository);
                        if (committeerepConversion) {
                            IcirsCrmtoicicommitreps icirsCrmtoicicommitreps = objectMapper.readValue(response.getBody(), IcirsCrmtoicicommitreps.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITREPS.Value> commitRepsVals = icirsCrmtoicicommitreps.getValue();

                            List<String> idValString = commitRepsVals.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITREPS.Value::getIcirsCrmtoicicommitrepid)
                                    .toList();
                            for (String id : idValString) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                    break;
                    case AZURE_CRM_ICI_BOARD_COMMITTEES_REP:
                        Boolean boardcommitteerepConversion = convertObj(response, IcirsCrmtoiciboardcommitteereps.class, BoardCommitrepStagingEntity.class, objectMapper, conversionService, boardCommitrepStagingEntityRepository);
                        if (boardcommitteerepConversion) {
                            IcirsCrmtoiciboardcommitteereps icirsCrmtoiciboardcommitteereps = objectMapper.readValue(response.getBody(), IcirsCrmtoiciboardcommitteereps.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEEREPS.Value> boardCommitteeReps = icirsCrmtoiciboardcommitteereps.getValue();

                            List<String> idValStr = boardCommitteeReps.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIBOARDCOMMITTEEREPS.Value::getIcirsCrmtoiciboardcommitteerepid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_COMMITTEES:
                        Boolean committeeConversion = convertObj(response, IcirsCrmtoicicommittees.class, CommitteeStagingEntity.class, objectMapper, conversionService, committeeStagingEntityRepository);
                        if(committeeConversion){
                            IcirsCrmtoicicommittees icirsCrmtoicicommittees = objectMapper.readValue(response.getBody(), IcirsCrmtoicicommittees.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEES.Value> committees = icirsCrmtoicicommittees.getValue();
                            List<String> idValStr = committees.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEES.Value::getIcirsCrmtoicicommitteeid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_COMPANIES:
                        Boolean companyConversion = convertObj(response, IcirsCrmtoicicompanieses.class, CompanyStagingEntity.class, objectMapper, conversionService, companyStagingRepo);
                        if(companyConversion){
                            IcirsCrmtoicicompanieses companies = objectMapper.readValue(response.getBody(), IcirsCrmtoicicompanieses.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANIESES.Value> companiesValue = companies.getValue();
                            List<String> idValStr = companiesValue.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANIESES.Value::getIcirsCrmtoicicompaniesid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }

                        }
                        break;
                    case AZURE_CRM_ICI_COMPANY_COMMUNICATIONS:
                        Boolean companyCommunicationConversion = convertObj(response, IcirsCrmtoicicompanycommunications.class, CompanyCommunicationStagingEntity.class, objectMapper, conversionService, companyCommunicationStagingEntityRepository);
                        if (companyCommunicationConversion) {
                            IcirsCrmtoicicompanycommunications icirscrmicicommunications = objectMapper.readValue(response.getBody(), IcirsCrmtoicicompanycommunications.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYCOMMUNICATIONS.Value> icirsCrmtoicicompanycommunications = icirscrmicicommunications.getValue();
                            List<String> idValStr = icirsCrmtoicicompanycommunications.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYCOMMUNICATIONS.Value::getIcirsCrmtoicicompanycommunicationid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;

                    case AZURE_CRM_ICI_COMMITTEE_MEETINGS:
                        Boolean committeeMeetingsConvert = convertObj(response, IcirsCrmtoicicommitteemeetingses.class, CommitteeMeetingsStagingEntity.class, objectMapper, conversionService, committeeMeetingsStagingEntityRepository);
                        if (committeeMeetingsConvert) {
                            IcirsCrmtoicicommitteemeetingses icirsCrmtoicicommitteemeetingses = objectMapper.readValue(response.getBody(), IcirsCrmtoicicommitteemeetingses.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEEMEETINGSES.Value> icirsCrmtoicicompanycommunications = icirsCrmtoicicommitteemeetingses.getValue();
                            List<String> idValStr = icirsCrmtoicicompanycommunications.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEEMEETINGSES.Value::getIcirsCrmtoicicommitteemeetingsid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI__COMMITTEE_MEETINGS_ATTENDEES:
                        Boolean committeeMeetingAttendees = convertObj(response, IcirsCrmtoicicommitteeMeetingAttendeeses.class, CommitteeMeetingAttendeesStagingEntity.class, objectMapper, conversionService, committeeMeetingsAttendeesStagingEntityRepository);
                        if (committeeMeetingAttendees) {
                            IcirsCrmtoicicommitteeMeetingAttendeeses icirsCrmtoicicommitteeMeetingAttendeeses = objectMapper.readValue(response.getBody(), IcirsCrmtoicicommitteeMeetingAttendeeses.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEE_MEETING_ATTENDEESES.Value> icirsCrmtoicicommitteemeetingattendees = icirsCrmtoicicommitteeMeetingAttendeeses.getValue();
                            List<String> idValStr = icirsCrmtoicicommitteemeetingattendees.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMMITTEE_MEETING_ATTENDEESES.Value::getIcirsCrmtoicicommitteeMeetingAttendeesid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_COMPANY_RELATIONS:
                        Boolean companyRelationsConversion = convertObj(response, IcirsCrmtoicicompanyrelations.class, CompanyRelationStagingEntity.class, objectMapper, conversionService, companyRelationStagingEntityRepository);
                        if (companyRelationsConversion) {
                            IcirsCrmtoicicompanyrelations icirsCrmtoicicompanyrelations = objectMapper.readValue(response.getBody(), IcirsCrmtoicicompanyrelations.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYRELATIONS.Value> companyRelationsValues = icirsCrmtoicicompanyrelations.getValue();
                            List<String> idValString = companyRelationsValues.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICOMPANYRELATIONS.Value::getIcirsCrmtoicicompanyrelationid)
                                    .toList();
                            for (String id : idValString) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;

                    case AZURE_CRM_ICI_ESPPS:
                        Boolean esppsConversion = convertObj(response, IcirsCrmtoiciespps.class, ESppStagingEntity.class, objectMapper, conversionService, eSppStagingEntityRepository);
                        if (esppsConversion) {
                            IcirsCrmtoiciespps icirsCrmtoiciespps = objectMapper.readValue(response.getBody(), IcirsCrmtoiciespps.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIESPPS.Value> esppValues = icirsCrmtoiciespps.getValue();
                            List<String> idValStr = esppValues.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIESPPS.Value::getIcirsCrmtoiciesppid)
                                    .toList();
                            for (String id : idValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    //TODO this one case is special since saveAll was causing issues, so individual saves here are a must
                    case AZURE_CRM_ICI_MAILING_RECIPS:
                            IcirsCrmtoicimailingrecips mailrecips = objectMapper.readValue(response.getBody(), IcirsCrmtoicimailingrecips.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGRECIPS.Value> mailRecipVals = mailrecips.getValue();
                            List<MailrecipStagingEntityKey> stagingIds = new ArrayList<>();
                            for(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGRECIPS.Value mailRecipVal: mailRecipVals) {
                                MailrecipStagingEntityKey stagingEntityKey = new MailrecipStagingEntityKey();
                                stagingEntityKey.setCode(mailRecipVal.getIcirsCode());
                                stagingEntityKey.setPnum(mailRecipVal.getIcirsPnum());
                                if(!stagingIds.contains(stagingEntityKey)) {
                                    stagingIds.add(stagingEntityKey);
                                }
                            }
                            List<MailrecipStagingEntity> mailrecipStagingEntities = new ArrayList<>();
                            for(MailrecipStagingEntityKey stagingEntityKey : stagingIds) {
                                 List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGRECIPS.Value> mailreips = mailRecipVals.stream()
                                        .filter(value-> value.getIcirsCode().equals(stagingEntityKey.getCode()) && value.getIcirsPnum().equals(stagingEntityKey.getPnum()))
                                        .toList();
                                 for(int i =0; i < mailreips.size(); i++) {
                                     MailrecipStagingEntity stagingEntity = conversionService.convert(mailreips.get(i), MailrecipStagingEntity.class);
                                     stagingEntity.setRevision(i);
                                     mailrecipStagingEntities.add(stagingEntity);
                                 }
                            }
                            for(int i=0; i < mailrecipStagingEntities.size(); i++) {
                                mailrecipStagingRepo.save(mailrecipStagingEntities.get(i));
                            }

                            List<String> idValStr = mailRecipVals.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGRECIPS.Value::getIcirsCrmtoicimailingrecipid)
                                    .toList();
                            for(String id: idValStr){
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        break;
                    case AZURE_CRM_ICI_MAILINGS:
                        Boolean mailingsConversion = convertObj(response, IcirsCrmtoicimailings.class, MailingStagingEntity.class, objectMapper, conversionService, mailingStagingEntityRepository);
                        if (mailingsConversion) {
                            IcirsCrmtoicimailings icirsCrmtoicimailings = objectMapper.readValue(response.getBody(), IcirsCrmtoicimailings.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGS.Value> mailings = icirsCrmtoicimailings.getValue();
                            List<String> idStrings = mailings.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMAILINGS.Value::getIcirsCrmtoicimailingid)
                                    .toList();
                            for (String id : idStrings) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_MEMBERSHIP:
                        Boolean membershipConversion = convertObj(response, IcirsCrmtoicimemberships.class, CurrentMembershipStagingEntity.class, objectMapper, conversionService, currentMembershipStagingEntityRepository);
                        if (membershipConversion) {
                            IcirsCrmtoicimemberships icirsCrmtoicimemberships = objectMapper.readValue(response.getBody(), IcirsCrmtoicimemberships.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMEMBERSHIPS.Value> memberships = icirsCrmtoicimemberships.getValue();
                            List<String> membershipIdValStr = memberships.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIMEMBERSHIPS.Value::getIcirsCrmtoicimembershipid)
                                    .toList();
                            for (String id : membershipIdValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_PERSON_COMMUNICATIONS:
                        Boolean personCommunicationsConversion = convertObj(response, IcirsCrmtoicipersoncommunications.class, PersonCommunicationStagingEntity.class, objectMapper, conversionService, personCommunicationStagingEntityRepository);
                        if (personCommunicationsConversion) {
                            IcirsCrmtoicipersoncommunications icirsCrmtoicipersoncommunications = objectMapper.readValue(response.getBody(), IcirsCrmtoicipersoncommunications.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIPERSONCOMMUNICATIONS.Value> personCommunications = icirsCrmtoicipersoncommunications.getValue();
                            List<String> personCommunicationIdValStr = personCommunications.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIPERSONCOMMUNICATIONS.Value::getIcirsCrmtoicipersoncommunicationid)
                                    .toList();
                            for (String id : personCommunicationIdValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                    case AZURE_CRM_ICI_SALUTATIONS:
                        Boolean salutationConversion = convertObj(response, IcirsCrmtoicisalutations.class, SalutationStagingEntity.class, objectMapper, conversionService, salutationStagingEntityRepository);
                        if (salutationConversion) {
                            IcirsCrmtoicisalutations icirsCrmtoicisalutations = objectMapper.readValue(response.getBody(), IcirsCrmtoicisalutations.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICISALUTATIONS.Value> salutations = icirsCrmtoicisalutations.getValue();
                            List<String> salutationIdValStr = salutations.stream()
                                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICISALUTATIONS.Value::getIcirsCrmtoicisalutationid)
                                    .toList();
                            for (String id : salutationIdValStr) {
                                updateAzureFlag(tableName, callUrl, id, headers);
                            }
                        }
                        break;
                     //TODO the below is for compcontacts in ici, it's just called contact purposes in CRM -- this one has some weird lookup stuff going on so it has it's own special mapping method
                    case AZURE_CRM_ICI_CONTACT_PURPOSES:
                        Boolean compContact = convertObj(response, IcirsCrmtoicicontactpurposes.class, CompcontactStagingEntity.class, objectMapper, conversionService, CompcontactStagingEntityRepository);
                        if (compContact) {
                            IcirsCrmtoicicontactpurposes icirsCrmtoicicontactpurposes = objectMapper.readValue(response.getBody(), IcirsCrmtoicicontactpurposes.class);
                            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.Value> contactPurposes = icirsCrmtoicicontactpurposes.getValue();
                            List<CompContactStagingId> compContactStagingIds = new ArrayList<>();
                            for(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.Value contactPurpose: contactPurposes) {
                                CompContactStagingId compContactStagingId = new CompContactStagingId();
                                compContactStagingId.setCnum(contactPurpose.getIcirsCnum());
                                compContactStagingId.setPnum(contactPurpose.getIcirsPnum());
                                compContactStagingId.setPurpose(contactPurpose.getIcirsPurpose().shortValue());
                                if(!compContactStagingIds.contains(compContactStagingId)) {
                                    compContactStagingIds.add(compContactStagingId);
                                }
                            }
                            List<CompcontactStagingEntity> compContactStagingEntities = new ArrayList<>();
                            for(CompContactStagingId compContactStagingId: compContactStagingIds) {
                                List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.Value> filteredContactPurposes = contactPurposes.stream()
                                        .filter(value -> compContactStagingId.getCnum() == value.getIcirsCnum() &&
                                                 compContactStagingId.getPnum() == value.getIcirsPnum() &&
                                                compContactStagingId.getPurpose() == value.getIcirsPurpose()
                                                )
                                        .toList();
                                List<CompcontactStagingEntity> existingStagingEntites = compcontactStagingEntityRepository.findExistingCompContacts(compContactStagingId);

                                for(int i =0; i < filteredContactPurposes.size(); i++) {
                                    CompcontactStagingEntity newCompContact = conversionService.convert(filteredContactPurposes.get(i), CompcontactStagingEntity.class);
                                    newCompContact.setRevision(i + existingStagingEntites.size());
                                    compContactStagingEntities.add(newCompContact);
                                }
                            }
                            List<CompcontactStagingEntity> savedCompContacts = compcontactStagingEntityRepository.saveAll(compContactStagingEntities);
                            if(savedCompContacts != null && !savedCompContacts.isEmpty()) {
                                List<String> compContactsIdValStr = new ArrayList<>();
                                for(CompcontactStagingEntity savedCompContact: savedCompContacts) {
                                    List<String> compcontactsSaves = contactPurposes.stream()
                                            .filter(value -> savedCompContact.getCnum() == value.getIcirsCnum() && savedCompContact.getPnum() == value.getIcirsPnum() && savedCompContact.getPurpose() == value.getIcirsPurpose())
                                            .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICICONTACTPURPOSES.Value::getIcirsCrmtoicicontactpurposeid)
                                            .toList();
                                    compContactsIdValStr.addAll(compcontactsSaves);
                                }
                                for (String id : compContactsIdValStr) {
                                    updateAzureFlag(tableName, callUrl, id, headers);
                                }
                            }
                        }
                        break;
                }

                headers.add("Content-Type", "application/json");

            }

        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static java.sql.Date convertDate(String timestamp) {
        timestamp = timestamp == null || timestamp.isEmpty() || timestamp.isBlank() ||timestamp.contains("@")?"":timestamp.substring(0,timestamp.length()-1);
        if(timestamp.length()>1){
            timestamp = timestamp.replaceAll("T", " ");
        }
        timestamp = timestamp.split(" ")[0];
        return timestamp.isEmpty() ?null: Date.valueOf(timestamp);
    }

    private void updateAzureFlag(String tableName, String callUrl, String idValues, HttpHeaders headers) {
        try {
            URL updateUrl = new URL(callUrl);
            String updateUrlStr =  updateUrl.getProtocol() + "://" + updateUrl.getHost() + "/api/data/v9.2/" + tableName + "(" + idValues  + ")";
            String payload = "{\"icirs_ici_push\": true }";
            HttpHeaders currentHeaders = new HttpHeaders();
            currentHeaders.add("Content-Type", "application/json");
            currentHeaders.add("Authorization", headers.get("Authorization").get(0));
            HttpEntity<String> updatedEntity= new HttpEntity<>(payload,currentHeaders);
            RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
            ResponseEntity response = restTemplate.exchange(updateUrlStr, HttpMethod.PATCH,updatedEntity, Void.class);
            log.info(idValues);
            if(!response.getStatusCode().is2xxSuccessful()){
                log.error(response.getStatusCode() + "\n" + response.getBody() + "\t" + response.getHeaders().toString());
            }

        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public void blastEntity(String tokenCredentialAuthProvider, String tableName) throws MalformedURLException, JsonProcessingException {
        Integer recordCount = 5000;
        while(recordCount > 0) {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + tokenCredentialAuthProvider);
            HttpEntity<String> entity = new HttpEntity<>(headers);
            ObjectMapper objectMapper = new ObjectMapper();
            //TODO pull in all the relevant records here, and then serialize them
            URL scopeUrl = new URL(SCOPE);
            String callUrl = scopeUrl.getProtocol() + "://" + scopeUrl.getHost() + "/api/data/v9.2/" + tableName + "?$filter=icirs_ici_push eq false&$orderby=modifiedon asc";
            ResponseEntity<String> response = restTemplate.exchange(callUrl, HttpMethod.GET, entity, String.class);

            IcirsCrmtoiciaddresses icirsCrmtoiciaddresses = objectMapper.readValue(response.getBody(), IcirsCrmtoiciaddresses.class);
            List<org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.Value> addresses = icirsCrmtoiciaddresses.getValue();

            List<String> idValStrAdd = addresses.stream()
                    .map(org.ici.services.domain.dto.crm.ICIRS_CRMTOICIADDRESSES.Value::getIcirsCrmtoiciaddressid)
                    .toList();

            recordCount = idValStrAdd.size();
            //TODO serialized data into a call and fire them off quickly across topic w/10 partitions
            List<AzureRequestTopicObject> requestTopicObjects = new ArrayList<>();
            for (String idValue : idValStrAdd) {
                URL updateUrl = new URL(callUrl);
                String updateUrlStr = updateUrl.getProtocol() + "://" + updateUrl.getHost() + "/api/data/v9.2/" + tableName + "(" + idValue + ")";
                String payload = "{\"icirs_ici_push\": true }";
                HttpHeaders currentHeaders = new HttpHeaders();
                currentHeaders.add("Content-Type", "application/json");
                currentHeaders.add("Authorization", headers.get("Authorization").get(0));
                HttpEntity<String> updatedEntity = new HttpEntity<>(payload, currentHeaders);

                AzureRequestTopicObject azureRequestTopicObject = new AzureRequestTopicObject(updateUrlStr, updatedEntity, idValue);
                requestTopicObjects.add(azureRequestTopicObject);
            }
            for (AzureRequestTopicObject azureRequestTopicObject : requestTopicObjects) {
                CompletableFuture<SendResult<String, AzureRequestTopicObject>> future = kafkaTemplate.send("fireAzureRequests", azureRequestTopicObject);
                future
                        .thenApply(result -> {
                            AzureRequestTopicObject azrto = result.getProducerRecord().value();
                            updateAzureFlag("icirs_crmtoiciaddresses", azrto.getUrl(), azrto.getIdVal(), headers);
                            log.info("Successfully called for id = " + azrto.getIdVal());
                            return null;
                        })
                        .exceptionally(err -> {
                            log.error(err.getMessage());
                            return null;
                        });

            }
        }
    }
}
