package org.ici.members.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import com.thoughtworks.xstream.XStream;
import org.dozer.DozerBeanMapper;
import org.ici.members.MemberUtil;
import org.ici.members.db.domain.MailingCode;
import org.ici.members.db.domain.membership.*;
import org.ici.members.db.mappers.UserMapper;
import org.ici.members.db.mappers.membership.CompanyMapper;
import org.ici.services.domain.dto.login.OAuthDebugResponse;
import org.ici.services.domain.dto.login.Token;
import org.ici.services.domain.users.PersonRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MemberService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    private CompanyMapper companyMapper;

    private Logger logger = LoggerFactory.getLogger(MemberService.class);
    private static final int STATUS_ACTIVE  = 1;
    private static final int STATUS_DISABLE = 0;
    @Value("${auth.url}")
    private String access_token_url;

    @Value("${auth.grant_type}")
    private String grant_type;

    @Value("${auth.client_id}")
    private String client_id;

    @Value("${auth.client_secret}")
    private String client_secret;

    @Value("${auth.debug_url}")
    private String url;

    private ObjectMapper objectMapper = new ObjectMapper();

    public Token getToken(){
        ResponseEntity<String> response = null;
        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String,String> parametersMap = new LinkedMultiValueMap<>();
        parametersMap.add("grant_type",grant_type);
        parametersMap.add("client_id",client_id);
        parametersMap.add("client_secret",client_secret);

        Token tokenVal = restTemplate.postForObject(access_token_url,parametersMap, Token.class);
        ResponseEntity<OAuthDebugResponse> debugResponseResponseEntity = validateToken(tokenVal);

        if(debugResponseResponseEntity.getStatusCode().is4xxClientError() || debugResponseResponseEntity.getStatusCode().is5xxServerError() ||
                debugResponseResponseEntity.getStatusCode().isError()){

            throw new RuntimeException("The Token is invalid with status: " + debugResponseResponseEntity.getStatusCodeValue());

        }

        return tokenVal;
    }




    public Address applyPatchToAddresses(JsonMergePatch patch, Address personAddress) {
        try {
//            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
            JsonNode patchedNode = patch.apply(objectMapper.convertValue(personAddress, JsonNode.class));
            return  objectMapper.treeToValue(patchedNode, Address.class);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Person applyPatchToPerson(JsonMergePatch patch, Person personRecord) {
        try {
            JsonNode patched = patch.apply(objectMapper.convertValue(personRecord, JsonNode.class));
            return objectMapper.treeToValue(patched, Person.class);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResponseEntity<OAuthDebugResponse> validateToken(Token token){
        RestTemplate restTemplate = new RestTemplate();

        // Use the access token for authentication
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Authorization", "Bearer " + token.getAccess_token());
        HttpEntity<String> entity = new HttpEntity<>(headers1);

        return restTemplate.exchange(url, HttpMethod.GET,entity,OAuthDebugResponse.class);
    }

    public List<PersonRequest> getPersonFromRequest(List<PersonRequest> requests) {
        List<Integer> pnumRequests = requests.stream()
                .filter(request -> request.getPnum() != null)
                .map(PersonRequest::getPnum)
                .collect(Collectors.toList());

        List<PersonRequest> requestsWithNoPnum = requests.stream()
                .filter(request -> request.getPnum() == null)
                .collect(Collectors.toList());

        String whereClause = buildWhereClause(requestsWithNoPnum);
        if(whereClause.length() == 0 && pnumRequests.size() == 0){
            return new ArrayList<>();
        } else{
            List<Integer> pnums = new ArrayList<>();
            if(whereClause.length() > 0) {
                pnums.addAll(userMapper.getPersonByRequest(whereClause, "membership..person", "pnum"));
            }
           pnums.addAll(pnumRequests);
           List<PersonRequest> finalPeopleList = new ArrayList<>();
           for(Integer pnum: pnums){
               Person person = userMapper.getPersonByPnum(pnum);
               PersonRequest personRequest = mapPersonToPersonRequest(person.getPnum(), person, true);
               if(personRequest != null) {
                   finalPeopleList.add(personRequest);
               }
           }
           return finalPeopleList;
        }
    }

    public List<PersonRequest> getPersonRecords(List<String> emails) {
        List<PersonRequest> personRequests = new ArrayList<>();
        for(String email: emails){
            Person person = userMapper.getPersonByEmail(email);
            PersonRequest userVal = new PersonRequest();
            if(person == null) {
                logger.warn("User does not exist: " + email);
                userVal.setEmail(email);
                personRequests.add(userVal);
            }else {
                userVal = mapPersonToPersonRequest(person.getPnum(), person);
            }
            personRequests.add(userVal);
        }

        return personRequests;
    }


    private void printObject(Object val) {
        XStream xs = new XStream();
        System.out.println(xs.toXML(val));
    }

    public PersonRequest mapPersonToPersonRequest(Integer pnum, Person person, boolean allowNulls){
        if(allowNulls){
            List<Address> addresses = userMapper.getAddressByPnum(pnum);
            List<Company> companies = userMapper.getCompanyByPnum(pnum);
            Company company = (companies!=null && companies.size()>0) ? companies.get(0) : null;
            Address address = (addresses!=null && addresses.size()>0) ? addresses.get(0) : null;

            // Create Base Record
            PersonRequest userVal = new PersonRequest();
            userVal.setStatus(STATUS_ACTIVE);
            userVal.setPnum(pnum);
            userVal.setType(getUserType(pnum));
            userVal.setTypeStr(MemberUtil.TYPE_STRINGS[userVal.getType()]);
            userVal.setPrefix(person.getPrefix());
            userVal.setFname(person.getFname());
            userVal.setMname(person.getMiddle());
            userVal.setLname(person.getLname());
            userVal.setTitle(person.getTitle());
            userVal.setPhone(person.getPhone());
            userVal.setMobile(person.getCellPhone());
            userVal.setEmail(person.getEmail());
            if (company == null) {
                userVal.setCompanyName("");
                userVal.setCnum(0);
            }
            else {
                userVal.setCompanyName(company.getName());
                userVal.setCnum(company.getCnum());
            }

            // Set Address
            if (address != null) {
                userVal.setStreet1(address.getStreet1());
                userVal.setStreet2(address.getStreet2());
                userVal.setCity(address.getCity());
                userVal.setState(address.getState());
                userVal.setZip(address.getZip());
                userVal.setCountry(getCountryAcronym(address.getCountry()));
            }

            // Set Committees
            List<Commitrep> commitrep = userMapper.getCommittesByPnum(pnum);
            if (commitrep != null) {
                List<Integer> committees = new ArrayList<>();
                List<Integer> primary = new ArrayList<>();
                for (Commitrep c : commitrep) {
                    committees.add(c.getCode());

                    if (c.getActive() != null && c.getActive().equalsIgnoreCase("y")) {
                        primary.add(c.getCode());
                    }
                }

                userVal.setCommittees(committees.toArray(new Integer[0]));
                userVal.setCommitPrimary(convertIntegers(primary));
            }

            // Set Mailings
            List<MailingCode> mailings = userMapper.getMailingsByPnum(pnum);
            if (mailings != null) {
                List<Integer> mailcodes = new ArrayList<>();
                for (MailingCode c : mailings)
                    mailcodes.add(c.getCode());

                userVal.setMailings(mailcodes.toArray(new Integer[0]));
            }

            // Enterprise Subscribers need their companies mailings to determine memo access
            if (userVal.getType() == MemberUtil.TYPE_ENTERPRISE_SUBSCRIBER) {
                List<MailingCode> esCodes = userMapper.getEnterpriseSubscriberMailings(pnum);
                List<Integer> mailcodes = Arrays.asList(userVal.getMailings());
                List<Integer> nonStaticMailCodes = new ArrayList<>();
                nonStaticMailCodes.addAll(mailcodes);

                for (MailingCode c : esCodes) {
                    if(!mailcodes.contains(Integer.valueOf(c.getCode()))) {
                        nonStaticMailCodes.add(Integer.valueOf(c.getCode()));
                    }
                }

                userVal.setMailings(mailcodes.toArray(new Integer[0]));
            }
            userVal.setIquest(false);
            userVal.setConfidential(false);
            userVal.setQuerytool(false);
            userVal.setDaily(false);

            Person verifyPerson = userMapper.getHasVerifyByPnum(pnum);
            boolean hasVerify = verifyPerson != null;
            userVal.setVerify(hasVerify);

            switch (userVal.getType()) {
                case MemberUtil.TYPE_EMPLOYEE:
                    userVal.setIquest(true);
                    userVal.setConfidential(true);
                    userVal.setQuerytool(true);
                    userVal.setDaily(checkMailings(mailings, MemberUtil.MAILING_DAILY));
                    break;
                case MemberUtil.TYPE_MEMBER:
                case MemberUtil.TYPE_INDEPENDENT_DIRECTOR:
                    userVal.setConfidential(checkMailings(mailings, MemberUtil.MAILING_CONFIDENTIAL));
                    userVal.setQuerytool(true);
                    break;
                default:
                    userVal.setQuerytool(checkMailings(mailings, MemberUtil.MAILING_QUERYTOOL));
                    break;
            }

            // Set status to ZERO if the user is inactive
//        if (userInactive())
//            userVal.setStatus(STATUS_DISABLE);

            logger.info("Retrieved Record: " + userVal.toString());
            return userVal;
        }
        else{
            return mapPersonToPersonRequest(pnum, person);
        }
    }

    public PersonRequest mapPersonToPersonRequest(Integer pnum, Person person){
        List<Address> addresses = userMapper.getAddressByPnum(pnum);
        List<Company> companies = userMapper.getCompanyByPnum(pnum);
        Company company = (companies!=null && companies.size()>0) ? companies.get(0) : null;
        Address address = (addresses!=null && addresses.size()>0) ? addresses.get(0) : null;

        // Create Base Record
        PersonRequest userVal = new PersonRequest();
        userVal.setStatus(STATUS_ACTIVE);
        userVal.setPnum(pnum);
        userVal.setType(getUserType(pnum));
        userVal.setTypeStr(MemberUtil.TYPE_STRINGS[userVal.getType()]);
        userVal.setPrefix(person.getPrefix());
        userVal.setFname(person.getFname());
        userVal.setMname(person.getMiddle());
        userVal.setLname(person.getLname());
        userVal.setTitle(person.getTitle());
        userVal.setPhone(person.getPhone());
        userVal.setMobile(person.getCellPhone());
        userVal.setEmail(person.getEmail());
        if (company == null) {
            userVal.setCompanyName("");
            userVal.setCnum(0);
        }
        else {
            userVal.setCompanyName(company.getName());
            userVal.setCnum(company.getCnum());
        }

        // Set Address
        if (address != null) {
            userVal.setStreet1(address.getStreet1());
            userVal.setStreet2(address.getStreet2());
            userVal.setCity(address.getCity());
            userVal.setState(address.getState());
            userVal.setZip(address.getZip());
            userVal.setCountry(getCountryAcronym(address.getCountry()));
        }

        // Set Committees
        List<Commitrep> commitrep = userMapper.getCommittesByPnum(pnum);
        if (commitrep != null) {
            List<Integer> committees = new ArrayList<>();
            List<Integer> primary = new ArrayList<>();
            for (Commitrep c : commitrep) {
                committees.add(c.getCode());

                if (c.getActive() != null && c.getActive().equalsIgnoreCase("y")) {
                    primary.add(c.getCode());
                }
            }

            userVal.setCommittees(committees.toArray(new Integer[0]));
            userVal.setCommitPrimary(convertIntegers(primary));
        }

        // Set Mailings
        List<MailingCode> mailings = userMapper.getMailingsByPnum(pnum);
        if (mailings != null) {
            List<Integer> mailcodes = new ArrayList<>();
            for (MailingCode c : mailings)
                mailcodes.add(c.getCode());

            userVal.setMailings(mailcodes.toArray(new Integer[0]));
        }

        // Enterprise Subscribers need their companies mailings to determine memo access
        if (userVal.getType() == MemberUtil.TYPE_ENTERPRISE_SUBSCRIBER) {
            List<MailingCode> esCodes = userMapper.getEnterpriseSubscriberMailings(pnum);
            List<Integer> mailcodes = Arrays.asList(userVal.getMailings());
            List<Integer> nonStaticMailCodes = new ArrayList<>();
            nonStaticMailCodes.addAll(mailcodes);

            for (MailingCode c : esCodes) {
                if(!mailcodes.contains(Integer.valueOf(c.getCode()))) {
                    nonStaticMailCodes.add(Integer.valueOf(c.getCode()));
                }
            }

            userVal.setMailings(mailcodes.toArray(new Integer[0]));
        }



        // setup Boolean flags
        userVal.setIquest(false);
        userVal.setConfidential(false);
        userVal.setQuerytool(false);
        userVal.setDaily(false);

        Person verifyPerson = userMapper.getHasVerifyByPnum(pnum);
        boolean hasVerify = verifyPerson != null;
        userVal.setVerify(hasVerify);

        switch (userVal.getType()) {
            case MemberUtil.TYPE_EMPLOYEE:
                userVal.setIquest(true);
                userVal.setConfidential(true);
                userVal.setQuerytool(true);
                userVal.setDaily(checkMailings(mailings, MemberUtil.MAILING_DAILY));
                break;
            case MemberUtil.TYPE_MEMBER:
            case MemberUtil.TYPE_INDEPENDENT_DIRECTOR:
                userVal.setConfidential(checkMailings(mailings, MemberUtil.MAILING_CONFIDENTIAL));
                userVal.setQuerytool(true);
                break;
            default:
                userVal.setQuerytool(checkMailings(mailings, MemberUtil.MAILING_QUERYTOOL));
                break;
        }

        // Set status to ZERO if the user is inactive
//        if (userInactive())
//            userVal.setStatus(STATUS_DISABLE);

        logger.info("Retrieved Record: " + userVal.toString());
        return userVal;
    }

    private boolean checkMailings(List<MailingCode> mailings, int mailing) {
        for (MailingCode code:mailings) {
            if (code.getCode() == mailing) {
                return true;
            }
        }
        return false;
    }

    private int getUserType(int pnum) {
        List<Person> iciMutual = userMapper.isIciMutualMember(pnum);
        Person p = userMapper.isEmployee(pnum);
        if (p != null || (iciMutual != null && iciMutual.size() > 0)) {
            return MemberUtil.TYPE_EMPLOYEE;
        }

        p = userMapper.isMember(pnum);
        if (p != null) {
            return MemberUtil.TYPE_MEMBER;
        }

        p = userMapper.isIndependentDirector(pnum);
        if (p != null) {
            return MemberUtil.TYPE_INDEPENDENT_DIRECTOR;
        }

        p = userMapper.isEnterpriseSubscriber(pnum);
        if (p != null) {
            return MemberUtil.TYPE_ENTERPRISE_SUBSCRIBER;
        }

        List<Mailrecip> statsMailings = userMapper.getStatisticalSubscribtions(pnum);
        if (statsMailings != null && statsMailings.size() > 0) {
            return MemberUtil.TYPE_STATISTICAL_SUBSCRIBER;
        }

        p = userMapper.isAssociateMember(pnum);
        if (p != null) {
            return MemberUtil.TYPE_ASSOCIATE_MEMBER;
        }

//        p = userMapper.isDataRepublisher(pnum);
//        if (p != null) {
//            return MemberUtil.TYPE_DATA_REPUBLISHER;
//        }

        List<Mailrecip> events = userMapper.getPersonEvents(pnum);
        if (events != null && events.size() > 0) {
            return MemberUtil.TYPE_EVENT_ATTENDEE;
        }

        return MemberUtil.TYPE_UNKNOWN;
    }

    private String getCountryAcronym(String country) {
        if(country == null || country.length() == 0){
            return "US";
        }
        else {
            CountryListDrupalLkup countryListLkup = userMapper.getCountryDrupalInfo(country);
            if (countryListLkup != null)
                return countryListLkup.getAccronym();
        }
        return "US";
    }

    public String buildCompanyProfile(Integer cnum) {
        Company company = userMapper.getCompanyByCnum(cnum);
        Person primary = userMapper.getCompanyPrimaryContact(cnum);
        Person ceo = userMapper.getCompanyCEO(cnum);
        List<Person> boardMembers = userMapper.getCompanyBoardMembers(cnum);
        List<Person> idcCouncil = userMapper.getCompanyIdcCouncil(cnum);
        List<Person> atlanticCouncil = userMapper.getCompanyAtlanticCouncil(cnum);
        List<Person> pacificCouncil = userMapper.getCompanyPacificCouncil(cnum);
        List<CommitteePerson> committeeMembers = userMapper.getCompanyCommitteeMembers(cnum);


        // TODO: Remove duplicates from committeeMembers if they already exist in atlanticCouncil or pacificCouncil


        String retval = "<div>\n";

        if (company != null) {
            retval += "<h1>" + company.getName() + "</h1>\n";
        }

        if (primary != null) {
            retval += "<h2>Primary Contact</h2>\n";
            retval += "<p>" + primary.getFname() + " " + primary.getLname() + "</p>\n";
        }

        if (ceo != null) {
            retval += "<h2>CEO</h2>\n";
            retval += "<p>" + ceo.getFname() + " " + ceo.getLname() + "</p>\n";
        }

        if (boardMembers != null && boardMembers.size() > 0) {
            retval += "<h2>Board Members</h2>\n";
            for (Person p : boardMembers)
                retval += "<p>" + p.getFname() + " " + p.getLname() + "</p>\n";
        }

        if (idcCouncil != null && idcCouncil.size() > 0) {
            retval += "<h2>IDC Council Members</h2>\n";
            for (Person p : idcCouncil)
                retval += "<p>" + p.getFname() + " " + p.getLname() + "</p>\n";
        }

        if (atlanticCouncil != null && atlanticCouncil.size() > 0) {
            retval += "<h2>Atlantic Council Members</h2>\n";
            for (Person p : atlanticCouncil)
                retval += "<p>" + p.getFname() + " " + p.getLname() + "</p>\n";
        }

        if (pacificCouncil != null && pacificCouncil.size() > 0) {
            retval += "<h2>Pacific Council Members</h2>\n";
            for (Person p : pacificCouncil)
                retval += "<p>" + p.getFname() + " " + p.getLname() + "</p>\n";
        }

        if (committeeMembers != null && boardMembers.size() > 0) {
            retval += "<h2>Committee Members</h2>\n";
            String lastCommittee = "";
            for (CommitteePerson p : committeeMembers) {
                if (!lastCommittee.equalsIgnoreCase(p.getCommittee())) {
                    retval += "<h3>" + p.getCommittee() + "</h3>\n";
                    lastCommittee = p.getCommittee();
                }
                retval += "<p>" + p.getFname() + " " + p.getLname() + "</p>\n";
            }
        }

        return retval + "</div>";
    }




    public static Integer[] convertIntegers(List<Integer> integers) {
        Integer[] ret = new Integer[integers.size()];
        for (int i=0; i < ret.length; i++) {
            ret[i] = integers.get(i).intValue();
        }
        return ret;
    }

    public PersonRequest getPersonRecord(int pnum) {
        Person person = userMapper.getPersonByPnum(pnum);
        if (person == null) {
            logger.warn("Invalid User: " + pnum);
            return null;
        }

        List<Address> addresses = userMapper.getAddressByPnum(pnum);
        List<Company> companies = userMapper.getCompanyByPnum(pnum);
        Company company = (companies!=null && companies.size()>0) ? companies.get(0) : null;
        Address address = (addresses!=null && addresses.size()>0) ? addresses.get(0) : null;

        // Create Base Record
        PersonRequest userVal = new PersonRequest();
        if (person.getStatus().equalsIgnoreCase("a"))
            userVal.setStatus(STATUS_ACTIVE);
        else
            userVal.setStatus(STATUS_DISABLE);

        userVal.setPnum(pnum);
        userVal.setType(getUserType(pnum));
        userVal.setTypeStr(MemberUtil.TYPE_STRINGS[userVal.getType()]);
        userVal.setFname(person.getFname());
        userVal.setMname(person.getMiddle());
        userVal.setLname(person.getLname());
        userVal.setTitle(person.getTitle());
        userVal.setPhone(person.getPhone());
        userVal.setMobile(person.getCellPhone());
        userVal.setEmail(person.getEmail());
        userVal.setPrefix(person.getPrefix());
        if (company == null) {
            userVal.setCompanyName("");
            userVal.setCnum(0);
        }
        else {
            userVal.setCompanyName(company.getName());
            userVal.setCnum(company.getCnum());
        }

        // Set Address
        if (address != null) {
            userVal.setStreet1(address.getStreet1());
            userVal.setStreet2(address.getStreet2());
            userVal.setCity(address.getCity());
            userVal.setState(address.getState());
            userVal.setZip(address.getZip());
            userVal.setCountry(getCountryAcronym(address.getCountry()));
        }

        // Set Committees
        List<Commitrep> commitrep = userMapper.getCommittesByPnum(pnum);
        if (commitrep != null) {
            List<Integer> committees = new ArrayList<>();
            List<Integer> primary = new ArrayList<>();
            for (Commitrep c : commitrep) {
                committees.add(c.getCode());

                if (c.getActive() != null && c.getActive().equalsIgnoreCase("y")) {
                    primary.add(c.getCode());
                }
            }

            userVal.setCommittees(committees.toArray(new Integer[0]));
            userVal.setCommitPrimary(convertIntegers(primary));
        }

        // Set Mailings
        List<MailingCode> mailings = userMapper.getMailingsByPnum(pnum);
        if (mailings != null) {
            List<Integer> mailcodes = new ArrayList<>();
            for (MailingCode c : mailings) {
                mailcodes.add(c.getCode());
            }

            userVal.setMailings(mailcodes.toArray(new Integer[0]));
        }

        // Enterprise Subscribers need their companies mailings to determine memo access
        if (userVal.getType() == MemberUtil.TYPE_ENTERPRISE_SUBSCRIBER) {
            List<MailingCode> esCodes = userMapper.getEnterpriseSubscriberMailings(pnum);

            List<Integer> mailcodes = new ArrayList<>();
            if (userVal.getMailings() != null && userVal.getMailings().length > 0) {
                for (Integer x : userVal.getMailings()) {
                    mailcodes.add(x);
                }
            }

            if (esCodes != null) {
                for (MailingCode c : esCodes) {
                    if (!mailcodes.contains(c.getCode())) {
                        mailcodes.add(c.getCode());
                    }
                }
            }

            userVal.setMailings(mailcodes.toArray(new Integer[0]));
        }

//        // Catch any random undefined user
//        if (userVal.getType() == MemberUtil.TYPE_UNKNOWN
//                && (userVal.getCommittees() == null || userVal.getCommittees().length == 0)
//                && (userVal.getMailings() == null || userVal.getMailings().length == 0)) {
//            logger.warn("Unknown User with no committees or mailings: " + pnum);
//            return null;
//        }

        // setup Boolean flags
        userVal.setIquest(false);
        userVal.setConfidential(false);
        userVal.setQuerytool(false);
        userVal.setDaily(false);

        Person verifyPerson = userMapper.getHasVerifyByPnum(pnum);
        boolean hasVerify = verifyPerson != null;
        userVal.setVerify(hasVerify);

        switch (userVal.getType()) {
            case MemberUtil.TYPE_EMPLOYEE:
                userVal.setIquest(true);
                userVal.setConfidential(checkMailings(mailings, MemberUtil.MAILING_CONFIDENTIAL));
                userVal.setQuerytool(true);
                userVal.setDaily(checkMailings(mailings, MemberUtil.MAILING_DAILY));
                break;
            case MemberUtil.TYPE_MEMBER:
            case MemberUtil.TYPE_INDEPENDENT_DIRECTOR:
                userVal.setConfidential(checkMailings(mailings, MemberUtil.MAILING_CONFIDENTIAL));
                userVal.setQuerytool(true);
                break;
//            case MemberUtil.TYPE_ASSOCIATE_MEMBER:
//            case MemberUtil.TYPE_ENTERPRISE_SUBSCRIBER:
//            case MemberUtil.TYPE_STATISTICAL_SUBSCRIBER:
//                userVal.setQuerytool(checkMailings(MemberUtil.MAILING_QUERYTOOL));
//                break;
            default:
                userVal.setQuerytool(checkMailings(mailings, MemberUtil.MAILING_QUERYTOOL));
                break;
        }

        // Set status to ZERO if the user is inactive
//        if (userInactive())
//            userVal.setStatus(STATUS_DISABLE);

//        logger.info("Retrieved Record: " + userVal.toString());
        return userVal;
    }


    private String buildWhereClause(List<PersonRequest> requestsWithNoPnum) {
        StringJoiner stringBuilder = new StringJoiner(" AND ");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Field[] requestFields = PersonRequest.class.getDeclaredFields();
        for(PersonRequest request: requestsWithNoPnum){
            for(int i =0; i < requestFields.length; i++) {
                String fieldName = requestFields[i].getName();
                if(fieldName.equalsIgnoreCase("type") || fieldName.equalsIgnoreCase("typeStr")){
                    continue;
                }
                String filterField = "";
                try {
                    Field currentField = PersonRequest.class.getDeclaredField(fieldName);
                    currentField.setAccessible(true);
                    Object fieldVal = currentField.get(request);
                    if(fieldVal != null){
                        filterField = fieldName + " = ";

                        if(currentField.getType().getName().contains("java.lang.String")){
                            if(!fieldVal.equals("")) {
                                fieldVal.toString().replaceAll("\'", "\\'");
                                filterField += "'" + fieldVal + "'";
                            } else{
                                filterField = null;
                            }
                        }
                        else if (currentField.getType().getName().contains("java.util.Date")){
                            Date fieldValDate = (Date)fieldVal;
                            String formattedDate = simpleDateFormat.format(fieldValDate);
                            filterField += formattedDate;
                        }
                        else{
                            filterField += fieldVal;
                        }
                        if(filterField != null) {
                            stringBuilder.add(filterField);
                        }
                    }
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }



    private String buildWhereClause(Class tClass, Object searchableObject) {
        StringJoiner stringBuilder = new StringJoiner(" AND ");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Field[] requestFields = tClass.getDeclaredFields();

            for(int i =0; i < requestFields.length; i++) {
                String fieldName = requestFields[i].getName();
                /*if(fieldName.equalsIgnoreCase("type")){
                    continue;
                }*/
                String filterField = "";
                try {
                    Field currentField = tClass.getDeclaredField(fieldName);
                    currentField.setAccessible(true);
                    Object fieldVal = currentField.get(searchableObject);
                    if(fieldVal != null){
                        filterField = fieldName + " = ";

                        if(currentField.getType().getName().contains("java.lang.String")){
                            filterField += "'" + fieldVal + "'";
                        }
                        else if (currentField.getType().getName().contains("java.util.Date")){
                            Date fieldValDate = (Date)fieldVal;
                            String formattedDate = simpleDateFormat.format(fieldValDate);
                            filterField += formattedDate;
                        }
                        else{
                            filterField += fieldVal;
                        }
                        stringBuilder.add(filterField);
                    }
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        return stringBuilder.toString();
    }

    public List<Company> getCompanies(Company company) {
        String whereClause = buildWhereClause(company.getClass(), company);
        if(whereClause.length() == 0){
            return new ArrayList<>();
        } else{
            List<Integer> companyCnums = new ArrayList<>(companyMapper.getCompanyByRequest(whereClause, "membership..company", "cnum"));
            CompanyExample example = new CompanyExample();
            example.or().andCnumIn(companyCnums);
            return companyMapper.selectByExample(example);
        }
    }
}
