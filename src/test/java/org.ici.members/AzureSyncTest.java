package org.ici.members;

import org.ici.members.services.CRMDynamics365AuthenticationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.List;
import java.net.MalformedURLException;
import java.util.ArrayList;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AzureSyncTest {

     Logger logger = LoggerFactory.getLogger( MemberServiceTest.class );

    @Autowired
    private CRMDynamics365AuthenticationService service;

   private List<String> azureUrls = new ArrayList<>();

    public AzureSyncTest(){
         azureUrls.add("icirs_crmtoicicontactses");
         azureUrls.add("icirs_crmtoiciaddresses");
         azureUrls.add("icirs_crmtoiciboardcommittees");
         azureUrls.add("icirs_crmtoiciboardcommitteereps");
         azureUrls.add("icirs_crmtoicicommitteereps");
         azureUrls.add("icirs_crmtoicicommittees");
         azureUrls.add("icirs_crmtoicicompanieses");
         azureUrls.add("icirs_crmtoicicompanycommunications");
         azureUrls.add("icirs_crmtoicicompanyrelations");
         azureUrls.add("icirs_crmtoicicontactpurposes");
         azureUrls.add("icirs_crmtoiciespps");
         azureUrls.add("icirs_crmtoicimailingrecips");
         azureUrls.add("icirs_crmtoicimailings");
         azureUrls.add("icirs_crmtoicimemberships");
         azureUrls.add("icirs_crmtoicipersoncommunications");
         azureUrls.add("icirs_crmtoicisalutations");
    }

    @Test
    public void authenticateAndTestSync(){
        try {
            String token = service.authenticateCrm();
            assertNotNull(token);
            for(String url: azureUrls){
                 service.getAndSyncEntity(token,url);
            }
        } catch (MalformedURLException e) {
            fail();
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
            throw new RuntimeException(e);

        }
    }

    @Test
    public void getEvents(){
        try {
            String token = service.authenticateCrm();
            assertNotNull(token);
            String eventHtml = "https://icitest.crm.dynamics.com/api/data/v9.2/msevtmgt_events( 3a9ea727-b0d8-4cb4-b4c3-031cc1acc5eb)";

        } catch (MalformedURLException e) {
            fail();
            throw new RuntimeException(e);
        }

    }

}
