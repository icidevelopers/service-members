package org.ici.members;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MemberServiceTest {

    private static final Logger logger = LoggerFactory.getLogger( MemberServiceTest.class );

    @Test
    public void fakeTest() throws Exception {
        Integer pnum = 82370;
        assertNotNull( pnum );
    }


}
