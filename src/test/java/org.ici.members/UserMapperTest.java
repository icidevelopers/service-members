package org.ici.members;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserMapperTest {

    private static final Logger logger = LoggerFactory.getLogger( UserMapperTest.class );

//    @Autowired
//    private UserMapper userMapper;

    @Test
    public void fakeTest() throws Exception {
        Integer pnum = 82370;
        assertNotNull( pnum );
    }

//    @Before
//    public void setUp() throws Exception {
//        Injector injector = Guice.createInjector( new DbModule());
//        dao = injector.getInstance( RetirementReportDAO.class );
//    }


//    @Test
//    public void getCommittees() throws Exception {
//
//        Integer pnum = 82370;
//
//        List<Commitrep> commitreps = userMapper.getCommittesByPnum(pnum);
//
//        assertNotNull( pnum );
//        logger.info( String.valueOf(pnum) );
//    }
//
//    @Test
//    public void getDatabaseServer() throws Exception {
//
//        Integer pnum = 82370;
//
//        List<Address> personAddress = userMapper.getAddressViewByPnum(pnum);
//
//        assertNotNull( pnum );
//        logger.info( String.valueOf(pnum) );
//
//    }
//
//    @Test
//    public void getGrandIobNameFromIob() throws Exception {
//        String grandIobName = dao.getGrandIobNameFromIob( 12 );
//        assertNotNull( grandIobName );
//        logger.info( grandIobName );
//    }
//
//    @Test
//    public void getTnaForMoneyMarket() throws Exception {
//        DateTime date = new DateTime( 2017, 4, 30, 0, 0 );
//        Integer tna = dao.getTna( 94359, 0, date );
//        assertNotNull( tna );
//        logger.info( "tna for money market: {}", tna );
//    }
//
//    @Test
//    public void getTnaForConventional() throws Exception {
//        DateTime date = new DateTime( 2017, 4, 30, 0, 0 );
//        Integer tna = dao.getTna( 81970, 2, date );
//        assertNotNull( tna );
//        logger.info( "tna for conventional: {}", tna );
//    }
//
//    @Test
//    public void getNncfFromConventional() throws Exception {
//        DateTime date = new DateTime( 2017, 4, 30, 0, 0 );
//        Integer tna = dao.getNncf( 81970, 2, date );
//        assertNotNull( tna );
//        logger.info( "nncf for conventional: {}", tna );
//    }
//
//    @Test
//    public void getNncfForMoneyMarket() throws Exception {
//        DateTime date = new DateTime( 2017, 4, 30, 0, 0 );
//        Integer tna = dao.getTna( 94359, 0, date );
//        assertNotNull( tna );
//        logger.info( "nncf for money market: {}", tna );
//    }
}